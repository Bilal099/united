@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="icon-settings font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase"> Edit Vessel</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form role="form" method="POST" action="{{route('vessels.update',$vessel->id)}}">
                        @method('PUT')
                        @csrf
                        <div class="form-body">
                            <div class="form-group @error('name') has-error @enderror">
                                <label>Agent Name</label>
                                <input type="text" name="name" class="form-control" value="{{old('name') != null ?old('name'):$vessel->name}}" placeholder="Vessel Name">
                                @error('name')
                                <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn blue">Update</button>
                            <a href="{{route('vessels.index')}}" class="btn default">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection