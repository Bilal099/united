<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8" />
    {{-- <title>Metronic Admin Theme #2 | Admin Dashboard 2</title> --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>United Marine</title>
    <link rel="icon" type="image/png" href="{{asset('images/favicon-32x32.png')}}"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="Preview page of Metronic Admin Theme #2 for statistics, charts, recent events and reports"
        name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=all" rel="stylesheet"
        type="text/css" />
    {{-- <link href="{{asset('assets/global/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" /> --}}
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"/>
    <link href="{{asset('assets/global/plugins/simple-line-icons/simple-line-icons.min.css')}}" rel="stylesheet"
        type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')}}" rel="stylesheet"
        type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')}}" rel="stylesheet"
        type="text/css" />
    <link href="{{asset('assets/global/plugins/morris/morris.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/fullcalendar/fullcalendar.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/jqvmap/jqvmap/jqvmap.css')}}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="{{asset('assets/global/css/components.min.css')}}" rel="stylesheet" id="style_components" type="text/css" />
    <link href="{{asset('assets/global/css/plugins.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="{{asset('assets/layouts/layout2/css/layout.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/layouts/layout2/css/themes/blue.min.css')}}" rel="stylesheet" type="text/css" id="style_color" />
    <link href="{{asset('assets/layouts/layout2/css/custom.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="favicon.ico" />
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <style>
        #profileImage {
            width: 30px;
            height: 30px;
            border-radius: 15%;
            /* background: #512DA8; */
            background: #17c4bb;
            font-size: 15px;
            color: #fff;
            text-align: center;
            /* line-height: 150px; */
            display: inline-block;
            padding-top: 4px;
            /* margin: 20px 0; */
        }
        .page-header.navbar .top-menu .navbar-nav>li.dropdown-user .dropdown-toggle {
            padding: 12px 12px !important;
        }
    </style>

    @yield('customCSS')
</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid">
    <!-- BEGIN HEADER -->
    <div class="page-header navbar navbar-fixed-top">
        <!-- BEGIN HEADER INNER -->
        <div class="page-header-inner ">
            <!-- BEGIN LOGO -->
            <div class="page-logo">
                <a href="#">
                    {{-- <img src="{{asset('assets/layouts/layout2/img/logo-default.png')}}" alt="logo" class="logo-default" />  --}}
                    <h4 class="logo-default" style="color: white">UNITED MARINE</h4>
                </a>
                <div class="menu-toggler sidebar-toggler">
                    <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
                </div>
            </div>
            <!-- END LOGO -->
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse"
                data-target=".navbar-collapse"> </a>
            <!-- END RESPONSIVE MENU TOGGLER -->
            <!-- BEGIN PAGE ACTIONS -->
            
            <!-- BEGIN PAGE TOP -->
            <div class="page-top">
                
                <!-- BEGIN TOP NAVIGATION MENU -->
                <div class="top-menu">
                    <ul class="nav navbar-nav pull-right">
                       
                        <li class="dropdown dropdown-user">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                                data-close-others="true">
                                {{-- <img alt="" class="img-circle" src="{{asset('assets/layouts/layout2/img/avatar3_small.jpg')}}" /> --}}
                                {{-- <img src="" alt="" class="img-circle" id="profileImage"> --}}
                                <div  id="profileImage" class="img-circle"></div>
                                <span class="username username-hide-on-mobile" id="firstName"> {{Auth::user()->name}} </span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-default">
                                {{-- <li>
                                    <a href="page_user_profile_1.html">
                                        <i class="icon-user"></i> My Profile </a>
                                </li>
                                
                                <li class="divider"> </li> --}}
                                <li>
                                    <a class="" data-toggle="modal" data-target="#myModal" >
                                        {{-- <i class="icon-setting"></i>  --}}
                                        <i class="icon-settings"></i> Change Password
                                     </a>
                                </li>
                                
                                <li>
                                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        <i class="icon-key"></i> Log Out </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                            @csrf
                                        </form>
                                </li>
                            </ul>
                        </li>
                        <!-- END USER LOGIN DROPDOWN -->
                        <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                        
                        <!-- END QUICK SIDEBAR TOGGLER -->
                    </ul>
                </div>
                <!-- END TOP NAVIGATION MENU -->
            </div>
            <!-- END PAGE TOP -->
        </div>
        <!-- END HEADER INNER -->
    </div>
    <!-- END HEADER -->
    <!-- BEGIN HEADER & CONTENT DIVIDER -->
    <div class="clearfix"> </div>
    <!-- END HEADER & CONTENT DIVIDER -->
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        <div class="page-sidebar-wrapper">
            <!-- END SIDEBAR -->
            <div class="page-sidebar navbar-collapse collapse">
                <!-- BEGIN SIDEBAR MENU -->
                
                <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-hover-submenu "
                    data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                    <li class="nav-item {{Request::segment(1) == 'home'? 'start active open':''}}"> 
                        {{--  start active open --}}
                        <a href="{{ url('/home')}}" class="nav-link nav-toggle">
                            <i class="icon-home"></i>
                            <span class="title">Dashboard</span>
                            <span class="selected"></span>
                            <span class="arrow open"></span>
                        </a>
                        {{-- <ul class="sub-menu">
                            <li class="nav-item start active open">
                                <a href="index-2.html" class="nav-link ">
                                    <i class="icon-bar-chart"></i>
                                    <span class="title">Dashboard 1</span>
                                    <span class="selected"></span>
                                </a>
                            </li>
                            <li class="nav-item start ">
                                <a href="dashboard_2.html" class="nav-link ">
                                    <i class="icon-bulb"></i>
                                    <span class="title">Dashboard 2</span>
                                    <span class="badge badge-success">1</span>
                                </a>
                            </li>
                            <li class="nav-item start ">
                                <a href="dashboard_3.html" class="nav-link ">
                                    <i class="icon-graph"></i>
                                    <span class="title">Dashboard 3</span>
                                    <span class="badge badge-danger">5</span>
                                </a>
                            </li>
                        </ul> --}}
                    </li>
                    <li class="nav-item {{Request::segment(1) == 'agents'? 'start active open':''}}">
                        <a href="{{route('agents.index')}}" class="nav-link nav-toggle">
                            {{-- <i class="icon-home"></i> --}}
                            <i class="fas fa-user-tie"></i>
                            <span class="title">Agents</span>
                            <span class="selected"></span>
                            <span class="arrow open"></span>
                        </a>
                        {{-- <ul class="sub-menu">
                            <li class="nav-item start">
                                <a href="{{route('agents.index')}}" class="nav-link ">
                                    <i class="icon-bar-chart"></i>
                                    <span class="title">View Agents</span>
                                </a>
                            </li>
                            <li class="nav-item start ">
                                <a href="{{route('agents.create')}}" class="nav-link ">
                                    <i class="icon-bulb"></i>
                                    <span class="title">Add Agent</span>
                                </a>
                            </li>
                        </ul> --}}
                    </li>
                    <li class="nav-item {{Request::segment(1) == 'vessels'? 'start active open':''}}">
                        <a href="{{route('vessels.index')}}" class="nav-link nav-toggle">
                            {{-- <i class="icon-home"></i> --}}
                            <i class="fas fa-ship"></i>
                            <span class="title">Vessels</span>
                            <span class="selected"></span>
                            <span class="arrow open"></span>
                        </a>
                        {{-- <ul class="sub-menu">
                            <li class="nav-item start">
                                <a href="{{route('vessels.index')}}" class="nav-link ">
                                    <i class="icon-bar-chart"></i>
                                    <span class="title">View Vessel</span>
                                </a>
                            </li>
                            <li class="nav-item start ">
                                <a href="{{route('vessels.create')}}" class="nav-link ">
                                    <i class="icon-bulb"></i>
                                    <span class="title">Add Vessel</span>
                                </a>
                            </li>
                        </ul> --}}
                    </li>

                    <li class="nav-item {{Request::segment(1) == 'container'? 'start active open':''}} ">
                        <a href="{{route('container.index')}}" class="nav-link nav-toggle">
                            {{-- <i class="icon-home"></i> --}}
                            <i class="fas fa-boxes"></i>
                            <span class="title">Container</span>
                            <span class="selected"></span>
                            <span class="arrow open"></span>
                        </a>
                        {{-- <ul class="sub-menu">
                            <li class="nav-item start">
                                <a href="{{route('container.index')}}" class="nav-link ">
                                    <i class="icon-bar-chart"></i>
                                    <span class="title">View Container</span>
                                </a>
                            </li>
                            <li class="nav-item start ">
                                <a href="{{route('container.create')}}" class="nav-link ">
                                    <i class="icon-bulb"></i>
                                    <span class="title">Add Container</span>
                                </a>
                            </li>
                        </ul> --}}
                    </li>

                    <li class="nav-item {{Request::segment(1) == 'frieghtForWarder'? 'start active open':''}}">
                        <a href="{{route('frieghtForWarder.index')}}" class="nav-link nav-toggle">
                            {{-- <i class="icon-home"></i> --}}
                            <i class="fas fa-hand-point-right"></i>
                            <span class="title">Frieght Forwarder</span>
                            <span class="selected"></span>
                            <span class="arrow open"></span>
                        </a>
                    </li>
                    <li class="nav-item {{Request::segment(1) == 'itemtypes'? 'start active open':''}}">
                        <a href="{{route('itemtypes.index')}}" class="nav-link nav-toggle">
                            {{-- <i class="icon-home"></i> --}}
                            <i class="fas fa-list-alt"></i>
                            <span class="title">Container Types</span>
                            <span class="selected"></span>
                            <span class="arrow open"></span>
                        </a>
                    </li>
                    <li class="nav-item {{Request::segment(1) == 'import-cargo'? 'start active open':''}}">
                        <a href="{{ route('import-cargo.index')}}" class="nav-link nav-toggle">
                            {{-- <i class="icon-home"></i> --}}
                            <i class="fas fa-file-import"></i>
                            <span class="title">Import Cargo</span>
                            <span class="selected"></span>
                            <span class="arrow open"></span>
                        </a>
                    </li>

                    <li class="nav-item {{Request::segment(1) == 'export-cargo'? 'start active open':''}}">
                        <a href="{{route('export-cargo.index')}}" class="nav-link nav-toggle">
                            {{-- <i class="icon-home"></i> --}}
                            <i class="fas fa-file-export"></i>
                            <span class="title">Export Cargo</span>
                            <span class="selected"></span>
                            <span class="arrow open"></span>
                        </a>
                        
                    </li>
                    <li class="nav-item {{Request::segment(1) == 'users'? 'start active open':''}}">
                        <a href="{{route('users.index')}}" class="nav-link nav-toggle">
                            <i class="icon-user"></i>
                            <span class="title">Users</span>
                            <span class="selected"></span>
                            <span class="arrow open"></span>
                        </a>
                    </li>

                    <li class="nav-item {{Request::segment(1) == 'report'? 'start active open':''}}">
                        <a href="{{route('report.view')}}" class="nav-link nav-toggle">
                            <i class="far fa-file-alt"></i>
                            <span class="title">Reports</span>
                            <span class="selected"></span>
                            <span class="arrow open"></span>
                        </a>
                    </li>

                    <li class="nav-item {{Request::segment(1) == 'ViewReport'? 'start active open':''}}">
                        <a href="{{route('report.ViewReport')}}" class="nav-link nav-toggle">
                            <i class="far fa-file-alt"></i>
                            <span class="title">View Reports</span>
                            <span class="selected"></span>
                            <span class="arrow open"></span>
                        </a>
                    </li>
                    
                </ul>
                <!-- END SIDEBAR MENU -->
            </div>
            <!-- END SIDEBAR -->
        </div>
        <!-- END SIDEBAR -->
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <!-- BEGIN PAGE HEADER-->

                <!-- END PAGE HEADER-->
                <div id="success_div" class="custom-alerts alert alert-success fade in" style="display: none;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    <p id="success_msg"></p>
                </div>
                @yield('content')
                
            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
        
    </div>
    <!-- END CONTAINER -->
    <!-- BEGIN FOOTER -->
    <div class="page-footer">
        <div class="page-footer-inner"> 2015 &copy; United Marine
            {{-- <a target="_blank" href="#">Keenthemes</a>  --}}
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
    </div>
    <!-- END FOOTER -->
    <!-- BEGIN QUICK NAV -->
    {{-- <nav class="quick-nav">
        <a class="quick-nav-trigger" href="#0">
            <span aria-hidden="true"></span>
        </a>
        <ul>
            <li>
                <a href="https://1.envato.market/EA4JP" target="_blank" class="active">
                    <span>Purchase Metronic</span>
                    <i class="icon-basket"></i>
                </a>
            </li>
            <li>
                <a href="https://1.envato.market/EA4JP" target="_blank">
                    <span>Customer Reviews</span>
                    <i class="icon-users"></i>
                </a>
            </li>
            <li>
                <a href="http://keenthemes.com/showcast/" target="_blank">
                    <span>Showcase</span>
                    <i class="icon-user"></i>
                </a>
            </li>
            <li>
                <a href="http://keenthemes.com/metronic-theme/changelog/" target="_blank">
                    <span>Changelog</span>
                    <i class="icon-graph"></i>
                </a>
            </li>
        </ul>
        <span aria-hidden="true" class="quick-nav-bg"></span>
    </nav> --}}
    <div class="quick-nav-overlay"></div>
    <!-- END QUICK NAV -->



    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Change Password</h4>
                </div>
                <div class="modal-body">
                    <div id="error_div" class="custom-alerts alert alert-danger fade in" style="display: none">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                        <p id="error_msg"></p>
                    </div>
                    <form action="" id="form_change_password" method="post">
                        @csrf
                        <div class="form-body row">
                            <div class="form-group col-md-12">
                                <label class="">Current Password</label>
        
                                <input  type="password" name="current_password" class="form-control" placeholder="Enter Current Password" id="current_password" >
                                
                               
                            </div>
                            <div class="form-group col-md-12">
                                <label class="">New Password</label>
                                <input  type="password" name="new_password" class="form-control" placeholder="Enter New Password" id="new_password" >
                               
                            </div>
                            <div class="form-group col-md-6 col-lg-12">
                                <label class="">Confirm New Password</label>
                                <input  type="password" name="confirm_new_password" class="form-control" placeholder="Enter Confirm New Password" id="confirm_new_password">
                               
                            </div>
                            
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="submit" form="form_change_password" id="submit_btn" class="btn btn-success" >Submit</button>

                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> --}}
    
    {{-- <script src="{{asset('assets/global/plugins/respond.min.js')}}"></script>
    <script src="{{asset('assets/global/plugins/excanvas.min.js')}}"></script> 
    <script src="{{asset('assets/global/plugins/ie8.fix.min.js')}}"></script>  --}}
            
    <!-- BEGIN CORE PLUGINS -->
    <script src="{{asset('assets/global/plugins/jquery.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/js.cookie.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jquery.blockui.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{asset('assets/global/plugins/moment.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}" type="text/javascript">
    </script>
    <script src="{{asset('assets/global/plugins/morris/morris.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/morris/raphael-min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/counterup/jquery.waypoints.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/counterup/jquery.counterup.min.js')}}" type="text/javascript"></script>

    {{-- <script src="{{asset('assets/global/plugins/amcharts/amcharts/amcharts.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/amcharts/amcharts/serial.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/amcharts/amcharts/pie.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/amcharts/amcharts/radar.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/amcharts/amcharts/themes/light.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/amcharts/amcharts/themes/patterns.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/amcharts/amcharts/themes/chalk.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/amcharts/ammap/ammap.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/amcharts/ammap/maps/js/worldLow.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/amcharts/amstockcharts/amstock.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/fullcalendar/fullcalendar.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/horizontal-timeline/horizontal-timeline.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/flot/jquery.flot.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/flot/jquery.flot.resize.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/flot/jquery.flot.categories.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js')}}" type="text/javascript">
    </script>
    <script src="{{asset('assets/global/plugins/jquery.sparkline.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js')}}" type="text/javascript"></script> --}}
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="{{asset('assets/global/scripts/app.min.js')}}" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{asset('assets/pages/scripts/dashboard.min.js')}}" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN THEME LAYOUT SCRIPTS -->
    <script src="{{asset('assets/layouts/layout2/scripts/layout.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/layouts/layout2/scripts/demo.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/layouts/global/scripts/quick-sidebar.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/layouts/global/scripts/quick-nav.min.js')}}" type="text/javascript"></script>
    <!-- END THEME LAYOUT SCRIPTS -->
<script>
    $(document).ready(function(){
        $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
              });
    })
</script>
    @yield('customJS')
    <script>
        $(document).ready(function () {
            
            var name = '<?= Auth::user()->name ?>';
            var intials = name.charAt(0).toUpperCase(); 
            
            var profileImage = $('#profileImage').text(intials);

            console.log('intials',intials);

            $('#clickmewow').click(function () {
                $('#radio1003').attr('checked', 'checked');
            });
            

            // var url      = window.location.href;
            // $('.nav-item').closest('ul').find('a[href="'+ url +'"]').closest('li').addClass('start active open');

        })

        $("#submit_btn").click(function (e) { 

            e.preventDefault();

            let current_password        = $("#current_password").val(); 
            let new_password            = $("#new_password").val();
            let confirm_new_password    = $("#confirm_new_password").val();

            $.ajax({
                type: "POST",
                url: "{{route('changePassword')}}",
                data: {
                    _token : "{{ csrf_token() }}",
                    current_password : current_password,
                    new_password : new_password,
                    confirm_new_password : confirm_new_password,
                },
                success: function (response) 
                {    
                    if (response['status']) 
                    {
                        // $("#myModal").hide();
                        $("#myModal").modal('hide');
                        $("#success_msg").text("Password is successfully changed!");
                        $("#success_div").show();
                    } 
                    else {
    
                        $("#error_msg").text(response['error']);
                        $("#error_div").show();
                    }
                }
            });
        });

        

        

    </script>
</body>
</html>
