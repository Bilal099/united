@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="icon-settings font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase">Edit User</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form role="form" method="POST" action="{{route('users.update',$user->id)}}" autocomplete="off">
                        @csrf
                        @method('PUT')
                        <div class="form-body">
                            <div class="form-group @error('name') has-error @enderror ">
                                <label>User Name</label>
                                <input type="text" name="name" value="{{old('name')?old('name'):$user->name}}" class="form-control" placeholder="User Name">
                                @error('name')
                                <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group @error('email') has-error @enderror">
                                <label>User Email</label>
                                <input type="text" name="email" value="{{old('email')?old('email'):$user->email}}" class="form-control" placeholder="User Email">
                                @error('email')
                                <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group @error('user_type') has-error @enderror">
                                <label>User Type</label>
                                <select name="user_type" class="form-control" id="">
                                    <option value>-- Select user Type --</option>
                                    @foreach ($usertypes as $key => $item)
                                        <option value="{{$key}}" {{$user->userTypeId == $key?'selected':''}}>{{$item}}</option>
                                    @endforeach
                                </select>
                                @error('user_type')
                                <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn blue">Save</button>
                            <a href="{{route('users.index')}}" class="btn default">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection