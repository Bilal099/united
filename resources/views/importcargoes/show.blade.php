@extends('layouts.app')
@section('customCSS')
<link href="{{asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="icon-settings font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase">View Import Cargo</span>
                    </div>
                    <button class="btn btn-primary pull-right" id="btn-toggle-remarks" data-value="hide">Show Remarks</button>
                </div>
                <div class="portlet-body">
                        <div class="form-body row">
                            <div class="col-sm-12">
                                @if (Session::has('message'))
                                    <div class="note note-success">
                                        <h5>{{Session::get('message')}}</h5>
                                    </div>
                                @endif
                                <table class="table table-striped table-bordered" style="margin-top:20px">
                                    <thead>
                                        <tr>
                                            <th>M.V</th>
                                            <th>VOY</th>
                                            <th>OF</th>
                                            <th>Date of Measurement/De-Stuffing</th>
                                            <th>Container</th>
                                            <th>Size</th>
                                            <th>Type</th>
                                            <th>Status</th>
                                            <th>Serial No</th>
                                            <th>IGM Number</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{{$importcargo->vessel->name}}</td>
                                            <td>{{$importcargo->voy}}</td>
                                            <td>{{date_format(date_create($importcargo->departureDate),"j M, Y")}}</td>
                                            <td>{{date_format(date_create($importcargo->dom_des),"j M, Y")}}</td>
                                            <td>{{$importcargo->container->name}}</td>
                                            <td>{{$importcargo->container->size}}</td>
                                            <td>{{$importcargo->container->ContainerType->name}}</td>
                                            <td>{{$importcargo->status}}</td>
                                            <td>{{$importcargo->serialNo}}</td>
                                            <td>{{$importcargo->igm_number}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            
                            <div class="col-sm-12">
                                <h4>Import Cargo Details</h4>
                            </div>
                            <div class="col-sm-12">
                                <form action="{{route('importCargo.remarks')}}" method="post" id="formRemarks">
                                    <input type="hidden" name="importcargoId" value="{{Request::segment(2)}}">
                                    @csrf
                                    <table class="table table-striped table-bordered" style="margin-top:20px" id="importDetailTable">
                                        <thead>
                                            <tr>
                                                <th>Index</th>
                                                <th>Stowage on Board</th>
                                                <th>Marks & NOS</th>
                                                <th>Description</th>
                                                {{-- <th>Size</th>
                                                <th>Type</th> --}}
                                                <th>Seal No</th>
                                                <th class="remarks-colum hidden">Remarks</th>
                                                <th class="remarks-colum hidden">Accessories Found At De-Stuffing</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($importcargo->importcargosdetail as $item)
                                                <tr>
                                                    <td>{{@$item->cargo_index}}</td>
                                                    <td>{{@$item->stowageOnBoard}}</td>
                                                    <td>{{@$item->mark_no}}</td>
                                                    <td>{{@$item->description}}</td>
                                                    {{-- <td>{{@$item->size}}</td>
                                                    <td>{{@$item->itemTypeId}}</td> --}}
                                                    <td>{{@$item->sealNo}}</td>
                                                    <td class="remarks-colum hidden">
                                                        <input type="hidden" name="importdetailId[]" value="{{@$item->id}}">
                                                        <input type="text" value="{{@$item->remarks}}" name="remarks[]" class="form-control" placeholder="Enter Remarks" />
                                                    </td>
                                                    <td class="remarks-colum hidden">
                                                        <input type="text" value="{{@$item->accessories}}" name="accessories[]" class="form-control" id="">
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </form>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" form="formRemarks" class="btn blue hidden updateRemarksBtn">Update Remarks</button>
                            <a href="{{route('import-cargo.index')}}" class="btn default">Cancel</a>
                        </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('customJS')
    <script>
        $(document).on('click','#btn-toggle-remarks',function(){
            if($(this).attr('data-value') === 'hide')
            {
                $('.remarks-colum').removeClass('hidden');
                $(this).attr('data-value','show');
                $(this).text('Hide Remarks');
                $('.updateRemarksBtn').removeClass('hidden');
            }
            else
            {
                $('.remarks-colum').addClass('hidden');
                $(this).attr('data-value','hide');
                $(this).text('Show Remarks');
                $('.updateRemarksBtn').addClass('hidden');
            }
        })
    </script>
@endsection