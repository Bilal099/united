@extends('layouts.app')
@section('customCSS')
<link href="{{asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="icon-settings font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase">Add Import Cargo</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form role="form" method="POST" action="{{route('import-cargo.store')}}" autocomplete="off" id="importCargo">
                        @csrf
                        <div class="form-body row">
                            <div class="form-group col-md-6 col-lg-3 @error('mv') has-error @enderror ">
                                <label>M. V.</label>
                                <input list="mvList" type="text" id="mv" name="mv" value="{{old('mv')}}" class="form-control" placeholder="Search M. V.">
                                <datalist id="mvList">
                                    
                                </datalist>
                                @error('mv')
                                <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group col-md-6 col-lg-3 @error('voy') has-error @enderror">
                                <label>VOY</label>
                                <input type="text" name="voy" class="form-control" value="{{old('voy')}}" placeholder="Enter VOY">
                                @error('voy')
                                <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group col-md-6 col-lg-3 @error('igm_number') has-error @enderror">
                                <label>IGM Number</label>
                                <input type="text" name="igm_number" class="form-control" value="{{old('igm_number')}}" placeholder="Enter IGM Number">
                                @error('igm_number')
                                <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group col-md-6 col-lg-3 @error('of') has-error @enderror">
                                <label>OF</label>
                                {{-- <input type="date" name="of" class="form-control" placeholder="Enter OF"> --}}
                                <div class="input-group date form_datetime form_datetime bs-datetime date-picker">
                                    <input type="text" size="16" class="form-control" name="of" value="{{old('of')}}">
                                    <span class="input-group-addon">
                                        <button class="btn default date-set" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                                </div>
                                @error('of')
                                <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group col-md-3 @error('date_of_measurement') has-error @enderror">
                                <label>Date of Measurement/De-Stuffing</label>
                                {{-- <input type="date" name="date_of_measurement" class="form-control" placeholder="Enter Date of Measurement"> --}}
                                <div class="input-group date form_datetime form_datetime bs-datetime date-picker">
                                    <input type="text" size="16" class="form-control" name="date_of_measurement" value="{{old('date_of_measurement')}}">
                                    <span class="input-group-addon">
                                        <button class="btn default date-set" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                                </div>
                                @error('date_of_measurement')
                                <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group col-md-3 @error('container') has-error @enderror">
                                <label>Container</label>
                                <input list="containerList" type="text" value="{{old('container')}}"  id="container" name="container" class="form-control" placeholder="Search Container">
                                <datalist id="containerList">

                                </datalist>
                                @error('container')
                                <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group col-md-3 @error('size') has-error @enderror">
                                <label>Size</label>
                                <select  id="size" name="size" class="form-control">
                                    <option value>Select Size</option>
                                    <option value="20" {{old('size') == 20?'selected':''}}>20</option>
                                    <option value="40" {{old('size') == 40?'selected':''}}>40</option>
                                </select>
                                @error('size')
                                <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group col-md-3 @error('container_type') has-error @enderror">
                                <label>Container Type</label>
                                <select class="form-control " name="container_type" id="container_type">
                                    <option value>Select Container Type</option>
                                    @foreach ($itemtypes as $item)
                                        <option value="{{$item->id}}" {{old('container_type') == $item->id?'selected':''}}>{{$item->name}}</option>
                                    @endforeach
                                </select>
                                @error('container_type')
                                    <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                @enderror
                            </div>
                                
                            <div class="col-sm-12">
                                <h4>Import Cargo Details <button type="button" class="btn btn-primary" id="btnAddDetail" style="float: right;">Add Detail</button></h4>
                            </div>
                            <div class="col-sm-12">
                                <table class="table table-striped table-bordered" style="margin-top:20px" id="importDetailTable">
                                    <thead>
                                        <tr>
                                            <th>Index</th>
                                            <th>Stowage on Board</th>
                                            <th>Marks & NOS</th>
                                            <th>Description</th>
                                            <th>Measurment M3</th>
                                            <th>Seal No</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (old('index') != null)
                                            @foreach (old('index') as $key => $oldValues)
                                                <tr>
                                                    <td>
                                                        <div class="form-group @error('index.'.$key) has-error @enderror">
                                                            <input type="text" class="form-control " name="index[]" value="{{$oldValues}}" placeholder="Enter Index">
                                                        </div>
                                                        @error('index.'.$key)
                                                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                                        @enderror
                                                    </td>
                                                    <td>
                                                        <div class="form-group @error('stowage.'.$key) has-error @enderror">
                                                        <input type="text" class="form-control " value="{{old('stowage.'.$key)}}" name="stowage[]" placeholder="Enter Stowage on Board">
                                                        </div>
                                                        @error('stowage.'.$key)
                                                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                                        @enderror
                                                    </td>
                                                    <td>
                                                        <div class="form-group @error('marks.'.$key) has-error @enderror">
                                                        <input type="text" class="form-control " value="{{old('marks.'.$key)}}" name="marks[]" placeholder="Enter Marks">
                                                        </div>
                                                        @error('marks.'.$key)
                                                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                                        @enderror
                                                    </td>
                                                    <td>
                                                        <div class="form-group @error('description.'.$key) has-error @enderror">
                                                        <input type="text" class="form-control " value="{{old('description.'.$key)}}" name="description[]" placeholder="Enter Description">
                                                        </div>
                                                        @error('description.'.$key)
                                                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                                        @enderror
                                                    </td>
                                                    <td>
                                                        <div class="form-group @error('measurment.'.$key) has-error @enderror">
                                                        <input type="text" class="form-control " value="{{old('measurment.'.$key)}}" name="measurment[]" placeholder="Enter measurment M3">
                                                        </div>
                                                        @error('measurment.'.$key)
                                                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                                        @enderror
                                                    </td>
                                                    <td>
                                                        <div class="form-group @error('sealno.'.$key) has-error @enderror">
                                                        <input type="text" class="form-control " value="{{old('sealno.'.$key)}}" name="sealno[]" placeholder="Enter Seal No">
                                                        </div>
                                                        @error('sealno.'.$key)
                                                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                                        @enderror
                                                    </td>
                                                    <th>
                                                        @if ($key != 0)
                                                            <button type="button" class="btn btn-danger btnRowDelete"><i class="fa fa-trash"></i></button>
                                                        @endif
                                                    </th>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td>
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" name="index[]" placeholder="Enter Index">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                    <input type="text" class="form-control" name="stowage[]" placeholder="Enter Stowage on Board">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                    <input type="text" class="form-control" name="marks[]" placeholder="Enter Marks">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                    <input type="text" class="form-control" name="description[]" placeholder="Enter Description">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                    <input type="text" class="form-control" name="measurment[]" placeholder="Enter Measurment M3">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group">
                                                    <input type="text" class="form-control" name="sealno[]" placeholder="Enter Seal No">
                                                    </div>
                                                </td>
                                                <th>
                                                    {{-- <button type="button" class="btn btn-danger btnRowDelete"><i class="fa fa-trash"></i></button> --}}
                                                </th>
                                            </tr>
                                        @endif
                                        
                                       
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                        <div class="form-actions">
                            <button type="submit" id="alert_msg" class="btn blue">Save</button>
                            <a href="{{route('import-cargo.index')}}" class="btn default">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script type="text/html" id="data-row">
        <tr>
            <td>
                <div class="form-group">
                    <input type="text" class="form-control" name="index[]" placeholder="Enter Index">
                </div>
            </td>
            <td>
                <div class="form-group">
                <input type="text" class="form-control" name="stowage[]" placeholder="Enter Stowage on Board">
                </div>
            </td>
            <td>
                <div class="form-group">
                <input type="text" class="form-control" name="marks[]" placeholder="Enter Marks">
                </div>
            </td>
            <td>
                <div class="form-group">
                <input type="text" class="form-control" name="description[]" placeholder="Enter Description">
                </div>
            </td>
            <td>
                <div class="form-group">
                <input type="text" class="form-control" name="measurment[]" placeholder="Enter Measurment M3">
                </div>
            </td>
            <td>
                <div class="form-group">
                <input type="text" class="form-control" name="sealno[]" placeholder="Enter Seal No">
                </div>
            </td>
            <th>
                <button type="button" class="btn btn-danger btnRowDelete"><i class="fa fa-trash"></i></button>
            </th>
        </tr>
    </script>
@endsection
@section('customJS')
<script src="{{asset('assets/global/plugins/jquery-repeater/jquery.repeater.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
{{-- <script src="{{asset('assets/pages/scripts/components-date-time-pickers.min.js')}}" type="text/javascript"></script> --}}
    <script>
        $(document).ready(function(){
            $('.date').datepicker({
                format: 'yyyy-mm-dd'
            });
        });
        $(document).on('keypress','#mv',function(){
            textVal = $(this).val();
            reqUrl = "{{route('vessel-fetch')}}";
            $.getJSON(reqUrl+'/'+textVal,function(data){
                if(data.status)
                {
                    $('#mvList').html('');
                    $.each(data.data,function(key,val){
                        $('#mvList').append('<option value="'+val.name+'"/>');
                    })
                }
            });
        });
        $(document).on('keypress','#container',function(){
            textVal = $(this).val();
            reqUrl = "{{route('container-fetch')}}";
            $.getJSON(reqUrl+'/'+textVal,function(data){
                if(data.status)
                {
                    $('#containerList').html('');
                    $.each(data.data,function(key,val){
                        $('#containerList').append('<option value="'+val.name+'"/>');
                    })
                }
            });
        });
        $(document).on('click','.btnRowDelete',function(){
            $(this).parents('tr').remove();
        });
        $(document).on('click','#btnAddDetail',function(){
            $('#importDetailTable').find('tbody').append($('#data-row').html());
        });

        $(document).on('blur','#container',function(){
            textVal = $(this).val();
            reqUrl = "{{route('container-exist')}}";
            $.getJSON(reqUrl+'/'+textVal,function(res){
                if(res.status == true)
                {
                    $('#size').val(res.data['size']);
                    $('#size').attr("style", "pointer-events: none;");
                    $('#container_type').val(res.data['itemtype_Id']);
                    $('#container_type').attr("style", "pointer-events: none;");
                }
                else
                {
                    $('#size').val('');
                    $('#size').attr("style", "pointer-events: auto;");
                    $('#container_type').val('');
                    $('#container_type').attr("style", "pointer-events: auto;");
                }
                
            });
        });

        $("#alert_msg").click(function (e) { 
            e.preventDefault();

            swal({
                title: "Are you sure?",
                // text: "Once Save, you will not be able to recover this imaginary file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
            if (willDelete) {
                myAlert();
            } else {
                swal("Your in not save!");
            }
            });

        });

        function myAlert(){  
            $('#importCargo').submit();
        }

    </script>
@endsection