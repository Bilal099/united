@extends('layouts.app')
@section('customCSS')
<link href="{{asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="icon-settings font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase">Add Import Cargo</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form role="form" method="POST" action="{{route('import-cargo.update',$importcargo->id)}}" autocomplete="off">
                        @method('PUT')
                        @csrf
                        <div class="form-body row">
                            <div class="form-group col-md-6 col-lg-3 @error('mv') has-error @enderror ">
                                <label>M. V.</label>
                                <input list="mvList" type="text" id="mv" value="{{$mvId}}" name="mv" class="form-control" placeholder="Search M. V.">
                                <datalist id="mvList">
                                    
                                </datalist>
                                @error('mv')
                                <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group col-md-6 col-lg-3 @error('voy') has-error @enderror">
                                <label>VOY</label>
                                <input type="text" name="voy" class="form-control" value="{{$importcargo->voy}}" placeholder="Enter VOY">
                                @error('voy')
                                <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group col-md-6 col-lg-3 @error('igm_number') has-error @enderror">
                                <label>IGM Number</label>
                                <input type="text" name="igm_number" class="form-control" value="{{$importcargo->igm_number}}" placeholder="Enter IGM Number">
                                @error('igm_number')
                                <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group col-md-6 col-lg-3 @error('of') has-error @enderror">
                                <label>OF</label>
                                {{-- <input type="date" name="of" class="form-control" placeholder="Enter OF"> --}}
                                <div class="input-group date form_datetime form_datetime bs-datetime date-picker">
                                    <input type="text" size="16" value="{{date('Y-m-d',strtotime($importcargo->departureDate))}}" class="form-control" name="of">
                                    <span class="input-group-addon">
                                        <button class="btn default date-set" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                                </div>
                                @error('of')
                                <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group col-md-3 @error('date_of_measurement') has-error @enderror">
                                <label>Date of Measurement/De-Stuffing</label>
                                {{-- <input type="date" name="date_of_measurement" class="form-control" placeholder="Enter Date of Measurement"> --}}
                                <div class="input-group date form_datetime form_datetime bs-datetime date-picker">
                                    <input type="text" size="16" class="form-control" value="{{date('Y-m-d',strtotime($importcargo->dom_des))}}" name="date_of_measurement">
                                    <span class="input-group-addon">
                                        <button class="btn default date-set" type="button">
                                            <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                                </div>
                                @error('date_of_measurement')
                                <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group col-md-3 @error('container') has-error @enderror">
                                <label>Container</label>
                                <input list="containerList" type="text" value="{{old('container') == null?$containerId:old('container')}}" id="container" name="container" class="form-control" placeholder="Search Container">
                                <datalist id="containerList">

                                </datalist>
                                @error('container')
                                <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group col-md-3 @error('size') has-error @enderror">
                                <label>Size</label>
                                <select  id="size" name="size" class="form-control">
                                    <option value>Select Size</option>
                                    <option value="20" {{old('size')!=null? (old('size') == 20?'selected':''):($importcargo->container->size == 20?'selected':'')}}>20</option>
                                    <option value="40" {{old('size')!=null? (old('size') == 40?'selected':''):($importcargo->container->size == 40?'selected':'')}}>40</option>
                                </select>
                                @error('size')
                                <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group col-md-3 @error('container_type') has-error @enderror">
                                <label>Container Type</label>
                                <select class="form-control " name="container_type" id="container_type">
                                    <option value>Select Container Type</option>
                                    @foreach ($itemtypes as $item)
                                        <option value="{{$item->id}}" {{old('container_type')!=null? (old('container_type') == $item->id?'selected':''):($importcargo->container->itemtype_Id==$item->id?'selected':'')}}>{{$item->name}}</option>
                                    @endforeach
                                </select>
                                @error('container_type')
                                    <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="col-sm-12">
                                <h4>Import Cargo Details <button type="button" class="btn btn-primary" id="btnAddDetail" style="float: right;">Add Detail</button></h4>
                            </div>
                            <div class="col-sm-12">
                                <table class="table table-striped table-bordered" style="margin-top:20px" id="importDetailTable">
                                    <thead>
                                        <tr>
                                            <th>Index</th>
                                            <th>Stowage on Board</th>
                                            <th>Marks & NOS</th>
                                            <th>Description</th>
                                            <th>Mesurment M3</th>
                                            {{-- <th>Size</th>
                                            <th>Type</th> --}}
                                            <th>Seal No</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (old('importdetailId') != null)
                                            @foreach (old('importdetailId') as $key => $oldValues)
                                                <tr>
                                                    <input type="hidden" name="importdetailId[]" value="{{$oldValues}}">
                                                    <td>
                                                        <div class="form-group @error('index.'.$key) has-error @enderror">
                                                            <input type="text" class="form-control " name="index[]" value="{{old('index.'.$key)}}" placeholder="Enter Index">
                                                        </div>
                                                        @error('index.'.$key)
                                                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                                        @enderror
                                                    </td>
                                                    <td>
                                                        <div class="form-group @error('stowage.'.$key) has-error @enderror">
                                                        <input type="text" class="form-control " value="{{old('stowage.'.$key)}}" name="stowage[]" placeholder="Enter Stowage on Board">
                                                        </div>
                                                        @error('stowage.'.$key)
                                                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                                        @enderror
                                                    </td>
                                                    <td>
                                                        <div class="form-group @error('marks.'.$key) has-error @enderror">
                                                        <input type="text" class="form-control " value="{{old('marks.'.$key)}}" name="marks[]" placeholder="Enter Marks">
                                                        </div>
                                                        @error('marks.'.$key)
                                                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                                        @enderror
                                                    </td>
                                                    <td>
                                                        <div class="form-group @error('description.'.$key) has-error @enderror">
                                                        <input type="text" class="form-control " value="{{old('description.'.$key)}}" name="description[]" placeholder="Enter Description">
                                                        </div>
                                                        @error('description.'.$key)
                                                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                                        @enderror
                                                    </td>
                                                    <td>
                                                        <div class="form-group @error('measurment.'.$key) has-error @enderror">
                                                        <input type="text" class="form-control " value="{{old('measurment.'.$key)}}" name="measurment[]" placeholder="Enter Mesurment">
                                                        </div>
                                                        @error('measurment.'.$key)
                                                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                                        @enderror
                                                    </td>
                                                    {{-- <td>
                                                        <div class="form-group @error('size.'.$key) has-error @enderror">
                                                        <input type="text" class="form-control " value="{{old('size.'.$key)}}" name="size[]" placeholder="Enter Size">
                                                        </div>
                                                        @error('size.'.$key)
                                                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                                        @enderror
                                                    </td>
                                                    <td>
                                                        <div class="form-group @error('type.'.$key) has-error @enderror">
                                                        <select class="form-control " name="type[]">
                                                            <option value>Select Item Type</option>
                                                            @foreach ($itemtypes as $item)
                                                                <option value="{{$item->id}}" {{old('type.'.$key) == $item->id?'selected':''}}>{{$item->name}}</option>
                                                            @endforeach
                                                        </select>
                                                        </div>
                                                        @error('type.'.$key)
                                                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                                        @enderror
                                                    </td> --}}
                                                    <td>
                                                        <div class="form-group @error('sealno.'.$key) has-error @enderror">
                                                        <input type="text" class="form-control " value="{{old('sealno.'.$key)}}" name="sealno[]" placeholder="Enter Seal No">
                                                        </div>
                                                        @error('sealno.'.$key)
                                                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                                        @enderror
                                                    </td>
                                                    <th>
                                                        <button type="button" class="btn btn-danger btnRowDelete" data-id="{{$oldValues}}"><i class="fa fa-trash"></i></button>
                                                    </th>
                                                </tr>
                                            @endforeach
                                        @else
                                            @foreach ($importcargo->importcargosdetail as $item)
                                                <tr>
                                                    <input type="hidden" name="importdetailId[]" value="{{$item->id}}">
                                                    <td>
                                                        <input type="text" class="form-control" value="{{$item->cargo_index}}" name="index[]" placeholder="Enter Index">
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" value="{{$item->stowageOnBoard}}" name="stowage[]" placeholder="Enter Stowage on Board">
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" value="{{$item->mark_no}}" name="marks[]" placeholder="Enter Marks">
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" value="{{$item->description}}" name="description[]" placeholder="Enter Description">
                                                    </td>
                                                    <td>
                                                        <input type="text" class="form-control" value="{{$item->measurment_m3}}" name="measurment[]" placeholder="Enter Measurment">
                                                    </td>
                                                    {{-- <td>
                                                        <input type="text" class="form-control" value="{{$item->size}}" name="size[]" placeholder="Enter Size">
                                                    </td>
                                                    <td>
                                                        <select class="form-control" name="type[]">
                                                            <option value>Select Item Type</option>
                                                            @foreach ($itemtypes as $itemtype)
                                                                <option value="{{$itemtype->id}}" {{$item->itemTypeId == $itemtype->id?'selected':''}}>{{$itemtype->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </td> --}}
                                                    <td>
                                                        <input type="text" class="form-control" value="{{$item->sealNo}}" name="sealno[]" placeholder="Enter Seal No">
                                                    </td>
                                                    <th>
                                                        <button type="button" class="btn btn-danger btnRowDelete" data-id="{{$item->id}}"><i class="fa fa-trash"></i></button>
                                                    </th>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn blue">Save</button>
                            <a href="{{route('import-cargo.index')}}" class="btn default">Cancel</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script type="text/html" id="data-row">
        <tr>
            <input type="hidden" name="importdetailId[]" value="0">
            <td>
                <input type="text" class="form-control" name="index[]" placeholder="Enter Index">
            </td>
            <td>
                <input type="text" class="form-control" name="stowage[]" placeholder="Enter Stowage on Board">
            </td>
            <td>
                <input type="text" class="form-control" name="marks[]" placeholder="Enter Marks">
            </td>
            <td>
                <input type="text" class="form-control" name="description[]" placeholder="Enter Description">
            </td>
            <td>
                <input type="text" class="form-control" name="measurment[]" placeholder="Enter Description">
            </td>
            <td>
                <input type="text" class="form-control" name="sealno[]" placeholder="Enter Seal No">
            </td>
            <th>
                <button type="button" class="btn btn-danger btnRowDelete" data-id="0"><i class="fa fa-trash"></i></button>
            </th>
        </tr>
    </script>
@endsection
@section('customJS')
<script src="{{asset('assets/global/plugins/jquery-repeater/jquery.repeater.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js')}}" type="text/javascript"></script>
    <script>
          getContainer = function(){
            textVal = $('#container').val();
            reqUrl = "{{route('container-exist')}}";
            $.getJSON(reqUrl+'/'+textVal,function(res){
                if(res.status == true)
                {
                    $('#size').val(res.data['size']);
                    $('#size').attr("style", "pointer-events: none;");
                    $('#container_type').val(res.data['itemtype_Id']);
                    $('#container_type').attr("style", "pointer-events: none;");
                }
                else
                {
                    $('#size').val('');
                    $('#size').attr("style", "pointer-events: auto;");
                    $('#container_type').val('');
                    $('#container_type').attr("style", "pointer-events: auto;");
                }
                
            });
        };
        $(document).ready(function(){
            $('.date').datepicker({
                format: 'yyyy-mm-dd'
            });
            getContainer();
        });
       
        $(document).on('keypress','#mv',function(){
            textVal = $(this).val();
            reqUrl = "{{route('vessel-fetch')}}";
            $.getJSON(reqUrl+'/'+textVal,function(data){
                if(data.status)
                {
                    $('#mvList').html('');
                    $.each(data.data,function(key,val){
                        $('#mvList').append('<option value="'+val.name+'"/>');
                    })
                }
            });
        });
        $(document).on('keypress','#container',function(){
            textVal = $(this).val();
            reqUrl = "{{route('container-fetch')}}";
            $.getJSON(reqUrl+'/'+textVal,function(data){
                if(data.status)
                {
                    $('#containerList').html('');
                    $.each(data.data,function(key,val){
                        $('#containerList').append('<option value="'+val.name+'"/>');
                    })
                }
            });
        });
        $(document).on('click','.btnRowDelete',function(){
            parentRow = $(this).parents('tr');
            if($(this).attr('data-id') == 0)
            {
                $(this).parents('tr').remove();
            }else
            {
                $recordId = $(this).attr('data-id');
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this import cargo detail",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel",
                    closeOnConfirm: true,
                    closeOnCancel: true
                    },
                    function(isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            url: "{{route('importdetal-delete')}}",
                            method: "POST",
                            dataType: "JSON",
                            data:{
                                id : $recordId
                            },
                            success:function(res){
                                $(parentRow).remove();
                            },
                            error:function(jhxr,err,code){
                                console.log(jhxr);
                            }
                        });
                    } else {
                        return
                    }
                });
            }
        });
        $(document).on('click','#btnAddDetail',function(){
            $('#importDetailTable').find('tbody').append($('#data-row').html());
        });

        $(document).on('blur','#container',getContainer);
    </script>
@endsection
