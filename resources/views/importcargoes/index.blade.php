@extends('layouts.app')
@section('customCSS')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap-toastr/toastr.min.css')}}" rel="stylesheet" type="text/css">
    <!-- END PAGE LEVEL PLUGINS -->

    <style>
        a.dt-button.buttons-print.btn.dark.btn-outline {
            display: none;
        }

        .btn-outline {
            display: none;
        }

        table.dataTable.dtr-inline.collapsed>tbody>tr>td:first-child, table.dataTable.dtr-inline.collapsed>tbody>tr>td:first-child::before{
            display: none;
        }
        table.dataTable.dtr-inline.collapsed>tbody>tr>td, table.dataTable>thead>tr>th , table.dataTable>tfoot>tr>th  {
            display: table-cell !important;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">View All Import Cargos</span>
                    </div>
                    <div class="tools"> </div>
                    <a href="{{route('import-cargo.create')}}" class="btn btn-danger" style="float:right">Add Import Cargo</a>
                </div>
                {{-- <a href="{{route('import-cargo.create')}}" class="btn btn-danger">Add Import Cargo</a> --}}
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="sample_table">
                        <thead>
                            <tr>
                                <th>Sr.No</th>
                                <th>Added By</th>
                                <th>Serial No</th>
                                <th>Voy</th>
                                <th>OF</th>
                                <th>Date of Measurement</th>
                                <th>Container</th>
                                <th>Vessel</th>
                                <th>Status</th>
                                <th style="min-width: 210px;">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $a =0;
                            @endphp
                            @foreach ($importcargos as $key => $item)
                                <tr>
                                    <td>{{++$a}}</td>
                                    <td>{{$item->user->name}}</td>
                                    <td>{{$item->serialNo}}</td>
                                    <td>{{$item->voy}}</td>
                                    <td>{{date_format(date_create($item->departureDate),"j M, Y")}}</td>
                                    <td>{{date_format(date_create($item->dom_des),"j M, Y")}}</td>
                                    <td>{{$item->container->name}}</td>
                                    <td>{{$item->vessel->name}}</td>
                                    <td>{{$item->status}}</td>
                                    <td>
                                        <a href="{{route('import-cargo.show',$item->id)}}" class="btn btn-info"><i class="fa fa-eye"></i></a>
                                        <a href="{{route('import-cargo.edit',$item->id)}}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                                        <a href="{{route('importCargo.invoice',['importId'=>$item->id,'status'=>45])}}" class="btn btn-danger">45 <i class="fa fa-print"></i></a>
                                        @if ($item->status == 23)
                                        <a href="{{route('importCargo.invoice',['importId'=>$item->id,'status'=>23])}}" class="btn btn-danger">23 <i class="fa fa-print"></i></a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
@endsection
@section('customJS')
    <script src="{{asset('assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{asset('assets/global/plugins/bootstrap-toastr/toastr.min.js')}}" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    <script>
        $(document).ready(function(){
            $('#sample_table').dataTable();
            @if (Session::has("message")) 
            toastr['success']("{{Session::get('message')}}", "Success Message")
            @endif
        })
    </script>
@endsection