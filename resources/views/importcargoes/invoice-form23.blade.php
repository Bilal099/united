<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="widtd=device-widtd, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        @page { margin: 10px; }
        body { margin: 10px; }
        *{
            font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
            font-size: 0.95em;
        }
        table.first-detail
        {
            width: 100%;
            border-collapse: collapse;
            border: none;
            margin: 10px 0px;
        }
        table.first-detail td
        {
            padding: 5px;
        }
        table.details
        {
            width: 100%;
            border-collapse: collapse;
            border: 1px black solid;
            margin-bottom: 20px
        }
        table.details td
        {
            padding: 5px;
            border: 1px black solid;
        }
        .text-center
        {
            text-align: center;
            
        }
        .h1-size{
            font-size: 1.5em;
        }
        .text-underline{
            text-decoration: underline;
        }
        .img-logo
        {
            width: 150px;
            height: auto;
            position: absolute;
        }
        .form-no
        {
            position: absolute;
            right: 10px;
        }
        .text-span-size
        {
            font-size: 1.1em;
        }
    </style>
</head>
<body>
    <img src="{{ public_path("images/unitedlogo.png") }}" class="img-logo">
    <p class="form-no"><b>Form No: 23</b></p>
    <h1 class="text-center h1-size">UNITED MARINE SURVEYORS (PVT) LTD.</h1>
    <p class="text-center">MARINE, FIRE AND MOTOR SURVEYORS, CERTIFIED IICL CONTAINER INSPECTORS, LIQUID<br>BULK & DRY CARGO INSPECTORS, NAUITICAL & ENGINEERING CONSULTANTS.</p>
    <p class="text-center">------------------------------------ (AN ISO 9001 : 2015 CERTIFIED COMPANY)--------------------------------------</p>
    <p class="text-center">Suite # 207, Shaheen Centre, Schon Circle, Kehkashan Block 7, Clifton, Karachi - 75600, Pakistan.<br>Tele: (92-21) 3586 1355-56 Fax: (92-21) 3586 1358 Email:info@ums.com.pk Web:http://www.ums.com.pk</p>
    <h2 class="text-underline text-center">DAILY MEASURMENT FOR IMPORT CARGO</h2>
    <table class="details">
        <tbody>
            <tr>
                <td><b>M.V.</b> <span class="text-underline text-span-size">{{$imports->vessel->name}}</span></td>
                <td><b>VOY</b> <span class="text-underline text-span-size">{{$imports->voy}}</span></td>
                <td><b>OF</b> <span class="text-underline text-span-size">{{date("d-M-Y",strtotime($imports->departureDate))}}</span></td>
            </tr>
            <tr>
                <td colspan="2"><b>DATE OF MEASURMENT/DE-STUFFING</b> <span class="text-underline text-span-size">{{date("d-M-Y",strtotime($imports->dom_des))}}</span></td>
                <td><b>CONTAINER NO.</b> <span class="text-underline text-span-size">{{$imports->container->name}}</span></td>
            </tr>
        </tbody>
    </table>
    <table class="details">
        <thead>
            <tr>
                <th>S.No</th>
                <th>INDEX #</th>
                <th>STOWAGE ON BOARD</th>
                <th>MARKS & NOS</th>
                <th>DESCRIPTION OF CARGO</th>
                <th>CONTAINER NO</th>
                <th>SIZE</th>
                <th>TYPE</th>
                <th>SEAL NO</th>
                <th>REMARKS</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($imports->importcargosdetail as $key => $item)
                <tr>
                    <td>{{++$key}}</td>
                    <td>{{$item->cargo_index}}</td>
                    <td>{{$item->stowageOnBoard}}</td>
                    <td>{{$item->mark_no}}</td>
                    <td>{{$item->description}}</td>
                    <td>{{$imports->container->name}}</td>
                    <td>{{$item->size}}</td>
                    <td>{{$item->itemTypeId}}</td>
                    <td>{{$item->sealNo}}</td>
                    <td>{{$item->remarks}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>