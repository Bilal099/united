@extends('layouts.app')


@section('content')
{{-- <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-lg" id="test">
    Launch Large Modal
  </button> --}}
{{-- <button id="test">TEST</button> --}}
<div class="m-heading-1 border-green m-bordered">
    <h3>United Marine Report Section</h3>
    @if (session('success'))
    <div id="" class="custom-alerts alert alert-success fade in">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <i class="fa-lg fa fa-warning"></i>
        {{session('success')}}
    </div>
    @elseif(session('error'))
    <div id="" class="custom-alerts alert alert-danger fade in">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <i class="fa-lg fa fa-warning"></i>
        {{session('error')}}
    </div>
    @endif
</div>
<div class="row">
    <div class="col-md-12">

        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="icon-settings font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase">Create Export Reports</span>
                </div>
            </div>
            <div class="portlet-body form">
                <form role="form" method="POST" action="{{route('report.export')}}" target="_blank" id="export_form">
                    @csrf
                    <input type="hidden" name="form_name" value="export_form">
                    <div class="form-body row">
                        {{-- <div class="form-group @error('name') has-error @enderror "> --}}
                        <div class="form-group col-md-6 col-lg-6 @error('export_vessel') has-error @enderror">
                            <label>SELECT VESSEL </label>
                            <select class="form-control" name="export_vessel" id="" required>
                                <option selected disabled>Select Option</option>
                                @foreach ($Vessel as $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                            @error('export_vessel')
                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6 col-lg-6 @error('export_container') has-error @enderror">
                            <label>SELECT CONTAINER </label>
                            <select class="form-control" name="export_container" id="" required>
                                <option selected disabled>Select Option</option>
                                @foreach ($Container as $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                            @error('export_container')
                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6 col-lg-6 @error('export_voy') has-error @enderror">
                            <label>VOYAGE</label>
                            <input type="text" class="form-control" placeholder="Enter voyage" name="export_voy" required>
                            @error('export_voy')
                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                            @enderror
                        </div>
                    </div>
                    <small>Note : (The Export report only show stuffing records.)</small>
                    <div class="form-actions">
                        <button type="submit" class="btn blue ref-save">Generate Export Report</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">

        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="icon-settings font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase">create import packaging report</span>
                </div>
            </div>
            <div class="portlet-body form">
                <form role="form" method="POST" action="{{route('report.import_package')}}" target="_blank" id="import_package_form">
                    @csrf
                    <input type="hidden" name="form_name" value="import_package_form">

                    <div class="form-body row">
                        <div class="form-group col-md-6 col-lg-6 @error('import_package_vessel') has-error @enderror">
                            <label>SELECT VESSEL </label>
                            <select class="form-control" name="import_package_vessel" id="">
                                <option selected disabled>Select Option</option>
                                @foreach ($Vessel as $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                            @error('import_package_vessel')
                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                            @enderror
                        </div>
                        <div
                            class="form-group col-md-6 col-lg-6 @error('import_package_container') has-error @enderror">
                            <label>SELECT CONTAINER </label>
                            <select class="form-control" name="import_package_container" id="">
                                <option selected disabled>Select Option</option>
                                @foreach ($Container as $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                            @error('import_package_container')
                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6 col-lg-6 @error('import_package_voy') has-error @enderror">
                            <label>VOYAGE</label>
                            <input type="text" class="form-control" placeholder="Enter voyage" name="import_package_voy"
                                required>
                            @error('import_package_voy')
                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                            @enderror
                        </div>
                    </div>
                    {{-- <small>Note : (The Export report only show stuffing records.)</small> --}}
                    <div class="form-actions">
                        <button type="submit" class="btn blue ref-save">Generate Export Report</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">

        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="icon-settings font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase">create import vehicle report</span>
                </div>
            </div>
            <div class="portlet-body form">
                <form role="form" method="POST" action="{{route('report.import_vehicle')}}" target="_blank" id="import_vehicle_form">
                    @csrf
                    <input type="hidden" name="form_name" value="import_vehicle_form">
                    
                    <div class="form-body row">
                        <div class="form-group col-md-6 col-lg-6 @error('import_vehicle_vessel') has-error @enderror">
                            <label>SELECT VESSEL </label>
                            <select class="form-control" name="import_vehicle_vessel" id="">
                                <option selected disabled>Select Option</option>
                                @foreach ($Vessel as $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                            @error('import_vehicle_vessel')
                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                            @enderror
                        </div>
                        <div
                            class="form-group col-md-6 col-lg-6 @error('import_vehicle_container') has-error @enderror">
                            <label>SELECT CONTAINER </label>
                            <select class="form-control" name="import_vehicle_container" id="">
                                <option selected disabled>Select Option</option>
                                @foreach ($Container as $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                            @error('import_vehicle_container')
                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-6 col-lg-6 @error('import_vehicle_voy') has-error @enderror">
                            <label>VOYAGE</label>
                            <input type="text" class="form-control" placeholder="Enter voyage" name="import_vehicle_voy"
                                required>
                            @error('import_vehicle_voy')
                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                            @enderror
                        </div>
                    </div>
                    {{-- <small>Note : (The Export report only show stuffing records.)</small> --}}
                    <div class="form-actions">
                        <button type="submit" class="btn blue ref-save">Generate Export Report</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modal-lg">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modal_title">Report</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" method="post">
                    @csrf
                    <input type="hidden" name="form_type_modal" value="" id="form_type_modal">
                    <div class="form-group col-md-6 col-lg-6">
                        <label>Vessel</label>
                        <select class="form-control" name="vessel_modal" id="vessel_modal" disabled>
                            <option selected disabled>Select Option</option>
                            @foreach ($Vessel as $item)
                            <option value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-6 col-lg-6">
                        <label>Container</label>
                        <select class="form-control" name="container_modal" id="container_modal" disabled>
                            <option selected disabled>Select Option</option>
                            @foreach ($Container as $item)
                            <option value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-6 col-lg-6">
                        <label>VOYAGE</label>
                        <input type="text" class="form-control" name="voy_modal" id="voy_modal" readonly>
                    </div>
                    <div class="form-group col-md-6 col-lg-6">
                        <label>Ref #</label>
                        <input type="text" class="form-control" name="ref_number" id="ref_number" required>
                    </div>
                
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" id="modal_form_btn">Save changes</button>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection

@section('customJS')
<script>
    $('.ref-save').click(function (e) {
        e.preventDefault();

        var vessel_modal;
        var container_modal;
        var voy_modal;
        var ref_number;

        var modal_form          = [ "form_type_modal", "vessel_modal", "container_modal", "voy_modal" ];
        var export_form         = [ "form_name", "export_vessel", "export_container", "export_voy" ];
        var import_package_form = [ "form_name", "import_package_vessel", "import_package_container", "import_package_voy"];
        var import_vehicle_form = [ "form_name", "import_vehicle_vessel", "import_vehicle_container", "import_vehicle_voy"];


        let formValue = $(this).closest('form').serialize().split("&");
        // console.log(formValue);
        let form = "#"+formValue[1].split("=")[1]; 
        if(formValue.length >= 5)
        {
            if(formValue[1].split("=")[1] == "export_form" )
            {
                $("#modal_title").text("Store Export Report");
                let _i = 0;
                formValue.forEach(value => {
                let temp = value.split("=");
                if(export_form[_i] == temp[0])
                {    
                    let temp_selector = "#"+modal_form[_i];
                        $(temp_selector).val(temp[1]);
                    _i++;
                }
                });
            }
            else if(formValue[1].split("=")[1] == "import_package_form" )
            {
                $("#modal_title").text("Store Import Package Report");
                let _i = 0;
                formValue.forEach(value => {
                let temp = value.split("=");
                if(import_package_form[_i] == temp[0])
                {    
                    let temp_selector = "#"+modal_form[_i];
                        $(temp_selector).val(temp[1]);
                    _i++;
                }
                });
            }
            else if(formValue[1].split("=")[1] == "import_vehicle_form" )
            {
                $("#modal_title").text("Store Import Vehicle Report");
                let _i = 0;
                formValue.forEach(value => {
                let temp = value.split("=");
                if(import_vehicle_form[_i] == temp[0])
                {    
                    let temp_selector = "#"+modal_form[_i];
                        $(temp_selector).val(temp[1]);
                    _i++;
                }
                });
            }

        

            swal({
                    title: "United Marine Report!",
                    text: `If you want to view report without saving reference then click on VIEW ONLY.
                        If you want to save report with reference and view click STORE AND VIEW.`,
                    // icon: "warning",
                    // buttons: true,
                    // button: "View Only!",
                    buttons: ["View Only!", "Store and View!"],
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        // swal("Poof! Your imaginary file has been deleted!", {
                        // // icon: "success",
                        // });
                        $('#modal-lg').modal('show');

                    } else {
                        // swal("Your imaginary file is safe!");
                        
                        $(form).submit();
                    }
                });
        }   
        else{
            alert("Please Select All Fields In Your Select Report Section!");
        }
    });

    $("#modal_form_btn").click(function (e) { 
        e.preventDefault();

        let vessel_modal       = $("#vessel_modal").val();
        let container_modal     = $("#container_modal").val();
        let voy_modal           = $("#voy_modal").val();
        let ref_number          = $("#ref_number").val();
        let form_type_modal     = $("#form_type_modal").val();

        let form = "#"+form_type_modal;

        $.ajax({
            type: "POST",
            url: "{{route('report.ref')}}",
            data: {
                _token              : "{{csrf_token()}}",
                vessel_modal       : vessel_modal,              
                container_modal     : container_modal,                
                voy_modal           : voy_modal,              
                ref_number          : ref_number,                 
                form_type_modal     : form_type_modal,                
            },
            success: function (response) {
                $('#modal-lg').modal('hide');
                console.log("response",response);
                if(response[0])
                {
                    if(response[1] == "ref_exist")
                    {
                        swal("Your report is already store with Ref # "+response[2]+".")
                        .then((value) => {
                                $(form).submit();
                                // swal("Your");
                        });
                    }
                    else{
                        $(form).submit();
                    }
                }
            }
        });
        
    });
</script>
@endsection
