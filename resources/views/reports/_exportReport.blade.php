<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <style>
        table,
        td,
        th {
            border: 1px solid #595959;
            border-collapse: collapse;
            font-size: 10px;
            font-family: Arial, Helvetica, sans-serif;
            text-align: center;
        }

        td,
        th {
            /* padding: 3px; */
            /* padding: 10px; */
            width: 50px;
            height: 25px;
        }

        th {
            background: #f0e6cc;
        }

        .even {
            background: #fbf8f0;
        }

        .odd {
            background: #fefcf9;
        }

        table,
        th,
        td,
        tr {}

        .img-logo {
            width: 150px;
            height: auto;
            position: absolute;
        }

        .text-center {
            text-align: center;

        }

        .h1-size {
            font-size: 1.7em;
        }

        .p-size {
            font-size: 0.7em;
        }

        .main-p-size {
            font-size: 0.6em;
        }

        .text-underline {
            text-decoration: underline;
        }

        @page {
            /* margin: 330px 25px 25px 25px; */
            margin: 25px 25px 25px 25px;

            font-family: Arial, Helvetica, sans-serif;

        }


        /** Define the header rules **/
        #header {
            position: fixed;
            
            
            /* color: #0d4175; */
            /* background-color: aqua; */
        }
        @page:first {
            @top-left {
            content: normal;
            }


        /** Define the footer rules **/
        #footer {
            position: fixed;
            top: 600px;
            bottom: 0cm;
            left: 0cm;
            right: 0cm;
            height: 6cm;
            /* background-color: aqua; */

        }

        .main{
            /* top: 250px;
            bottom: 600px;
            left: 0cm;
            right: 0cm; */
            /* height: 6cm; */
        }

    </style>
</head>

<body>
    <div id="header">
        <label>Abbas</label>
    </div>
    {{-- <div id="header"> --}}
        <div class="p-size" style="float: right; border: 1px solid; padding:0.5em;"> Form No. 18</div>
        <br>
        <div style="color:#0d4175; position:relative;">
            <div>
                <img src="{{ public_path("images/unitedlogo.png") }}" class="img-logo" style="width:90px; margin-top:-20px;">
            </div>
            <div style="">
            <h1 class="text-center h1-size" style="font-family:'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif; margin-bottom:-10px">UNITED MARINE SURVEYORS (PVT) LTD.</h1>
            <p class="text-center p-size" style="">MARINE, FIRE AND MOTOR SURVEYORS, CERTIFIED IICL CONTAINER INSPECTORS,
                LIQUID<br>BULK & DRY CARGO INSPECTORS, NAUITICAL & ENGINEERING CONSULTANTS.</p>
            <p class="text-center p-size" style="margin-top:-5px;">
                <strong> -------------------------------------------------------------- </strong>
                (AN ISO 9001 : 2015 CERTIFIED COMPANY)
                <strong> --------------------------------------------------------------</strong></p>
            <p style="text-align:center" class="p-size">Suite # 207, 2nd Floor Shaheen Centre, Block-7, Clifton,
                Karachi-75600, Pakistan.<br> 
             Tele:(92-21)3586 1355-56 Fax:(92-21)3586 1354 E-mail
                :info@ums.com.pk Web: http:www.ums.com.pk </p>
            </div>
        </div>
        <div style="" class="p-size">
            <p style="display: inline;">Ref: {{ isset($ref_number)? $ref_number:"xxxx-xx/xx-CM/PS"}}</p>
            <p style="display: inline;color:white">--------------------------------------------------------------------------------------------------------------------------------------</p>
            <p style="display: inline;text-align:right">Date : {{ @date_format(NOW(),"jS M,Y") }}</p>
        </div>
     
        <div style="">
            <p class="p-size" style="text-align: center;">
                <strong>
                M.V. "{{strtoupper(@$vessel_name)}}" VOY-{{@$voy}} OF {{ @date("jS M,Y", strtotime($report_details[0]->cargo_date)) }} AT KARACHI.
                <br>
                CARGO ARRIVAL / MEASUREMENT / CARGO CONDITION AT THE TIME OF ARRIVAL & STUUFING IN THE CONTAINER
                </strong>
            </p>
        </div>

        <p class="p-size" style="text-align: justify">
            <i>CERTIFIED</i> that upon receipt instructions from
            <strong>M/s PAK SHAHEEN CONTAINER SERVICES</strong>,
            we the undersigned surveyors, attended at
            <strong>Pak Shaheen Yard Keamari Karachi</strong>,
            on
            {{-- <strong>{{ @date("jS M,Y", strtotime($report[0]->exportcargosCertificate->date)) }} (Day)</strong> --}}
            <strong>{{ @date("jS M,Y", strtotime($date_of_insp)) }} (Day)</strong>
            for the purpose of Carrying out cargo arrival/ measurement/ cargo condition at the time of arrival &
            stuffing in the containers We report as under:
        </p>
    {{-- </div> --}}

    



    <div class="main" style="page-break-after:auto;">
        <table style="width: 100%">
            <tbody>
                <tr>
                    <th colspan="2" rowspan="2">Client <br>
                        <br>
                        location</th>
                    <td colspan="7" rowspan="2" style="text-align: left !important;"> 
                        {{-- : {{ @$client }} --}}
                        : M/s PAK SHAHEEN CONTAINER SERVICES
                        <br>
                        <br>
                        {{-- : {{ @$location }} --}}
                        : PSCS KEAMARI YARD
                    </td>
                    <th>Vessel Name</th>
                    <td>{{ strtoupper(@$vessel_name) }}</td>
                </tr>
                <tr>
                    <th>Voyage</th>
                    <td>{{ @$voy }}</td>
                </tr>
                <tr>
                    <th colspan="2" rowspan="2">Container no.</th>
                    <td colspan="4" rowspan="2">{{ strtoupper(@$container) }}</td>
                    <th rowspan="2">Container type</th>
                    <td colspan="2" rowspan="2">{{ strtoupper(@$container_type) }}</td>
                    <th>Destination</th>
                    <td>{{ strtoupper(@$port) }}</td>
                </tr>
                <tr>
                    <th>Date of INSP</th>
                    <td style="">{{ @date("jS M,Y", strtotime($date_of_insp)) }}</td>
                </tr>
                <tr>
                    <th>S.NO</th>
                    <th>DATE</th>
                    <th>SHIPPER/ <br> CLEARING <br>AGENT</th>
                    <th>CONSIGNEE <br> NAME</th>
                    <th>DESCRIPTION <br> OF <br> CARGO</th>
                    <th>MARKS &amp; NO. </th>
                    <th>NO. OF PACKAGES</th>
                    <th>CUBIC M3</th>
                    <th>TYPE <br> OF <br> PACKAGE</th>
                    <th colspan="2">REMARKS</th>
                </tr>
                @php
                $i = 1;
                $total_noOfPackages = 0;
                $total_cubeM3 = 0;
                @endphp
                @foreach ($report_details as $item)
                {{-- @foreach ($item->exportcargosdetail as $exportcargosdetail) --}}
                <tr>
                    <td>{{ $i++ }}</td>
                    <td>{{ @date("jS M,Y", strtotime($item->cargo_date)) }}</td>
                    <td>{{ @$item->agent}}</td>
                    <td>{{ @$item->frieghtforwarder }}</td>
                    <td>{{ @$item->description }}</td>
                    <td>{{ @$item->marksAndNo }}</td>
                    @if (@($item->mean_check == 'yes'))
                        <td>{{ @($item->noOfPackages_sum/$item->num_of_rows) }}</td>
                        <td>{{ @($item->avg_sum/$item->num_of_rows) }} </td>
                    @else
                        <td>{{ @$item->noOfPackages_sum }}</td>
                        <td>{{ @$item->avg_sum }} </td>
                    @endif
                    
                    <td>{{ @$item->description }}</td>
                    <td colspan="2">{{ @$item->remarks }}</td>
                </tr>
                @php
                    if ($item->mean_check == 'yes')
                    {
                        $total_noOfPackages += @($item->noOfPackages_sum/$item->num_of_rows);
                        $total_cubeM3       += @($item->avg_sum/$item->num_of_rows);
                    }
                    else{
                        $total_noOfPackages += @$item->noOfPackages_sum;
                        $total_cubeM3       += @$item->avg_sum;
                    }
                
                @endphp
                {{-- @endforeach --}}
            
                @endforeach
                {{-- @foreach ($report as $item)
                    @foreach ($item->exportcargosdetail as $exportcargosdetail)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ @date("jS M,Y", strtotime($exportcargosdetail->updated_at)) }}</td>
                        <td>{{ @$item->agent->name}}</td>
                        <td>{{ @$item->frieghtforwarder->name }}</td>
                        <td>{{ @$exportcargosdetail->description }}</td>
                        <td>{{ @$exportcargosdetail->marksAndNo }}</td>
                        <td>{{ @$exportcargosdetail->noOfPackages }}</td>
                        <td>{{ @$exportcargosdetail->average }} </td>
                        <td>{{ @$exportcargosdetail->description }}</td>
                        <td colspan="2">{{ @$exportcargosdetail->remarks }}</td>
                    </tr>
                    @php
                    $total_noOfPackages += @$exportcargosdetail->noOfPackages;
                    $total_cubeM3       += @$exportcargosdetail->average;
                    @endphp
                    @endforeach
                
                @endforeach --}}

                @for ($j = $i; $j < 25; $j++) 
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td colspan="2"></td>
                    </tr>

                    @endfor
                    <tr> 
                        <td style="border: none !important;"></td>
                        <td style="border: none !important;"></td>
                        <td style="border: none !important;"></td>
                        <th colspan="3">TOTAL NO OF PACKAGES</th>
                        <td>{{ @$total_noOfPackages }}</td>
                        <td>{{ @$total_cubeM3 }}</td>
                        <th colspan="3">TOTAL CUBIC M3</th>

                    </tr>

            </tbody>
        </table>

    </div>

    {{-- <div id="footer"> --}}
        <p class="main-p-size"> NOTES </p>
        <p class="main-p-size">
            1. The seal of the above container was found intact / sound prior to Stuffing in the container
            <br>
            2. The above survey relates to external condition of the cargi and does not reflect the number of packages /
            or content
            <br>
            The above findings are correct and best of our knowledge at the time and place of survey only.
            <br>
            <strong>ISSUED WITHOUT PREJUDICE</strong>, to the rights/ liabilities of whomsoever may concerned
        </p>
        <br>
        <br>
        <p style="text-align: right"><i>for</i> <strong>UNITED MARINE SURVEYORS (PVT) LTD</strong></p>
        <p class="main-p-size" style="text-align:center;">
            This repors is subject to the condition that is it understood and agreed that neither the firm, nor any of
            its surveyors in unber any circumstances whatsoever, to be held responsible for

            any inaccuracy in any report or certificate issued bt this firm or its surveyors/ agent for any error of
            judgment, default or negligence.
        </p>


    {{-- </div> --}}


</body>

</html>
