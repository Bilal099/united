<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <style>
        table,
        td,
        th {
            border: 1px solid #595959;
            border-collapse: collapse;
            font-size: 9px;
            font-family: Arial, Helvetica, sans-serif;
            text-align: center;
        }

        td,
        th {
            /* padding: 3px; */
            /* padding: 10px; */
            width: 50px;
            height: 25px;
        }

        th {
            background: #f0e6cc;
        }

        .even {
            background: #fbf8f0;
        }

        .odd {
            background: #fefcf9;
        }

        table,
        th,
        td,
        tr {}

        .img-logo {
            width: 150px;
            height: auto;
            position: absolute;
        }

        .text-center {
            text-align: center;

        }

        .h1-size {
            font-size: 1.5em;
        }

        .p-size {
            font-size: 0.7em;
        }

        .main-p-size {
            font-size: 0.6em;
        }

        .text-underline {
            text-decoration: underline;
        }

        @page {
            margin: 200px 40px 180px 40px;
            font-family: Arial, Helvetica, sans-serif;

        }


        /** Define the header rules **/
        #header {
            position: fixed;
            top: -190px;
            left: 0cm;
            right: 0cm;
            height: 5cm;
            /* background-color: aqua; */
        }



        /** Define the footer rules **/
        #footer {
            position: fixed;
            top: 700px;
            bottom: 0cm;
            left: 0cm;
            right: 0cm;
            height: 5cm;
            /* background-color: aqua; */

        }

        .main{
           
        }

    </style>
</head>

<body>
    <div id="header">
        <img src="{{ public_path("images/unitedlogo1.png") }}" class="img-logo" style="width:90px;margin-top: 10px;">
        <h1 class="text-center h1-size" style="margin-top: 40px; font-size:1.5em; color: #594F97;">UNITED MARINE SURVEYORS (PVT) LTD.</h1>
        <p class="text-center p-size" style="color: #594F97;"><i>MARINE, FIRE AND MOTOR SURVEYORS, CERTIFIED IICL CONTAINER INSPECTORS,
            LIQUID<br>BULK & DRY CARGO INSPECTORS, NAUTICAL & ENGINEERING CONSULTANTS.</i>
            <br>
            <strong>--------------------------------------------------------------</strong><i>(AN ISO 9001 : 2015 CERTIFIED COMPANY)</i><strong>--------------------------------------------------------------</strong>
            <br>
            Suite # 207, 2nd Floor Shaheen Centre, Block-7, Clifton,
            Karachi-75600, Pakistan. 
            <br> 
            Tele:(92-21)3586 1355-56 Fax:(92-21)3586 1354 E-mail
            :info@ums.com.pk Web: http:www.ums.com.pk </p>

        <div style="" class="p-size">
            <p style="width:100%;"> <span style="font-style: bold"> Ref: {{ isset($ref_number)? $ref_number:"xxxx-xx/xx-CM/PS"}} </span>
            {{-- <span style="float:right; font-style: bold">Date : {{ @date_format(NOW(),"jS M,Y") }}</span></p> --}}
            <span style="float:right; font-style: bold">Date : {{ @($report_created_date != "")? date_format($report_created_date,"jS M,Y"):date_format(NOW(),"jS M,Y") }}</span></p>
        </div>
     
    </div>

    <div id="footer">
        <br>
        <p class="main-p-size" style="font-style: bold"> NOTES:- </p>
        <p class="main-p-size">
            1. The seal of the above container was found intact / sound prior to Stuffing in the container
            <br>
            2. The above survey relates to external condition of the cargo and does not reflect the number of packages /
            or content
            <br>
            The above findings are correct and best of our knowledge at the time and place of survey only.
            <br>
            <strong>ISSUED WITHOUT PREJUDICE</strong>, to the rights/ liabilities of whosoever may concern
        </p>
        <p style="text-align: right"><i>for</i> <strong>UNITED MARINE SURVEYORS (PVT) LTD</strong>
            <br>
            <strong style="font-size: x-small">(PSCS-OPS-3-10)</strong>
        </p>
        <hr>
        <p class="main-p-size" style="text-align:center;">
            This report is subject to the condition that is it understood and agreed that neither the firm, nor any of
            its surveyors in under any circumstances whatsoever, to be held responsible for

            any inaccuracy in any report or certificate issued by this firm or its surveyors/ agent for any error of
            judgment, default or negligence.
        </p>


    </div>



    <div class="main" style="page-break-after:auto;">
        <div style="">
            <p class="p-size" style="text-align: center;">
                <strong>
                M.V. "{{strtoupper(@$vessel_name)}}" VOY-{{@$voy}} OF {{ @date("jS M,Y", strtotime($report_details[0]->cargo_date)) }} AT KARACHI.
                <br>
                CARGO ARRIVAL / MEASUREMENT / CARGO CONDITION AT THE TIME OF ARRIVAL & STUFFING IN THE CONTAINER
                </strong>
            </p>
        </div>

        <p class="p-size" style="text-align: justify">
            <i>CERTIFIED</i> that upon receipt instructions from
            <strong>M/s PAK SHAHEEN CONTAINER SERVICES KARACHI</strong>,
            we the undersigned surveyors, attended at
            <strong>Pak Shaheen Yard Keamari Karachi</strong>,
            on
            <strong>{{ @date("jS M,Y", strtotime($report[0]->exportcargosCertificate->date)) }} (Day)</strong>
         
            for the purpose of carrying out cargo arrival/ measurement/ cargo condition at the time of arrival &
            stuffing in the containers We report as under:-
        </p>
        <table style="width: 100%; font-size:0.3em">
            <tbody>
                <tr>
                    <th colspan="2" rowspan="2">CLIENT <br>
                        <br>
                        LOCATION</th>
                    <td colspan="6" rowspan="2" style="text-align: left !important;"> 
                        : M/s PAK SHAHEEN CONTAINER SERVICES (PVT) LTD.
                        <br>
                        <br>
                        : PSCS KEAMARI YARD
                    </td>
                    <th colspan="2">VESSEL NAME</th>
                    <td colspan="2">{{ strtoupper(@$vessel_name) }}</td>
                </tr>
                <tr>
                    <th colspan="2">VOYAGE</th>
                    <td colspan="2">{{ @$voy }}</td>
                </tr>
                <tr>
                    <th colspan="2" rowspan="2">CONTAINER NO.</th>
                    <td colspan="4" rowspan="2">{{ strtoupper(@$container) }}</td>
                    <th rowspan="2">CONTAINER TYPE</th>
                    <td colspan="1" rowspan="2">{{ $container_size."'".strtoupper(@$container_type) }}</td>
                    <th colspan="2">DESTINATION</th>
                    <td colspan="2">{{ strtoupper(@$port) }}</td>
                </tr>
                <tr>
                    <th colspan="2">DATE OF INSP</th>
                    <td colspan="2">{{ @date("jS M,Y", strtotime($report[0]->exportcargosCertificate->date)) }}</td>
                </tr>
                <tr style="width: 100%;">
                    <th style="width: 6.6%">S.NO</th>
                    <th style="width: 6.6%">DATE</th>
                    <th style="width: 6.6%">SHIPPER/ <br> CLEARING <br>AGENT</th>
                    <th style="width: 6.6%">CONSIGNEE <br> NAME</th>
                    <th style="width: 6.6%">DESCRIPTION <br> OF <br> CARGO</th>
                    <th style="width: 6.6%">MARKS &amp; NO. </th>
                    <th style="width: 6.6%">NO. OF PACKAGES</th>
                    <th style="width: 6.6%">CUBIC M3</th>
                    <th style="width: 6.6%">TYPE <br> OF <br> PACKAGE</th>
                    <th style="width: 40%;" colspan="3">REMARKS</th>
                </tr>
                @php
                $i = 1;
                $total_noOfPackages = 0;
                $total_cubeM3 = 0;
                @endphp
                @foreach ($report_details as $item)

                @if ($i == 13 || $i == 30 )
                    </tbody>
                </table>
                </div>
                <div style="page-break-before: always">
                    <table style="width: 100%; margin-top: -60px">
                    <tbody>
                    <tr style="visibility: hidden">
                        <th style="height:1%" colspan="2" rowspan="2">
                            CLIENT 
                            <br>
                            <br>
                            LOCATION
                        </th>
                        <td style="height:1%; text-align: left !important;" colspan="3" rowspan="2">
                            : M/s PAK SHAHEEN CONTAINER SERVICES (PVT) LTD.
                            <br>
                            <br>
                            : PSCS KEAMARI YARD
                        </td>
                        <th style="height:0.5%">VESSEL NAME</th>
                        <td style="height:0.5%">{{ strtoupper(@$vessel_name) }}</td>
                    </tr>
                    <tr style="visibility: hidden">
                        <th style="height:0.5%">VOYAGE</th>
                        <td style="height:0.5%">{{ @$voy }}</td>
                    </tr>
                    <tr style="visibility: hidden">
                        <th colspan="2" rowspan="1">CONTAINER NO.</th>
                        <td colspan="1" rowspan="1">{{ strtoupper(@$container) }}</td>
                        <th rowspan="1">CONTAINER TYPE</th>
                        <td colspan="1" rowspan="1">{{ @$container_size."'".strtoupper(@$container_type) }}</td>
                        <th>DATE OF INSP</th>
                        <th>{{ @date("jS M,Y", strtotime($report[0]->dom_des)) }}</th>
                    </tr>
                    <tr style="width: 100%;">
                        {{-- <th style="width: 10%">S.NO</th>
                        <th style="width: 10%">INDEX</th>
                        <th style="width: 10%">Marks & NO.</th>
                        <th style="width: 10%">TYPE & NO. PACKAGES</th>
                        <th style="width: 10%">CUBIC M3</th>
                        <th style="width: 20%" colspan="2">REMARKS</th> --}}
                        <th style="width: 6.6%">S.NO</th>
                        <th style="width: 6.6%">DATE</th>
                        <th style="width: 6.6%">SHIPPER/ <br> CLEARING <br>AGENT</th>
                        <th style="width: 6.6%">CONSIGNEE <br> NAME</th>
                        <th style="width: 6.6%">DESCRIPTION <br> OF <br> CARGO</th>
                        <th style="width: 6.6%">MARKS &amp; NO. </th>
                        <th style="width: 6.6%">NO. OF PACKAGES</th>
                        <th style="width: 6.6%">CUBIC M3</th>
                        <th style="width: 6.6%">TYPE <br> OF <br> PACKAGE</th>
                        <th style="width: 40%;" colspan="3">REMARKS</th>
                    </tr>
                @endif

                <tr>
                    <td>{{ $i++ }}</td>
                    <td>{{ @date("jS M,Y", strtotime($item->cargo_date)) }}</td>
                    <td>{{ @strtoupper($item->agent)}}</td>
                    <td>{{ @strtoupper($item->frieghtforwarder) }}</td>
                    <td>{{ @$item->description }}</td>
                    <td>{{ @$item->marksAndNo }}</td>
                    @if (@($item->mean_check == 'yes'))
                        <td>{{ @($item->noOfPackages_sum/$item->num_of_rows) }}</td>
                        {{-- <td>{{ @($item->avg_sum/$item->num_of_rows) }} </td> --}}
                        <td>{{ @number_format((float)($item->avg_sum/$item->num_of_rows) , 3, '.', '')}}</td>
                    @else
                        <td>{{ @$item->noOfPackages_sum }}</td>
                        {{-- <td>{{ @$item->avg_sum }} </td> --}}
                        <td>{{ @number_format((float)($item->avg_sum) , 3, '.', '')}}</td>

                    @endif
                    
                    <td >{{ @$item->description }}</td>
                    <td colspan="3" style="width:10%; text-align:left !important">{{ @$item->remarks }}</td>
                </tr>
                @php
                    if ($item->mean_check == 'yes')
                    {
                        $total_noOfPackages += @($item->noOfPackages_sum/$item->num_of_rows);
                        $total_cubeM3       += @($item->avg_sum/$item->num_of_rows);
                    }
                    else{
                        $total_noOfPackages += @$item->noOfPackages_sum;
                        $total_cubeM3       += @$item->avg_sum;
                    }
                
                @endphp
            
                @endforeach
                {{-- @foreach ($report as $item)
                    @foreach ($item->exportcargosdetail as $exportcargosdetail)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ @date("jS M,Y", strtotime($exportcargosdetail->updated_at)) }}</td>
                        <td>{{ @$item->agent->name}}</td>
                        <td>{{ @$item->frieghtforwarder->name }}</td>
                        <td>{{ @$exportcargosdetail->description }}</td>
                        <td>{{ @$exportcargosdetail->marksAndNo }}</td>
                        <td>{{ @$exportcargosdetail->noOfPackages }}</td>
                        <td>{{ @$exportcargosdetail->average }} </td>
                        <td>{{ @$exportcargosdetail->description }}</td>
                        <td colspan="2">{{ @$exportcargosdetail->remarks }}</td>
                    </tr>
                    @php
                    $total_noOfPackages += @$exportcargosdetail->noOfPackages;
                    $total_cubeM3       += @$exportcargosdetail->average;
                    @endphp
                    @endforeach
                
                @endforeach --}}

                @for ($j = $i; $j < 10; $j++) 
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td colspan="3"></td>
                    </tr>

                    @endfor
                    <tr> 
                        <td style="border: none !important;"></td>
                        <td style="border: none !important;"></td>
                        <td style="border: none !important;"></td>
                        <th colspan="3">TOTAL NO OF PACKAGES</th>
                        <td>{{ @$total_noOfPackages }}</td>
                        <td>{{ @number_format((float)($total_cubeM3) , 3, '.', '') }}</td>
                        <th colspan="4">TOTAL CUBIC M3</th>

                    </tr>

            </tbody>
        </table>

    </div>

</body>

</html>
