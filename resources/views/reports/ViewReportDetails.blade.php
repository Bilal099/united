@extends('layouts.app')

@section('customCSS')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap-toastr/toastr.min.css')}}" rel="stylesheet" type="text/css">
    <!-- END PAGE LEVEL PLUGINS -->

    <style>
        a.dt-button.buttons-print.btn.dark.btn-outline {
            display: none;
        }

        .btn-outline {
            display: none;
        }
    </style>
@endsection

@section('content')
<div class="m-heading-1 border-green m-bordered">
    <h3>United Marine Report Section</h3>
    @if (session('success'))
    <div id="" class="custom-alerts alert alert-success fade in">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <i class="fa-lg fa fa-warning"></i>
        {{session('success')}}
    </div>
    @elseif(session('error'))
    <div id="" class="custom-alerts alert alert-danger fade in">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        <i class="fa-lg fa fa-warning"></i>
        {{session('error')}}
    </div>
    @endif
</div>
<div class="row">
    <div class="col-md-12">

        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="icon-settings font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase">View All Reports</span>
                </div>
            </div>
            <div class="portlet-body form">
                <table class="table table-striped table-bordered table-hover" id="sample_table">
                    <thead>
                        <tr>
                            {{-- <th style="display: none"></th>
                            <th style="display: none"></th> --}}
                            <th>S No.</th>
                            <th> Report Type </th>
                            <th> Vessel </th>
                            <th> Container </th>
                            <th> VOY </th>
                            <th> Ref # </th>
                            <th> Action </th>
                            
                        </tr>
                    </thead>
                    <tbody> 
                        @php
                            $_i = 1;
                            function returnReportName($report_type)
                            {
                                if($report_type == "export_form")
                                {
                                    return "Export Report";
                                }
                                elseif($report_type == "import_vehicle_form")
                                {
                                    return "Import Vehicle Report";
                                }
                                elseif ($report_type == "import_package_form") 
                                {
                                    return "Import Package Report";
                                }
                            }

                            function pdfRoute($report_type)
                            {
                                if($report_type == "export_form")
                                {
                                    return route("report.export");
                                }
                                elseif($report_type == "import_vehicle_form")
                                {
                                    return route("report.import_vehicle");
                                }
                                elseif ($report_type == "import_package_form") 
                                {
                                    return route("report.import_package");
                                }
                            }
                        @endphp
                        @foreach ($report as $item)
                        <tr>
                            <form action="{{pdfRoute($item->report_type)}}" method="post" target="_blank">
                                @csrf
                                <input type="hidden" name="view_detail_report" value="true">
                                <input type="hidden" name="vessel_id" value="{{$item->vessel_id}}">
                                <input type="hidden" name="container_id" value="{{$item->container_id}}">
                                <input type="hidden" name="voy" value="{{$item->voy}}">
                            <td>{{$_i++}}</td>
                            <td class="report_type">{{returnReportName($item->report_type)}}</td>
                            <td class="vessel">{{$item->vessel->name}}</td>
                            <td class="container">{{$item->container->name}}</td>
                            <td class="voy">{{$item->voy}}</td>
                            <td class="ref">{{$item->ref_number}}</td>
                            <td style="width: 10%"> 
                                <button type="submit" class="btn btn-info"><i class="fa fa-eye"></i></button> 
                                <a href="#" class="btn btn-danger ml-2 detele_btn" data-id="{{$item->id}}"><i class="fas fa-trash-alt"></i></a>
                            </td>
                            
                        </form>
                        </tr>
                        @endforeach
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>




@endsection

@section('customJS')
<script src="{{asset('assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{asset('assets/global/plugins/bootstrap-toastr/toastr.min.js')}}" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    <script>
        $(document).ready(function(){
            $('#sample_table').dataTable();
            
        });


        $(".view-report").click(function (e) { 
            e.preventDefault();

            let _this = $(this);
            let form_type = _this.closest('tr').find('.report_type').text();

            let vessel_id = _this.closest('tr').find('.vessel_id').text();
            let container_id = _this.closest('tr').find('.container_id').text();
            let voy = _this.closest('tr').find('.voy').text();

            if(form_type == "Export Report")
            {
                $.ajax({
                    type: "POST",
                    url: "{{route('report.export')}}",
                    data: {
                        _token: "{{csrf_token()}}",
                        export_vessel: vessel_id,
                        export_container: container_id,
                        export_voy: voy,
                    },
                    success: function (response) {
                        // window.open(response, '_blank');
                        // return response;
                        // var w = window.open('about:blank');
                        // w.document.open();
                        // w.document.write(response+'.pdf', '_blank', 'fullscreen=yes');
                        // w.document.close();

                        downloadFile(response);
                    }
                });
            }
            else if(form_type == "Import Vehicle Report")
            {
                
            }
            else if(form_type == "Import Package Report")
            {

            }

            // console.log('abc',tr);
            // let form_type = tr[1].toString(); //.replace("td>", "").replace("<", "").replace("/", "");
            // console.log('xyz',form_type);



            // let vessel = tr[2].toString();
            // let container = 

            // console.log("tr",tr[1]);
            // if(temp.indexOf("Package"))
            // {
            //     console.log('han bhai');
            // }

        });



        function downloadFile(response) {
        var blob = new Blob([response], {type: 'application/pdf'})
        var url = URL.createObjectURL(blob);
        location.assign(url);
        } 

        $(".detele_btn").click(function (e) { 
            e.preventDefault();
            let id = $(this).data('id');
            swal({
                title: "Are you sure?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
            if (willDelete) {
                
                $.ajax({
                    type: "post",
                    url: "{{route('report.delete')}}",
                    data: {
                        _token: "{{ csrf_token() }}",
                        id: id,
                    },
                    success: function (response) {
                        alert(response[1]);
                        location.reload();
                    }
                });
                
            } else {
                swal("Record in not delete!");
            }
            });
        });


    </script>


@endsection
