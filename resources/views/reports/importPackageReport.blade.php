<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <style>
        table,
        td,
        th {
            border: 1px solid #595959;
            border-collapse: collapse;
            font-size: 11px;
            font-family: Arial, Helvetica, sans-serif;
            text-align: center;
        }

        td,
        th {
            padding: 3px;
            width: 30px;
            height: 25px;
        }

        th {
            background: #f0e6cc;
        }

        .even {
            background: #fbf8f0;
        }

        .odd {
            background: #fefcf9;
        }

        table,
        th,
        td,
        tr {}

        .img-logo {
            width: 150px;
            height: auto;
            position: absolute;
        }

        .text-center {
            text-align: center;

        }

        .h1-size {
            font-size: 1.5em;
        }

        .p-size {
            font-size: 0.7em;
        }

        .main-p-size {
            font-size: 0.6em;
        }

        .text-underline {
            text-decoration: underline;
        }

        @page {
            margin: 200px 25px 180px 25px;
            font-family: Arial, Helvetica, sans-serif;

        }


        /** Define the header rules **/
        #header {
            position: fixed;
            top: -190px;
            left: 0cm;
            right: 0cm;
            height: 5cm;
        }



        /** Define the footer rules **/
        #footer {
            position: fixed;
            top: 720px;
            bottom: 0cm;
            left: 0cm;
            right: 0cm;
            height: 5cm;

        }

    </style>
</head>

<body>
    <div id="header">
        <img src="{{ public_path("images/unitedlogo1.png") }}" class="img-logo" style="width:90px;margin-top: 10px;">
        <h1 class="text-center h1-size" style="margin-top: 40px; font-size:1.5em; color: #594F97;">UNITED MARINE SURVEYORS (PVT) LTD.</h1>
        <p class="text-center p-size" style="color: #594F97;"> <i> MARINE, FIRE AND MOTOR SURVEYORS, CERTIFIED IICL CONTAINER INSPECTORS,
            LIQUID<br>BULK & DRY CARGO INSPECTORS, NAUTICAL & ENGINEERING CONSULTANTS.</i>
            <br>
            --------------------------------------------------------------<i>(AN ISO 9001 : 2015 CERTIFIED
            COMPANY)</i>--------------------------------------------------------------
            <br>
            Suite # 207, 2nd Floor Shaheen Centre, Block-7, Clifton,
            Karachi-75600, Pakistan. 
            <br>
            Tele:(92-21)3586 1355-56 Fax:(92-21)3586 1354 E-mail
            :info@ums.com.pk Web: http:www.ums.com.pk </p>

            

            <div style="" class="p-size">
                <p style="width:100%;"> <span style="font-style: bold"> Ref: {{ isset($ref_number)? $ref_number:"xxxx-xx/xx-CM/PS"}} </span>
                <span style="float:right; font-style: bold">Date : {{ ($report_created_date != "")? date_format($report_created_date,"jS M,Y"):date_format(NOW(),"jS M,Y") }}</span></p>
            </div>
    </div>

    <div id="footer">
        <p class="main-p-size" style="font-style: bold"> NOTES </p>
        <p class="main-p-size">
            1. The seal of the above container was found intact / sound prior to De-Stuffing in the container
            <br>
            2. The above survey relates to external condition of the cargo and does not reflect the number of packages /
            or content
            <br>
            The above findings are correct and best of our knowledge at the time and place of survey only.
            <br>
            <strong>ISSUED WITHOUT PREJUDICE</strong>, to the rights/ liabilities of whosoever may concern
        </p>
        <p style="text-align: right"><i>for</i> <strong>UNITED MARINE SURVEYORS (PVT) LTD</strong>
            <br>
            <strong style="font-size: x-small">(PSCS-OPS-3-10)</strong>
        </p>
        <hr>
        <p class="main-p-size" style=" text-align:center;">
            This repors is subject to the condition that is it understood and agreed that neither the firm, nor any of
            its surveyors in under any circumstances whatsoever, to be held responsible for

            any inaccuracy in any report or certificate issued by this firm or its surveyors/ agent for any error of
            judgment, default or negligence.
        </p>


    </div>



    <div class="main" style="page-break-after:auto;">
        <div style="">
            <p class="p-size" style="text-align: center;">
               <u>
                <strong>
                M.V. "{{strtoupper(@$vessel_name)}}" VOY-{{@$voy}} OF {{ @date("jS M,Y", strtotime($date_of_insp)) }} AT KARACHI.
                <br>
                CARGO CONDITION SURVEY REPORT AT THE TIME OF CONTAINER DE-STUFFING/MEASUREMENT.
                </strong>
            </u>

            </p>
        </div>

    <p class="p-size">
        <i>CERTIFIED</i> that upon receipt of instructions from
        <strong>M/s PAK SHAHEEN CONTAINER SERVICES KARACHI</strong>,
        we the undersigned surveyors, attended at
        <strong>Pak Shaheen Yard Keamari Karachi</strong>,
        on
        <strong>{{ @date("jS M,Y", strtotime($report[0]->dom_des)) }} (Day)</strong>
        for the purpose of Carrying out cargo condition survey report at the time of De-stuffing of cargo from the containers. We report as under:
    </p>
        <table style="width: 100%">
            <tbody>
                <tr>
                    <th style="height:1%" colspan="2" rowspan="2">
                        CLIENT 
                        <br>
                        <br>
                        LOCATION
                    </th>
                    <td style="height:1%; text-align: left !important;" colspan="3" rowspan="2">
                        : M/s PAK SHAHEEN CONTAINER SERVICES (PVT) LTD.
                        <br>
                        <br>
                        : PSCS KEAMARI YARD
                    </td>
                    <th style="height:0.5%">VESSEL NAME</th>
                    <td style="height:0.5%">{{ strtoupper(@$vessel_name) }}</td>
                </tr>
                <tr>
                    <th style="height:0.5%">VOYAGE</th>
                    <td style="height:0.5%">{{ @$voy }}</td>
                </tr>
                <tr>
                    <th colspan="2" rowspan="1">CONTAINER NO.</th>
                    <td colspan="1" rowspan="1">{{ strtoupper(@$container) }}</td>
                    <th rowspan="1">CONTAINER TYPE</th>
                    <td colspan="1" rowspan="1">{{ @$container_size."'".strtoupper(@$container_type) }}</td>
                    <th>DATE OF INSP</th>
                    <th>{{ @date("jS M,Y", strtotime($report[0]->dom_des)) }}</th>
                </tr>
                <tr>
                    <th>S.NO</th>
                    <th>INDEX</th>
                    <th>Marks & NO.</th>
                    <th>TYPE & NO. PACKAGES</th>
                    <th>CUBIC M3</th>
                    <th colspan="2">REMARKS</th>
                </tr>
                @php
                $i = 1;
                $total_cubeM3 = 0;
                @endphp
                @foreach ($report[0]->importcargosdetail as $item)
                @if ($i == 11 || $i == 30 )
                    </tbody>
                </table>
                </div>
                <div style="page-break-before: always">
                    <table style="width: 100%; margin-top: -90px">
                    <tbody>
                    <tr style="visibility: hidden">
                        <th style="height:1%" colspan="2" rowspan="2">
                            CLIENT 
                            <br>
                            <br>
                            LOCATION
                        </th>
                        <td style="height:1%; text-align: left !important;" colspan="3" rowspan="2">
                            : M/s PAK SHAHEEN CONTAINER SERVICES (PVT) LTD.
                            <br>
                            <br>
                            : PSCS KEAMARI YARD
                        </td>
                        <th style="height:0.5%">VESSEL NAME</th>
                        <td style="height:0.5%">{{ strtoupper(@$vessel_name) }}</td>
                    </tr>
                    <tr style="visibility: hidden">
                        <th style="height:0.5%">VOYAGE</th>
                        <td style="height:0.5%">{{ @$voy }}</td>
                    </tr>
                    <tr style="visibility: hidden">
                        <th colspan="2" rowspan="1">CONTAINER NO.</th>
                        <td colspan="1" rowspan="1">{{ strtoupper(@$container) }}</td>
                        <th rowspan="1">CONTAINER TYPE</th>
                        <td colspan="1" rowspan="1">{{ @$container_size."'".strtoupper(@$container_type) }}</td>
                        <th>DATE OF INSP</th>
                        <th>{{ @date("jS M,Y", strtotime($report[0]->dom_des)) }}</th>
                    </tr>
                    <tr>
                        <th style="width: 10%">S.NO</th>
                        <th style="width: 10%">INDEX</th>
                        <th style="width: 10%">Marks & NO.</th>
                        <th style="width: 10%">TYPE & NO. PACKAGES</th>
                        <th style="width: 10%">CUBIC M3</th>
                        <th style="width: 20%" colspan="2">REMARKS</th>
                    </tr>
                @endif
                <tr>
                    <td>{{ $i++ }}</td>
                    <td>{{ @$item->cargo_index}}</td>
                    <td>{{ @$item->mark_no }}</td>
                    <td>{{ @$item->description }}</td>
                    <td>{{ @$item->measurment_m3 }}</td>
                    <td colspan="2">
                        {{ @$item->remarks }}
                    </td>
                </tr>
                @php
                $total_cubeM3 += $item->measurment_m3;
                @endphp
                @endforeach

                
                @for ($j = $i; $j < 10; $j++)  
                <tr>
                    <td>{{$j}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td colspan="2"></td>
                </tr>
                @endfor
                    <tr>
                        <td colspan="4" style="border: none !important;"></td>
                        <td>{{ @$total_cubeM3 }}</td>
                        <th colspan="2">TOTAL CUBIC M3</th>

                    </tr> 

            </tbody>
        </table>

    </div>

</body>

</html>
