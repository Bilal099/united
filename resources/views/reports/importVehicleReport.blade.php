<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <style>
        table,
        td,
        th {
            border: 1px solid #595959;
            border-collapse: collapse;
            /* font-size: 11px; */
            font-family: Arial, Helvetica, sans-serif;
            text-align: center;
        }

        td,
        th {
            /* padding: 3px; */
            /* width: 60px; */
            height: 50px;
        }

        td{
            height: 80px !important;
        }

        th {
            background: #f0e6cc;
        }

        .even {
            background: #fbf8f0;
        }

        .odd {
            background: #fefcf9;
        }

        table,
        th,
        td,
        tr {}

        .img-logo {
            width: 150px;
            height: auto;
            position: absolute;
        }

        .text-center {
            text-align: center;

        }

        .h1-size {
            font-size: 1.5em;
        }

        .p-size {
            font-size: 0.7em;
        }

        .main-p-size {
            font-size: 0.6em;
        }

        .text-underline {
            text-decoration: underline;
        }

        @page {
            margin: 220px 25px 250px 25px;
            font-family: Arial, Helvetica, sans-serif;

        }


        /** Define the header rules **/
        #header {
            position: fixed;
            top: -220px;
            left: 0cm;
            right: 0cm;
            height: 5.5cm;
            /* background-color: yellow; */
        }



        /** Define the footer rules **/
        #footer {
            position: fixed;
            top: 630px;
            bottom: 0cm;
            left: 0cm;
            right: 0cm;
            height: 6cm;
            /* background-color: aquamarine; */
        }

    </style>
</head>

<body>
    <div id="header">
        <img src="{{ public_path("images/unitedlogo1.png") }}" class="img-logo" style="width:90px;margin-top: 10px;">
        <h1 class="text-center h1-size" style="margin-top: 40px; font-size:1.5em; color: #594F97;">UNITED MARINE SURVEYORS (PVT) LTD.</h1>
        <p class="text-center p-size" style="color: #594F97;"> <i> MARINE, FIRE AND MOTOR SURVEYORS, CERTIFIED IICL CONTAINER INSPECTORS,
            LIQUID<br>BULK & DRY CARGO INSPECTORS, NAUTICAL & ENGINEERING CONSULTANTS.</i>
            <br>
            --------------------------------------------------------------<i>(AN ISO 9001 : 2015 CERTIFIED
            COMPANY)</i>--------------------------------------------------------------
            <br>
            Suite # 207, 2nd Floor Shaheen Centre, Block-7, Clifton,
            Karachi-75600, Pakistan. 
            <br>
            Tele:(92-21)3586 1355-56 Fax:(92-21)3586 1354 E-mail
            :info@ums.com.pk Web: http:www.ums.com.pk </p>

            <div style="" class="p-size">
                <p style="width:100%;">
                    <span style="font-style: bold">Inspection/Certificate No: {{ isset($ref_number)? $ref_number:"xxxx-xx/xx-CM/PS"}}</span>
                    {{-- <span style="float:right; font-style: bold">Date : {{ @date_format(NOW(),"jS M,Y") }}</span> --}}
                    <span style="float:right; font-style: bold">Date : {{ ($report_created_date != "")? date_format($report_created_date,"jS M,Y"):date_format(NOW(),"jS M,Y") }}</span></p>
                    Ref: <strong><u>Cargo Condition At The Time Of De-Stuffing</u></strong>
                </p>
            </div>
            
            <p class="p-size"> </p>
         
    </div>

    <div id="footer">
        <p class="main-p-size" style="font-style: bold"> NOTES:- </p>
        
            <ul class="main-p-size">
                <li>The seal of the above container was found intact / sound prior to De-Stuffing of container</li>
                <li>The above survey relates to only for the external condition of the cargo and does not reflect the number of packages / or content</li>
            </ul>
            {{-- 1. The seal of the above container was found intact / sound prior to De-Stuffing in the container
            <br>
            2. The above survey relates to external condition of the cargo and does not reflect the number of packages /
            or content --}}
            {{-- <br> --}}
            <p class="main-p-size">
            The above relates to our findings, which is to the best of our knowledge, true correct and represent to our finding, at the time and place of survey only.
            <br>
            <strong>ISSUED WITHOUT PREJUDICE</strong>, to the rights/ liabilities of whosoever may concern
        </p>
        <br>
        <br>
        <p style="text-align: right"><i>for</i> <strong>UNITED MARINE SURVEYORS (PVT) LTD</strong>
        <br>
        <strong style="font-size: x-small">(PSCS-OPS-3-10)</strong>
        </p>
        <hr>
        <p class="main-p-size" style="text-align:center;">
            This repors is subject to the condition that is it understood and agreed that neither the firm, nor any of
            its surveyors in under any circumstances whatsoever, to be held responsible for

            any inaccuracy in any report or certificate issued by this firm or its surveyors/ agent for any error of
            judgment, default or negligence.
        </p>


    </div>



    <div class="main" style="page-break-after:auto;">
        
        <div style="">
            <p class="p-size" style="text-align: center;">
               <u> <strong>
                M.V. "{{strtoupper(@$vessel_name)}}" VOY-{{@$voy}} OF {{ @date("jS M,Y", strtotime($date_of_insp)) }} AT KARACHI.
                <br>
                CARGO CONDITION SURVEY REPORT AT THE TIME OF CONTAINER DE-STUFFING/MEASUREMENT.
                </strong></u>
            </p>
        </div>

    <p class="p-size">
        <i>CERTIFIED</i> that upon receipt instructions from
        <strong>M/s PAK SHAHEEN CONTAINER SERVICES KARACHI</strong>,
        we the undersigned surveyors, attended at
        <strong>Pak Shaheen Yard Keamari Karachi</strong>,
        on
        <strong>{{ @date("jS M,Y", strtotime($report->dom_des)) }} (Day)</strong>
        for the purpose of Carrying out cargo condition survey at the time of De-stuffing of cargo from the containers. We report as under:-
        <br>
        <u>PSCS-OPS-3-10</u>
    </p>
        <table style="width: 100%; font-size:0.7em !important">
          
            <tbody>
     
                <tr>
                    <th style="width:2%">S.NO</th>
                    <th style="width:10%">DATE</th>
                    <th style="width:5%">INDEX</th>
                    <th style="width:10%">MARKS & NO.</th>
                    <th style="width:18%">DESCRIPTION OF CARGO</th>
                    <th style="width:10%">CONTAINER NO. /SIZE /TYPE</th>
                    <th style="width:10%">CUBIC M3</th>
                    <th style="width:30%" colspan="1">REMARKS</th>
                </tr> 
                @php
                $i = 1;
                @endphp
                @foreach ($report->importcargosdetail as $item)
                
                @php
                    $temp_description = explode(",",$item->description)
                @endphp
                <tr>
                    <td>{{ $i++ }}</td>
                    <td>{{ @date("jS M,Y", strtotime($report->dom_des)) }}</td>
                    <td>{{ @$item->cargo_index}}</td>
                    <td>{{ @$item->mark_no }}</td>
                    {{-- <td>{{ @$item->description }}</td> --}}
                    <td> 
                        @foreach ($temp_description as $desc)
                            {{$desc}}
                            <br>
                        @endforeach
                    </td>
                    <td>{{ strtoupper(@$report->container->name." / ".@$report->container->size."/".@$report->container->ContainerType->name)}}</td>
                    {{-- <td>{{ strtoupper(@$report->container->name)}} / 
                        <br>
                            {{strtoupper(@$report->container->size)}}/
                            <br>
                            {{strtoupper(@$report->container->ContainerType->name)}}
                    </td> --}}

                    {{-- <td>{{ @$item->measurment_m3 }}</td> --}}
                    <td>{{ @number_format((float)($item->measurment_m3) , 3, '.', '')}}</td>

                    <td style="text-align:left !important" colspan="1">
                        {{ @$item->remarks }}
                        @if (@$item->accessories != null)
                            <br>
                            <strong><u>Accessories found at time of de-stuffing</u></strong>
                            <br>
                            {{ @$item->accessories }}
                        @endif
                    </td>
                </tr>
                @endforeach

                
                {{-- @for ($j = $i; $j <= 7; $j++)  
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td colspan="1"></td>
                    </tr>

                    @endfor --}}
                

            </tbody>
        </table>

    </div>


</body>

</html>
