@extends('layouts.app')

@section('customCSS')
    
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="icon-settings font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase">Add New Frieght For Warder</span>
                </div>
            </div>
            <div class="portlet-body form">
                <form role="form" method="POST" action="{{route('frieghtForWarder.update',$FrieghtForWarder->id)}}">
                    @method('PUT')
                    @csrf
                    <div class="form-body">
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" name="name" class="form-control @error('name') has-error @enderror" placeholder="Name" value="{{old('name') != null ?old('name'):$FrieghtForWarder->name}}">
                            @error('name')
                                    <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                            @enderror
                        </div>
                        
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn blue">Update</button>
                        <a href="{{url('frieghtForWarder')}}" class="btn default">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('customJS')
    
@endsection