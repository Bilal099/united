<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="widtd=device-widtd, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    {{-- <link href="{{asset('assets/global/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" /> --}}

    <title>Document</title>
    <style>
        @page { margin: 10px; }
        body { margin: 10px; }
        *{
            font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
            font-size: 0.95em;
        }
        table.first-detail
        {
            width: 100%;
            border-collapse: collapse;
            border: none;
            margin: 10px 0px;
        }
        table.first-detail td
        {
            padding: 5px;
        }
        table.details
        {
            width: 100%;
            border-collapse: collapse;
            border: 1px black solid;
        }
        table.details td
        {
            padding: 5px;
            border: 1px black solid;
        }
        .text-center
        {
            text-align: center;
            
        }
        .h1-size{
            font-size: 1.5em;
        }
        .text-underline{
            text-decoration: underline;
        }
        .img-logo
        {
            width: 150px;
            height: auto;
            position: absolute;
        }
        .form-no
        {
            position: absolute;
            right: 10px;
        }
        .text-span-size
        {
            font-size: 1.1em;
        }

        .in-line{
            text-align: justify; 

            /* ie 7*/  
            *width: 100%;  
            *-ms-text-justify: distribute-all-lines;
            *text-justify: distribute-all-lines;
            display: inline-block;
        }

        .p1 {
            display: inline-block;

            /* ie 7*/ 
            *display: inline;
            *zoom: 1;
            *text-align: left; 
        }

        .p2 {
            display: inline-block;
            vertical-align: baseline; 

            /* ie 7*/
            *display: inline;
            *zoom:1;
            *text-align: right;
        }
    </style>
</head>
<body>
    <img src="{{ public_path("images/unitedlogo.png") }}" class="img-logo" style="width:70px">
    {{-- <p class="form-no"><b>Form No: 23</b></p> --}}
    <h1 class="text-center h1-size">UNITED MARINE SURVEYORS (PVT) LTD.</h1>
    <p class="text-center">MARINE, FIRE AND MOTOR SURVEYORS, CERTIFIED IICL CONTAINER INSPECTORS, LIQUID<br>BULK & DRY CARGO INSPECTORS, NAUITICAL & ENGINEERING CONSULTANTS.</p>
    <hr>
    <p class="" style="font-size:0.8em;text-align:center">
        Suite # 207, 2nd Floor Shaheen Centre, Block-7, Clifton, Karachi-75600, Pakistan. Tele:(92-21)3586 1356 Fax:(92-21)3586 1354
    </p>
    <p style="font-size:0.8em;text-align:center">
        E-mail: info@ums.com.pk Web: http://www.ums.com.pk
    </p>

   

    {{-- <div>

        
        <div class="in-line">
            <b>VESSELS NAME:</b> <span class="text-underline text-span-size">{{ucwords(strtolower($ExportCargo->vessel->name))}}</span>
            <b>VOY</b> <span class="text-underline text-span-size">{{ucwords(strtolower($ExportCargo->voy))}}</span>
        </div>
        <div>
            <b>SHIPPING/CLEARING AGENT:</b> <span class="text-underline text-span-size">{{ucwords(strtolower($ExportCargo->agent->name))}}</span>
        </div>
        <div>
            <b>CONSIGNEE:</b> <span class="text-underline text-span-size">{{ucwords(strtolower($ExportCargo->frieghtforwarder->name))}}</span>
        </div>
        <b>PORT/DESTINATION:</b> <span class="text-underline text-span-size">{{ucwords(strtolower($ExportCargo->port->nameWoDiac))}} / {{ucwords(strtolower($ExportCargo->port->country))}}</span>
    </div> --}}

    <table class="first-detail">
        <tbody>
            <tr>
                <td><span class="" style="text-align:left"> <b>REPORT NO:</b> <span class="text-underline text-span-size">{{ucwords(strtolower($ExportCargo->serialNo))}}</span> </span></td>
                <td></td>
                <td><span class="" style="text-align:right"> <b>Date:</b> <span class="text-underline text-span-size">{{date("M,d Y") }}</span> </span></td>
            </tr>
            <tr>
                <td><b>VESSELS NAME:</b> <span class="text-underline text-span-size">{{ucwords(strtolower($ExportCargo->vessel->name))}}</span></td>
                <td></td>
                <td><b>VOY</b> <span class="text-underline text-span-size">{{ucwords(strtolower($ExportCargo->voy))}}</span></td>
            </tr>
            
            <tr>
                <td><b>OF</b> <span class="text-underline text-span-size">{{ucwords(strtolower($ExportCargo->departureDate))}}</span></td>
               
                <td><b>DESCRIPTION OF CARGO:</b> <span class="text-underline text-span-size">{{ucwords(strtolower($ExportCargo->description))}}</span></td>

            </tr>
            <tr>
                <td><b>SHIPPING/CLEARING AGENT:</b> <span class="text-underline text-span-size">{{ucwords(strtolower($ExportCargo->agent->name))}}</span></td>
            </tr>
            <tr>
                <td><b>CONSIGNEE:</b> <span class="text-underline text-span-size">{{ucwords(strtolower($ExportCargo->frieghtforwarder->name))}}</span></td>
            </tr>
            <tr>
                <td><b>PORT/DESTINATION:</b> <span class="text-underline text-span-size">{{ucwords(strtolower($ExportCargo->port->nameWoDiac))}} / {{ucwords(strtolower($ExportCargo->port->country))}}</span></td>
            </tr>
        </tbody>
    </table>
    <hr>
    <p class="text-center"> <b> FINDING OF SURVEY</b></p>
    <hr>
    <table class="details">
        <thead>
            <tr>
                <th>S.No</th>
                <th>Lenght CMS</th>
                <th>Breadth CMS</th>
                <th>Depth CMS</th>
                <th>No. of Package</th>
                <th>Marks & No.</th>
                <th>Average M<sup>3</sup></th>
            </tr>
        </thead>
        <tbody>
            @php
                $i = 1;
                $total_mean = 0;
                $total_mean_count = 0;
            @endphp
            @foreach ($ExportCargo->exportcargosdetail as $item)
            <tr>
                <td>{{$i++}}</td>
                <td>{{$item->length}}</td>
                <td>{{$item->breadth}}</td>
                <td>{{$item->depth}}</td>
                <td>{{$item->noOfPackages}}</td>
                <td>{{$item->marksAndNo}}</td>
                <td>{{$item->average}}</td>
                @if ($ExportCargo->mean_check == 'yes')

                    @php
                        $total_mean += $item->average;
                        $total_mean_count += 1;
                    @endphp
                
                @endif
            </tr>

            @endforeach
            @if ($ExportCargo->mean_check == 'yes')
                <tr>
                    <td>{{$i++}}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <th>Total Calculated Average</th>
                    <td>{{$total_mean/$total_mean_count}}</td>
                </tr>
            @endif
            
        </tbody>
    </table>

</body>
</html>