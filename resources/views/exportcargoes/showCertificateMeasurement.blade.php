@extends('layouts.app')
@section('customCSS')
<link href="{{asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="icon-settings font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase">View Export Cargo</span>
                    </div>
                    <a href="{{route('export-cargo.show',$export_id)}}" class="btn default pull-right">Back</a>

                    {{-- <a href="" class="btn btn-success pull-right">Add Certificate of measurement</a> --}}
                    {{-- <button class="btn btn-success pull-right" >Show Remarks</button> --}}
                </div>
                {{-- @if (Session::has('message')) --}}
                @if(session('message'))
                    <div id="" class="custom-alerts alert alert-success fade in">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                        <i class="fa-lg fa fa-warning"></i>  
                        {{session('message')}}
                    </div>
                @endif
                <div class="portlet-body">
                        <div class="form-body row">

                            {{-- <div class="col-sm-12">

                                <div class="col-xs-12">
                                    <div class="mt-element-ribbon bg-grey-steel">
                                        <div class="ribbon ribbon-round ribbon-color-danger uppercase">Export Cargo Certified By,</div>
                                        <p class="ribbon-content">Certified tha upon receipt of instruction from M/s. 
                                            <span class="caption-subject font-green bold uppercase">{{$export_details->exportcargosCertificate[0]->agentName}}</span>
                                            as Local Agents for the charterers/owners M/s. 
                                            <span class="caption-subject font-green bold uppercase">{{$export_details->exportcargosCertificate[0]->undersignedSurveyors}}</span>
                                            as undersigned surveyors, attended at 
                                            <span class="caption-subject font-green bold uppercase">{{$export_details->exportcargosCertificate[0]->attendedAt}}</span>.
                                            <br>
                                            @if ($export_details->exportcargosCertificate[0]->side = 'east')
                                                <span class="font-green uppercase bold"  >East</span>/<span class="uppercase bold">West</span>
                                            @else
                                                <span class="font-green uppercase bold"  >West</span>/<span class="uppercase bold">East</span>
                                            @endif
                                            Karachi on / from 
                                            <span class="caption-subject font-green bold uppercase">{{$export_details->exportcargosCertificate[0]->date}}</span>
                                            for the purpose of carryinout cargo condition Survey at the time of Arrival that mentioned below:
                                        </p>
                                    </div>
                                </div>
                                
                                
                            </div> --}}

                            <div class="col-sm-12">
                                <h4 class="caption-subject bold uppercase">Export Cargo</h4>
                            </div>
                            <div class="col-sm-12">
                                
                                <table class="table table-striped table-bordered" style="margin-top:20px">
                                    <thead>
                                        <tr>
                                            <th>Added By</th>
                                            <th>Serial No.</th>
                                            <th>Vessels</th>
                                            <th>VOY</th>
                                            <th>OF</th>
                                            <th>Shipper/Clearing Agent</th>
                                            <th>Consignee</th>
                                            <th>Port/Destination</th>
                                            <th>Status</th>
                                            <th>Description Of Cargo</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{{$export_details->user->name}}</td>
                                            <td>{{$export_details->serialNo}}</td>
                                            <td>{{$export_details->vessel->name}}</td>
                                            <td>{{$export_details->voy}}</td>
                                            <td>{{date_format(date_create($export_details->departureDate),"j M, Y")}}</td>
                                            <td>{{$export_details->agent->name}}</td>
                                            <td>{{$export_details->frieghtforwarder->name}}</td>
                                            <td>{{$export_details->port->nameWoDiac}} / {{$export_details->port->country}}</td>
                                            <td>{{$export_details->status}}</td>
                                            <td>{{$export_details->description}}</td>
                                            
                                            
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            
                            <div class="col-sm-12">
                                <h4 class="caption-subject bold uppercase">Export Cargo Measurements Details
                                    {{-- <button class="btn btn-success pull-right" id="btn-toggle-remarks" data-value="hide">Show Edit Measurements</button> --}}
                                </h4>

                            </div>
                            <div class="col-sm-12">
                                <form action="{{route('exportCargo.AddCertificateOfMeasurement')}}" method="post" id="formMeasurements">
                                    @csrf
                                    <input type="hidden" name="export_id" value="{{$export_id}}">
                                    <table class="table table-striped table-bordered" style="margin-top:20px" id="importDetailTable">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Marks & NO.</th>
                                                <th>Packages NOs.</th>
                                                <th>NO. Of Packages</th>
                                                <th>Length C M S</th>
                                                <th>Breadth C M S</th>
                                                <th>Depth C M S</th>
                                                <th>Average M<sup>3</sup> </th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                $i = 1;
                                            @endphp
                                            @foreach ($export_details->exportcargosdetail as $item)
                                                <tr>
                                                    <td>{{$i}}</td>
                                                    <td>{{$item->marksAndNo}}</td>
                                                    <td>{{$item->packagesNos}}</td>
                                                    <td class="noOfPackages">{{$item->noOfPackages}}</td>
                                                    {{-- <td class="export_detail_editable_disable">{{$item->length}}</td>
                                                    <td class="export_detail_editable_disable">{{$item->breadth}}</td>
                                                    <td class="export_detail_editable_disable">{{$item->depth}}</td> --}}


                                                    <td class="export_detail_editable_enable ">
                                                        <input type="hidden" name="exportdetailId[]" value="{{$item->id}}">
                                                        <input min="0" step="0.01" type="number" value="{{(($item->length != null)? $item->length:0)}}" name="length[]" class="form-control" placeholder="Enter Length" />
                                                    </td>
                                                    <td class="export_detail_editable_enable ">
                                                        <input min="0" step="0.01" type="number" value="{{(($item->breadth != null)? $item->breadth:0)}}" name="breadth[]" class="form-control" placeholder="Enter Breadth" />
                                                    </td>
                                                    <td class="export_detail_editable_enable ">
                                                        <input min="0" step="0.01" type="number" value="{{(($item->depth != null)? $item->depth:0)}}" name="depth[]" class="form-control" placeholder="Enter Depth" />
                                                    </td>

                                                    {{-- <td class="export_detail_editable_disable avg_calculation_td">{{$item->average}}</td> --}}
                                                    <td class="export_detail_editable_enable ">
                                                        <input min="0" step="0.01" type="number" value="{{(($item->average != null)? $item->average:0)}}" name="average[]" class="form-control avg_calculation_input" readonly />
                                                    </td>
                                                </tr>
                                                @php
                                                    $i += 1;
                                                @endphp
                                            @endforeach
                                        </tbody>
                                    </table>
                                </form>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" form="formMeasurements" class="btn blue  updateMeasurementsBtn">Update Measurements</button>
                        </div>
                </div>
            </div>
        </div>
    </div>

    
@endsection
@section('customJS')
    <script>
        $(document).on('click','#btn-toggle-remarks',function(){
            if($(this).attr('data-value') === 'hide')
            {
                $('.export_detail_editable_enable').removeClass('hidden');
                $(this).attr('data-value','show');
                $(this).text('Hide Edit Measurements');
                $('.updateMeasurementsBtn').removeClass('hidden');
                $('.export_detail_editable_disable').addClass('hidden');
            }
            else
            {
                $('.export_detail_editable_enable').addClass('hidden');
                $(this).attr('data-value','hide');
                $(this).text('Show Edit Measurements');
                $('.updateMeasurementsBtn').addClass('hidden');
                $('.export_detail_editable_disable').removeClass('hidden');
            }
        })

        $(document).on('keyup','.export_detail_editable_enable',function(){
            
            // let lenght              = parseFloat($(this).closest('tr').find('input[name="length[]"]').val());
            // let breadth             = parseFloat($(this).closest('tr').find('input[name="breadth[]"]').val());
            // let depth               = parseFloat($(this).closest('tr').find('input[name="depth[]"]').val());
            // let noOfPackages        = parseFloat($(this).closest('tr').find('.noOfPackages').text());

            // console.log('noOfPackages',noOfPackages);
            // console.log('lenght',lenght);
            // console.log('breadth',breadth);
            // console.log('depth',depth);

            let ref = $(this).closest('tr');

            let lenght              = parseFloat(ref.find('input[name="length[]"]').val()) || 0;
            let breadth             = parseFloat(ref.find('input[name="breadth[]"]').val()) || 0;
            let depth               = parseFloat(ref.find('input[name="depth[]"]').val()) || 0;
            let noOfPackages        = parseFloat(ref.find('.noOfPackages').text()) || 0;

            // console.log('lenght',lenght);
            // console.log('breadth',breadth);
            // console.log('depth',depth);

            // lenght              = ((lenght != 0 || lenght != null || lenght != '')? lenght:0) ;
            // breadth             = ((breadth != 0 || breadth != null || breadth != '')? breadth:0) ;
            // depth               = ((depth != 0 || depth != null || depth != '')? depth:0) ;


            if(lenght == 0 || lenght == null || lenght == '')
            {
                console.log('lenght',lenght);
                lenght = 0;
                
                ref.find('input[name="length[]"]').val("0");
                ref.find('.avg_calculation_input').val("0");

            }
            if(breadth == 0 || breadth == null || breadth == '')
            {
                console.log('breadth',breadth);
                breadth = 0;
                
                ref.find('input[name="breadth[]"]').val("0");
                ref.find('.avg_calculation_input').val("0");

            }
            if(depth == 0 || depth == null || depth == '')
            {
                console.log('depth',depth);
                depth = 0;
                ref.find('input[name="depth[]"]').val("0");
                ref.find('.avg_calculation_input').val("0");

            }


            // ref.find('input[name="length[]"]').val((lenght != 0 || lenght != null || lenght != '')? lenght:0) ;
            // ref.find('input[name="breadth[]"]').val((breadth != 0 || breadth != null || breadth != '')? breadth:0) ;
            // ref.find('input[name="depth[]"]').val((depth != 0 || depth != null || depth != '')? depth:0) ;

            // noOfPackages        = ((noOfPackages != 0 || noOfPackages != null || noOfPackages != '')? noOfPackages:0) ;

            let avg = (lenght * breadth * depth * noOfPackages) * 0.000001;

            ref.find('.avg_calculation_td').text(((avg != 0) ? avg:0));
            ref.find('.avg_calculation_input').val(((avg != 0) ? avg:0));

           
        })
    </script>
@endsection