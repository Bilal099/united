<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="widtd=device-widtd, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    {{-- <link href="{{asset('assets/global/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" /> --}}

    <title>Document</title>
    <style>
        @page { margin: 10px; }
        body { margin: 10px; }
        *{
            font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
            font-size: 0.95em;
        }
        table.first-detail
        {
            width: 100%;
            border-collapse: collapse;
            border: none;
            margin: 10px 0px;
        }
        table.first-detail td
        {
            padding: 5px;
        }
        table.details
        {
            width: 100%;
            border-collapse: collapse;
            border: 1px black solid;
        }
        table.details td
        {
            padding: 5px;
            border: 1px black solid;
        }
        .text-center
        {
            text-align: center;
            
        }
        .h1-size{
            font-size: 1.5em;
        }
        .text-underline{
            text-decoration: underline;
        }
        .img-logo
        {
            width: 150px;
            height: auto;
            position: absolute;
        }
        .form-no
        {
            position: absolute;
            right: 10px;
        }
        .text-span-size
        {
            font-size: 1.1em;
        }
    </style>
</head>
<body>
    <img src="{{ public_path("images/unitedlogo.png") }}" class="img-logo" style="width:90px">
    <p class="form-no"><b>Form No: 23</b></p>
    <h1 class="text-center h1-size">UNITED MARINE SURVEYORS (PVT) LTD.</h1>
    <p class="text-center">MARINE, FIRE AND MOTOR SURVEYORS, CERTIFIED IICL CONTAINER INSPECTORS, LIQUID<br>BULK & DRY CARGO INSPECTORS, NAUITICAL & ENGINEERING CONSULTANTS.</p>
    {{-- <p class="text-center">------------------------------------ (AN ISO 9001 : 2015 CERTIFIED COMPANY)--------------------------------------</p> --}}
    <p class="pull-right" style="text-align:right">Survay Report No. _____________  Date: _____________</p> <br>

    <p class="text-center" style=""> <strong> REPORT OF CARGO CONDITION FOUND AT THE TIME OF ARRIVAL STUFFING OF CONTAINERS</strong></p>
    <p class="">REF: CARGO CONDITION AT {{strtoupper($ExportCargo->status)}}</p>
    {{-- STUFFING / ARRIVAL --}}
    <hr>

    <p class="ribbon-content">Certified tha upon receipt of instruction from M/s. 
        <span class="text-underline" style="font-weight: bold">{{ucwords(strtolower($ExportCargo->exportcargosCertificate->agentName))}}</span>
        as Local Agents for the charterers/owners M/s. 
        <span class="text-underline" style="font-weight: bold">{{ucwords(strtolower($ExportCargo->exportcargosCertificate->undersignedSurveyors))}}</span>
        as undersigned surveyors, attended at 
        <span class="text-underline" style="font-weight: bold">{{ucwords(strtolower($ExportCargo->exportcargosCertificate->attendedAt))}}</span>
        {{-- East / West --}}
        @if ($ExportCargo->exportcargosCertificate->side = 'east')
            <span class="text-underline" style="">East</span>
            {{-- /<span class="uppercase bold">West</span> --}}
        @else
            <span class="text-underline" style="">West</span>
            {{-- /<span class="uppercase bold">East</span> --}}
        @endif
        Karachi on / from 
        <span class="text-underline" style="font-weight: bold">{{$ExportCargo->exportcargosCertificate->date}}</span>
        for the purpose of carryinout cargo condition Survey at the time of Arrival that mentioned below:
    </p>

    {{-- <div style="padding-right: 150px;"><b>VESSELS NAME:</b> <span class="text-underline text-span-size">VMExcellence</span>
    <b>VOY</b> <span class="text-underline text-span-size">IGM-294</span>
    <b>OF</b> <span class="text-underline text-span-size">12-Aug-2019</span>
    <b>DESCRIPTION OF CARGO:</b> <span class="text-underline text-span-size">Test Description</span></div style="padding-right: 150px;"> --}}

    {{-- <p class="text-center">Suite # 207, Shaheen Centre, Schon Circle, Kehkashan Block 7, Clifton, Karachi - 75600, Pakistan.<br>Tele: (92-21) 3586 1355-56 Fax: (92-21) 3586 1358 Email:info@ums.com.pk Web:http://www.ums.com.pk</p>
    <h2 class="text-underline text-center">DAILY MEASURMENT FOR IMPORT CARGO</h2> --}}
    <table class="first-detail">
        <tbody>
            <tr>
                <td><b>VESSELS NAME:</b> <span class="text-underline text-span-size">{{ucwords(strtolower($ExportCargo->vessel->name))}}</span></td>
                <td><b>VOY</b> <span class="text-underline text-span-size">{{ucwords(strtolower($ExportCargo->voy))}}</span></td>
                <td><b>OF</b> <span class="text-underline text-span-size">{{ucwords(strtolower($ExportCargo->departureDate))}}</span></td>
                <td><b>DESCRIPTION OF CARGO:</b> <span class="text-underline text-span-size">{{ucwords(strtolower($ExportCargo->description))}}</span></td>

            </tr>
            <tr>
                {{-- <td colspan="2"><b>DATE OF MEASURMENT/DE-STUFFING</b> <span class="text-underline text-span-size">12-sep-23</span></td>
                <td><b>CONTAINER NO.</b> <span class="text-underline text-span-size">756413-11</span></td> --}}
                <td><b>SHIPPING/CLEARING AGENT:</b> <span class="text-underline text-span-size">{{ucwords(strtolower($ExportCargo->agent->name))}}</span></td>
                <td><b>CONSIGNEE:</b> <span class="text-underline text-span-size">{{ucwords(strtolower($ExportCargo->frieghtforwarder->name))}}</span></td>
                <td><b>PORT/DESTINATION:</b> <span class="text-underline text-span-size">{{ucwords(strtolower($ExportCargo->port->nameWoDiac))}} / {{ucwords(strtolower($ExportCargo->port->country))}}</span></td>
                <td><b>CONTAINER:</b> <span class="text-underline text-span-size">{{ucwords(strtolower($ExportCargo->container->name))}}</span></td>


            </tr>
        </tbody>
    </table>
    <hr>
    <p class="text-center"> <b> FINDING OF SURVEY</b></p>
    <hr>
    <table class="details">
        <thead>
            <tr>
                <th>S.No</th>
                <th>DATE</th>
                <th>DESCRIPTION OF CARGO</th>
                <th>MARKS & NO.</th>
                {{-- <th>CONTAINER NO</th> --}}
                <th>SIZE</th>
                <th>SEAL NO</th>
                <th>NO. OF PACKAGES</th>
                <th>REMARKS</th>
            </tr>
        </thead>
        <tbody>
            @php
                $i = 1;
            @endphp
            @foreach ($ExportCargo->exportcargosdetail as $item)
            <tr>
                <td>{{$i++}}</td>
                <td>{{date("M,d Y", strtotime($item->cargo_date)) }}</td>
                <td>{{ucwords(strtolower($item->description))}}</td>
                <td>{{ucwords(strtolower($item->marksAndNo))}}</td>
                {{-- <td>{{ucwords(strtolower($item->container->name))}}</td> --}}
                <td>{{ucwords(strtolower($item->size))}}</td>
                <td>{{ucwords(strtolower($item->sealNo))}}</td>
                <td>{{ucwords(strtolower($item->noOfPackages))}}</td>
                <td>{{ucwords(strtolower($item->remarks))}}</td>
            </tr>
            @endforeach

            
        </tbody>
    </table>
    {{-- <script src="{{asset('assets/global/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script> --}}

</body>
</html>