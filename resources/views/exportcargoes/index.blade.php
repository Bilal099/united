@extends('layouts.app')

@section('customCSS')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
    {{-- <link href="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" /> --}}
    <link href="{{asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css" />

    <!-- END PAGE LEVEL PLUGINS -->

    <style>
        a.dt-button.buttons-print.btn.dark.btn-outline {
            display: none;
        }

        .btn.btn-outline.red {
            display: none;
        }

        .btn.btn-outline.green{
            display: none;

        }

        .btn.btn-outline.yellow{
            display: none;
            
        }

        .btn.btn-outline.purple{
            display: none;
            
        }

        .btn.btn-outline.dark{
            display: none;
            
        }

        .btn.default:not(.btn-outline) {
            display: none;
        }

        table.dataTable.dtr-inline.collapsed>tbody>tr>td:first-child, table.dataTable.dtr-inline.collapsed>tbody>tr>td:first-child::before{
            display: none;
        }
        table.dataTable.dtr-inline.collapsed>tbody>tr>td, table.dataTable>thead>tr>th , table.dataTable>tfoot>tr>th  {
            display: table-cell !important;
        }
    </style>
@endsection

@section('content')
{{-- @dd($container) --}}
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="icon-settings font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase">View Export Cargo Arrival</span>
                </div>
                <div class="tools"> </div>
                <div>
                    <a href="{{route('export-cargo.create')}}" class="btn btn-danger" style="float:right">Add Export Cargo</a>
                </div>
            </div>

            @if (session('success'))
                <div id="" class="custom-alerts alert alert-success fade in">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    <i class="fa-lg fa fa-warning"></i>  
                    {{session('success')}}
                </div>
            @elseif(session('error'))
                <div id="" class="custom-alerts alert alert-danger fade in">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    <i class="fa-lg fa fa-warning"></i>  
                    {{session('error')}}
                </div>
            @endif
            
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_1">
                    <thead>
                        <tr>
                            <th>S.No</th>
                            <th>Added By</th>
                            <th>Serial No.</th>
                            <th>Vessels</th>
                            <th>VOY</th>
                            <th>OF</th>
                            <th>Shipper/Clearing Agent</th>
                            <th>Consignee</th>
                            <th>Container</th>
                            <th>Port/Destination</th>
                            <th>Status</th>
                            <th>Description</th>
                            <th style="min-width: 100px;">Action</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>S.No</th>
                            <th>Added By</th>
                            <th>Serial No.</th>
                            <th>Vessels</th>
                            <th>VOY</th>
                            <th>OF</th>
                            <th>Shipper/Clearing Agent</th>
                            <th>Consignee</th>
                            <th>Container</th>
                            <th>Port/Destination</th>
                            <th>Status</th>
                            <th>Description</th>
                            <th style="min-width: 100px;">Action</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        @php
                            $i = 1;
                        @endphp
                        @foreach ($export_details as $item)
                        @if (strtolower($item->status) == strtolower("arrival"))
                        
                            <tr>
                                <td>{{$i}}</td>
                                <td>{{$item->user->name}}</td>
                                <td>{{$item->serialNo}}</td>
                                <td>{{$item->vessel->name}}</td>
                                <td>{{$item->voy}}</td>
                                <td>{{date("d M, Y", strtotime($item->departureDate)) }}</td>
                                <td>{{$item->agent->name}}</td>
                                <td>{{$item->frieghtforwarder->name}}</td>
                                <td>{{$item->container->name}}</td>
                                <td>{{$item->port->nameWoDiac}} / {{$item->port->country}}</td>
                                <td>{{$item->status}}</td>
                                <td>{{$item->description}}</td>
                                <td>
                                    
                                    {{-- <form id="form1" action="{{route('export-cargo.destroy',$item->id)}}" method="post">
                                        @method('DELETE')
                                        @csrf --}}

                                            <a href="{{route('export-cargo.show',$item->id)}}" class="btn btn-info"><i class="fa fa-eye"></i></a>
                                       
                                            <a href="{{route('export-cargo.edit',$item->id)}}" class="btn btn-primary"><i class="fa fa-edit"></i></a>

                                        {{-- <button type="submit" class="btn btn-danger delete_export_record"><i class="fa fa-trash"></i></button> --}}

                                        
                                            {{-- <button type="submit" id="alert_msg"
                                            class="btn btn-danger mt-sweetalert delete_export_record" 
                                            data-title="Do you agree to the Terms and Conditions?" 
                                            data-type="info" data-show-confirm-button="true" data-confirm-button-class="btn-success" data-show-cancel-button="true" 
                                            data-cancel-button-class="btn-default" data-close-on-confirm="false" data-close-on-cancel="false" data-confirm-button-text='Yes, I agree'
                                            data-cancel-button-text='No, I disagree' data-popup-title-success="Thank you" 
                                            data-popup-message-success="You have agreed to our Terms and Conditions" 
                                            data-popup-title-cancel="Cancelled" data-popup-message-cancel="You have disagreed to our Terms and Conditions"><i class="fa fa-trash"></i></button>
                                             --}}
                                        

                                    {{-- </form> --}}
                                </td>
                            </tr>
                            @php
                                $i += 1;
                            @endphp
                        @endif
                        @endforeach
                        
                        
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
        
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="icon-settings font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase">View Export Cargo Stuffing</span>
                </div>
                <div class="tools"> </div>
                <div>
                    {{-- <a href="{{route('export-cargo.create')}}" class="btn btn-danger" style="float:right">Add Export Cargo</a> --}}
                </div>
            </div>
            
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_2">
                    <thead>
                        <tr>
                            <th>S.No</th>
                            <th>Added By</th>
                            <th>Serial No.</th>
                            <th>Vessels</th>
                            <th>VOY</th>
                            <th>OF</th>
                            <th>Stuffing Date</th>
                            <th>Shipper/Clearing Agent</th>
                            <th>Consignee</th>
                            <th>Container</th>
                            <th>Port/Destination</th>
                            <th>Status</th>
                            <th>Description</th>
                            <th style="min-width: 100px;">Action</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>S.No</th>
                            <th>Added By</th>
                            <th>Serial No.</th>
                            <th>Vessels</th>
                            <th>VOY</th>
                            <th>OF</th>
                            <th>Stuffing Date</th>
                            <th>Shipper/Clearing Agent</th>
                            <th>Consignee</th>
                            <th>Container</th>
                            <th>Port/Destination</th>
                            <th>Status</th>
                            <th>Description</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        @php
                            $i = 1;
                        @endphp
                        @foreach ($export_details as $item)
                        @if (strtolower($item->status) == strtolower("Stuffing"))
                            <tr>
                                <td>{{$i}}</td>
                                <td>{{@$item->user->name}}</td>
                                <td>{{@$item->serialNo}}</td>
                                <td>{{@$item->vessel->name}}</td>
                                <td>{{@$item->voy}}</td>
                                <td>{{@date("d M, Y", strtotime($item->departureDate)) }}</td>
                                <td>{{@date("d M, Y", strtotime($item->stuffing_date)) }}</td>
                                {{-- <td>{{$item->departureDate}}</@td> --}}
                                <td>{{@$item->agent->name}}</td>
                                <td>{{@$item->frieghtforwarder->name}}</td>
                                <td>{{@$item->container->name}}</td>
                                <td>{{@$item->port->nameWoDiac}} / {{$item->port->country}}</td>
                                <td>{{@$item->status}}</td>
                                <td>{{@$item->description}}</td>

                            
                            <td>
                                <a href="{{route('export-cargo.show',$item->id)}}" class="btn btn-info"><i class="fa fa-eye"></i></a>
                                <a href="{{route('export-cargo.edit',$item->id)}}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                            </td>
                        </tr>
                        @php
                            $i += 1;
                        @endphp
                        @endif
                        
                        @endforeach
                        
                        
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
        
    </div>
</div>
@endsection

@section('customJS')
    <script src="{{asset('assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js')}}" type="text/javascript"></script>

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{asset('assets/pages/scripts/table-datatables-buttons.min.js')}}" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->

    <script>
        var SweetAlert = function () {
            return {
                init: function (_this) {
                    $(".mt-sweetalert").each(function () {
                        _thisTemp = _this;
                        var t = $(this).data("title"),
                            a = $(this).data("message"),
                            s = $(this).data("type"),
                            e = $(this).data("allow-outside-click"),
                            n = $(this).data("show-confirm-button"),
                            c = $(this).data("show-cancel-button"),
                            o = $(this).data("close-on-confirm"),
                            i = $(this).data("close-on-cancel"),
                            l = $(this).data("confirm-button-text"),
                            u = $(this).data("cancel-button-text"),
                            h = $(this).data("popup-title-success"),
                            d = $(this).data("popup-message-success"),
                            r = $(this).data("popup-title-cancel"),
                            f = $(this).data("popup-message-cancel"),
                            p = $(this).data("confirm-button-class"),
                            m = $(this).data("cancel-button-class");
                        $(this).click(function () {
                            swal({
                                title: t,
                                text: a,
                                type: s,
                                allowOutsideClick: e,
                                showConfirmButton: n,
                                showCancelButton: c,
                                confirmButtonClass: p,
                                cancelButtonClass: m,
                                closeOnConfirm: o,
                                closeOnCancel: i,
                                confirmButtonText: l,
                                cancelButtonText: u
                            }, function (t) {
                                t ? myAlert(_thisTemp) : swal(r, f, "error")
                            })
                        })
                    })
                }
            }
        }();
        // jQuery(document).ready(function () {
        //     SweetAlert.init()
        // });

        $('.delete_export_record').click(function (e){  
            _this = $(this);

            e.preventDefault();
            SweetAlert.init(_this);
        });

        function myAlert(_this)
        {  
            $(_this).closest('form').submit();
        }
    </script>
@endsection