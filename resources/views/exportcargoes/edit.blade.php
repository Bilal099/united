@extends('layouts.app')

@section('customCSS')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{asset('assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->

@endsection

@section('content')

{{-- @dd($export_details->exportcargosdetail) --}}

<div class="m-heading-1 border-green m-bordered">
    <h3>Cargo condition at arrival</h3>
    <p> Report of cargo condition found at the time of arrival stuffing of condition.</p>
    @if(session('message'))
        <div id="" class="custom-alerts alert alert-success fade in">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
            <i class="fa-lg fa fa-warning"></i>  
            {{session('message')}}
        </div>
    @endif

    
</div>
<form id="ExportForm" role="form" method="POST" action="{{route('export-cargo.update',$export_details->id)}}">
    @method('PUT')
    @csrf
    <input type="hidden" name="exportCertificate_id" value="{{$export_details->exportcargosCertificate->id}}">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="icon-settings font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase">Add Certificate Receipt</span>
                        
                    </div>
                </div>
                
                <div class="form-body row">
                    <div class="form-group col-md-6 @error('local_agent_ms') has-error @enderror">
                        <label class="">Local Agents</label>

                        <input  type="text" name="local_agent_ms" class="form-control" placeholder="Enter Value" id="" value="{{(old('local_agent_ms') != null)? old('local_agent_ms'):$export_details->exportcargosCertificate->agentName}}">
                        
                        @error('local_agent_ms')
                        <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                        @enderror
                    </div>
                    <div class="form-group col-md-6  @error('undesigned_surveyors') has-error @enderror">
                        {{-- <label class="">Local agents for the charterers/Owners M/s.</label> --}}
                        <label class="">Undesigned Surveyors</label>
                        <input  type="text" name="undesigned_surveyors" class="form-control" placeholder="Enter Value" id="" value="{{(old('undesigned_surveyors')!=null)? old('undesigned_surveyors'):$export_details->exportcargosCertificate->undersignedSurveyors}}">
                        @error('undesigned_surveyors')
                        <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                        @enderror
                    </div>
                    <div class="form-group col-md-6 col-lg-4 @error('certificate_date') has-error @enderror">
                        <label class="">Date</label>
                        <input  type="date" name="certificate_date" class="form-control" id="" value="{{(old('certificate_date')!=null)? old('certificate_date'):$export_details->exportcargosCertificate->date}}">
                        @error('certificate_date')
                        <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                        @enderror
                    </div>
                    <div class="form-group col-md-6 col-lg-4 @error('attended_at') has-error @enderror">
                        <label class="">Attended At</label>
                        <input  type="text" name="attended_at" class="form-control" placeholder="Enter Value" id="" value="{{(old('attended_at')!=null)? old('attended_at'):$export_details->exportcargosCertificate->attendedAt}}">
                        @error('attended_at')
                        <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                        @enderror
                    </div>
                    
                    <div class="form-group col-md-6 col-lg-4 @error('karachi_side') has-error @enderror">
                        <label class="">Karachi side, East/West</label>
                        <div class="mt-radio-inline">
                            @if(old('karachi_side')!=null)
                                <label class="mt-radio">
                                    <input type="radio" name="karachi_side" id="" value="east" {{ (old('karachi_side') == "east" )? 'checked':'' }} > East Side
                                    <span></span>
                                </label>
                                <label class="mt-radio">
                                    <input type="radio" name="karachi_side" id="" value="west" {{ (old('karachi_side') == "west" )? 'checked':'' }}> West Side
                                    <span></span>
                                </label>
                            @else
                                <label class="mt-radio">
                                    <input required  type="radio" name="karachi_side" id="" value="east" {{ ($export_details->exportcargosCertificate->side == "east" )? 'checked':'' }} > East Side
                                    <span></span>
                                </label>
                                <label class="mt-radio">
                                    <input required  type="radio" name="karachi_side" id="" value="west" {{ ($export_details->exportcargosCertificate->side == "west" )? 'checked':'' }}> West Side
                                    <span></span>
                                </label>
                            @endif
                        </div>
                        @error('karachi_side')
                        <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                        @enderror

                    </div>
                    
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="icon-settings font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase">Add Cargo Export</span>
                    </div>
                </div>
                <div class="portlet-body form">
                        
                        <div class="form-body row">
                        
                            <div class="form-group col-md-6 col-lg-4 @error('vessel_name') has-error @enderror ">
                                <label class="uppercase">Vessels Name</label>
                                <input required  list="vesselList" type="text" name="vessel_name" class="form-control" placeholder="Search Vessels Name" id="export_vessel" value="{{(old('vessel_name')!=null)? old('vessel_name'):$export_details->vessel->name}}">
                                
                                <datalist id="vesselList">

                                </datalist>
                                
                                @error('vessel_name')
                                <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group col-md-6 col-lg-4 @error('voy') has-error @enderror">
                                <label class="uppercase">VOY</label>
                                <input required  type="text" name="voy" class="form-control" placeholder="Enter VOY" value="{{(old('voy')!=null)? old('voy'):$export_details->voy}}">
                                @error('voy')
                                <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group col-md-6 col-lg-4 @error('of') has-error @enderror">
                                <label class="uppercase">OF</label>
                                <input required  type="date" name="of" class="form-control" placeholder="Enter OF" value="{{(old('of')!=null)? old('of'):$export_details->departureDate}}">
                                @error('of')
                                <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group col-md-6 @error('shipper_clearing_agent') has-error @enderror">
                                <label class="uppercase">Shipper/Clearing Agent</label>
                                <input required  list="agentList" type="text" name="shipper_clearing_agent" class="form-control" placeholder="Enter Shipper/Clearing Agent" id="export_agent" value="{{(old('shipper_clearing_agent')!=null)? old('shipper_clearing_agent'):$export_details->agent->name}}">
                                
                                <datalist id="agentList">

                                </datalist>
                                
                                @error('shipper_clearing_agent')
                                <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group col-md-6 @error('consignee') has-error @enderror">
                                <label class="uppercase">Consignee</label>
                                <input required  list="consigneeList" type="text" name="consignee" class="form-control" placeholder="Search Consignee" id="export_consignee" value="{{(old('consignee')!=null)? old('consignee'):$export_details->frieghtforwarder->name}}">
                                
                                <datalist id="consigneeList">

                                </datalist>
                                
                                @error('consignee')
                                <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                @enderror
                            </div>

                            

                            <div class="form-group col-md-6 @error('description_of_cargo') has-error @enderror">
                                <label for="single" class="control-label uppercase">Description of cargo</label>
                                <input required  type="text" class="form-control" name="description_of_cargo" placeholder="Enter Description Of Cargo" value="{{(old('description_of_cargo')!=null)? old('description_of_cargo'):$export_details->description}}">
                                @error('description_of_cargo')
                                <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                @enderror
                            </div>

                            

                            <div class="form-group col-md-6  @error('port') has-error @enderror">
                                <label for="single" class="control-label uppercase">Select Port/Destination</label>
                                @if (old('port')!=null)
                                    <select id="single" class="form-control select2" name="port">
                                        <option selected disabled>Select Option</option>
                                        @foreach($port as $data)
                                            <option value="{{$data->id}}" {{(old('port') == $data->id)? 'selected':''}}>{{$data->nameWoDiac}} / {{$data->country}} </option>
                                        @endforeach
                                    </select>
                                @else
                                    <select id="single" class="form-control select2" name="port">
                                        <option selected disabled>Select Option</option>
                                        @foreach($port as $data)
                                            <option value="{{$data->id}}" {{($export_details->port->id == $data->id)? 'selected':''}}>{{$data->nameWoDiac}} / {{$data->country}} </option>
                                        @endforeach
                                    </select>
                                @endif
                                
                                @error('port')
                                <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                @enderror
                            </div>

                            <div class="form-group col-md-4 @error('export_cargo_container') has-error @enderror">
                                <label for="single" class="control-label uppercase">Container No.</label>
                                <input list="containerList" id="export_container" type="text" class="form-control" name="export_cargo_container" placeholder="Enter Description Of Cargo" value="{{(old('export_cargo_container')!=null)? old('export_cargo_container'):$export_details->container->name}}">
                                <datalist id="containerList">

                                </datalist>
                                @error('export_cargo_container')
                                <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group col-md-4 @error('size') has-error @enderror">
                                <label>Size</label>
                                <select  id="size" name="size" class="form-control">
                                    <option value>Select Size</option>
                                    <option value="20" {{(old('size')!=null)? (old('size') == 20?'selected':''):(($export_details->container->size == 20)? 'selected':'')}}>20</option>
                                    <option value="40" {{(old('size')!=null)? (old('size') == 40?'selected':''):(($export_details->container->size == 40)? 'selected':'')}}>40</option>
                                </select>
                                @error('size')
                                <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                @enderror
                            </div>
                            <div class="form-group col-md-4 @error('container_type') has-error @enderror">
                                <label>Container Type</label>
                                <select class="form-control " name="container_type" id="container_type">
                                    <option value>Select Container Type</option>
                                    @foreach ($itemtypes as $item)
                                        <option value="{{$item->id}}" {{(old('container_type')!=null)? (old('container_type') == $item->id?'selected':''):(($export_details->container->ContainerType->id == $item->id)? 'selected':'')}}>{{$item->name}}</option>
                                    @endforeach
                                </select>
                                @error('container_type')
                                    <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                @enderror
                            </div>

                            <div class="col-sm-12">
                                <h4 class="caption-subject bold uppercase">Export Cargo Details <a class="btn btn-success" style="float: right;" id="add_detail">Add More Detail</a></h4>
                                
                            </div>
                            <div class="col-sm-12">
                                <h5 class="caption-subject"> 
                                    <input type="checkbox" name="mean_check" id="" {{ ($export_details->mean_check == 'yes')? "checked":"" }}> <strong> Check this box if there is unknown no of packages of multiple Length x Breadth x Depth</strong>
                                </h5>
                            </div>
                            <div class="col-sm-12">
                                <table class="table table-striped table-bordered" style="margin-top:20px ">
                                    <thead>
                                        <tr>
                                            
                                            <th>Date</th>
                                            <th>Description Of Cargo</th>
                                            <th>Marks & NO.</th>
                                            {{-- <th>Container NO.</th> --}}
                                            
                                            {{-- <th>Size</th> --}}
                                            <th>Seal NO.</th>
                                            <th>Packages Nos.</th>
                                            <th>NO. Of Packages</th>
                                            {{-- <th>Action</th> --}}
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (old('detail_date') != null)
                                            @foreach (old('detail_date') as $key => $oldValues)

                                            <input  type="hidden" name="export_detail_id[]" value="{{old('export_detail_id.'.$key)}}">

                                                @php
                                                    $rand = rand();
                                                @endphp
                                                <tr>
                                                    <td>
                                                        <div class="form-group @error('detail_date.'.$key) has-error @enderror">
                                                            <input type="date" name="detail_date[]" class="form-control" value="{{$oldValues}}" placeholder="" id="">
                                                        </div>
                                                        @error('detail_date.'.$key)
                                                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                                        @enderror
                                                    </td>

                                                    <td>
                                                        <div class="form-group @error('detail_doc.'.$key) has-error @enderror">
                                                        <input type="text" name="detail_doc[]" class="form-control" placeholder="" id="" value="{{old('detail_doc.'.$key)}}">
                                                        
                                                        </div>
                                                        @error('detail_doc.'.$key)
                                                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                                        @enderror
                                                    </td>

                                                    <td>
                                                        <div class="form-group @error('detail_marks_no.'.$key) has-error @enderror">
                                                        <input type="text" class="form-control " value="{{old('detail_marks_no.'.$key)}}" name="detail_marks_no[]" >
                                                        </div>
                                                        @error('detail_marks_no.'.$key)
                                                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                                        @enderror
                                                    </td>
                                                    
                                                    {{-- <td>
                                                        <div class="form-group @error('detail_container_no.'.$key) has-error @enderror">
                                                        <input list="detail_container_no_{{$rand}}" type="text" class="form-control export_container" value="{{old('detail_container_no.'.$key)}}" name="detail_container_no[]" data-rand="{{$rand}}">
                                                        <datalist id="detail_container_no_{{$rand}}"> 

                                                        </datalist>
                                                        </div>
                                                        @error('detail_container_no.'.$key)
                                                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                                        @enderror
                                                    </td> --}}

                                                    {{-- <td>
                                                        <div class="form-group @error('detail_size.'.$key) has-error @enderror">
                                                        <input type="text" class="form-control " value="{{old('detail_size.'.$key)}}" name="detail_size[]">
                                                        </div>
                                                        @error('detail_size.'.$key)
                                                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                                        @enderror
                                                    </td> --}}

                                                    <td>
                                                        <div class="form-group @error('detail_seal_no.'.$key) has-error @enderror">
                                                        <input type="text" class="form-control " value="{{old('detail_seal_no.'.$key)}}" name="detail_seal_no[]">
                                                        </div>
                                                        @error('detail_seal_no.'.$key)
                                                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                                        @enderror
                                                    </td>

                                                    <td>
                                                        <div class="form-group @error('detail_packagesNos.'.$key) has-error @enderror">
                                                        <input type="text" class="form-control " value="{{old('detail_packagesNos.'.$key)}}" name="detail_packagesNos[]">
                                                        </div>
                                                        @error('detail_packagesNos.'.$key)
                                                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                                        @enderror
                                                    </td>

                                                    <td>
                                                        <div class="form-group @error('detail_no_of_packages.'.$key) has-error @enderror">
                                                        <input type="text" class="form-control " value="{{old('detail_no_of_packages.'.$key)}}" name="detail_no_of_packages[]">
                                                        </div>
                                                        @error('detail_no_of_packages.'.$key)
                                                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                                        @enderror
                                                    </td>

                                                    <th>
                                                        @if (old('export_detail_id.'.$key) == null)
                                                            <button type="button" class="btn btn-danger deleteRow"><i class="fa fa-trash"></i></button>
                                                        @endif
                                                    </th>
                                                </tr>
                                            @endforeach
                                        @elseif ($export_details->exportcargosdetail != null)
                                            @foreach ($export_details->exportcargosdetail as $key => $values)
                                                @php
                                                    $rand = rand();
                                                @endphp
                                                <input  type="hidden" name="export_detail_id[]" value="{{$values->id}}">
                                                <tr>
                                                    <td>
                                                        <div class="form-group @error('detail_date.'.$key) has-error @enderror">
                                                            <input required  type="date" name="detail_date[]" class="form-control" value="{{$values->cargo_date}}" placeholder="" id="">
                                                        </div>
                                                        @error('detail_date.'.$key)
                                                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                                        @enderror
                                                    </td>

                                                    <td>
                                                        <div class="form-group @error('detail_doc.'.$key) has-error @enderror">
                                                        <input required  type="text" name="detail_doc[]" class="form-control" placeholder="" id="" value="{{$values->description}}">
                                                        
                                                        </div>
                                                        @error('detail_doc.'.$key)
                                                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                                        @enderror
                                                    </td>

                                                    <td>
                                                        <div class="form-group @error('detail_marks_no.'.$key) has-error @enderror">
                                                        <input required  type="text" class="form-control " value="{{$values->marksAndNo}}" name="detail_marks_no[]" >
                                                        </div>
                                                        @error('detail_marks_no.'.$key)
                                                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                                        @enderror
                                                    </td>
                                                    
                                                    {{-- <td>
                                                        <div class="form-group @error('detail_container_no.'.$key) has-error @enderror">
                                                        <input list="detail_container_no_{{$rand}}" required  type="text" class="form-control export_container" value="{{$values->container->name}}" name="detail_container_no[]" data-rand="{{$rand}}">
                                                        <datalist id="detail_container_no_{{$rand}}"> 

                                                        </datalist>
                                                        </div>
                                                        @error('detail_container_no.'.$key)
                                                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                                        @enderror
                                                    </td> --}}

                                                    {{-- <td>
                                                        <div class="form-group @error('detail_size.'.$key) has-error @enderror">
                                                        <input required  type="text" class="form-control " value="{{$values->size}}" name="detail_size[]">
                                                        </div>
                                                        @error('detail_size.'.$key)
                                                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                                        @enderror
                                                    </td> --}}

                                                    <td>
                                                        <div class="form-group @error('detail_seal_no.'.$key) has-error @enderror">
                                                        <input required  type="text" class="form-control " value="{{$values->sealNo}}" name="detail_seal_no[]">
                                                        </div>
                                                        @error('detail_seal_no.'.$key)
                                                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                                        @enderror
                                                    </td>

                                                    <td>
                                                        <div class="form-group @error('detail_packagesNos.'.$key) has-error @enderror">
                                                        <input type="text" class="form-control " value="{{$values->packagesNos}}" name="detail_packagesNos[]">
                                                        </div>
                                                        @error('detail_packagesNos.'.$key)
                                                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                                        @enderror
                                                    </td>

                                                    <td>
                                                        <div class="form-group @error('detail_no_of_packages.'.$key) has-error @enderror">
                                                        <input required  type="text" class="form-control " value="{{$values->noOfPackages}}" name="detail_no_of_packages[]">
                                                        </div>
                                                        @error('detail_no_of_packages.'.$key)
                                                            <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                                                        @enderror
                                                    </td>

                                                    {{-- <th>
                                                        @if ($values->average == 0)
                                                            <button type="button" class="btn btn-danger deleteRow"><i class="fa fa-trash"></i></button>
                                                        @endif
                                                    </th> --}}

                                                    <input type="hidden" name="export_detail_delete[]" value="no">
                                                </tr>
                                            @endforeach
                                        @else
                                        @php
                                            $rand = rand();
                                        @endphp
                                        <tr>
                                            <td>
                                                <input required  type="date" name="detail_date[]" class="form-control" placeholder="" id="">
                                            </td>
                                            <td>
                                                <input required  type="text" name="detail_doc[]" class="form-control" placeholder="" id="">
                                            </td>
                                            <td>
                                                <input required  type="text" name="detail_marks_no[]" class="form-control" placeholder="" id="">
                                            </td>
                                            {{-- <td>
                                                
                                                <input list="detail_container_no_{{$rand}}" required  type="text" class="form-control export_container" name="detail_container_no[]" data-rand="{{$rand}}">
                                                <datalist id="detail_container_no_{{$rand}}"> 

                                                </datalist>
                                            </td> --}}
                                            {{-- <td>
                                                <input required  type="text" name="detail_size[]" class="form-control" placeholder="" id="">
                                            </td> --}}
                                            <td>
                                                <input required  type="text" name="detail_seal_no[]" class="form-control" placeholder="" id="">
                                            </td>
                                            <td>
                                                <input type="text" class="form-control " name="detail_packagesNos[]">
                                            </td>
                                            <td>
                                                <input required  type="number" name="detail_no_of_packages[]" class="form-control" min="0" placeholder="" id="">
                                            </td>
                                            <td></td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                        <div class="form-actions">
                            <button type="submit" id="alert_msg"
                            class="btn btn-success mt-sweetalert" 
                            data-title="Do you agree to the Terms and Conditions?" 
                            data-type="info" data-show-confirm-button="true" data-confirm-button-class="btn-success" data-show-cancel-button="true" 
                            data-cancel-button-class="btn-default" data-close-on-confirm="false" data-close-on-cancel="false" data-confirm-button-text='Yes, I agree'
                            data-cancel-button-text='No, I disagree' data-popup-title-success="Thank you" 
                            data-popup-message-success="You have agreed to our Terms and Conditions" 
                            data-popup-title-cancel="Cancelled" data-popup-message-cancel="You have disagreed to our Terms and Conditions">Save</button>
                            <a href="{{route('export-cargo.index')}}" class="btn default">Cancel</a>
                        </div>
                    
                </div>
            </div>
        </div>
    </div>
</form>
{{-- <td>
    <input list="detail_container_no_{{$rand}}" required  type="text" class="form-control export_container" name="detail_container_no[]" data-rand="{{$rand}}">
    <datalist id="detail_container_no_{{$rand}}"> 

    </datalist>
</td> --}}
    <script type="text/html" id="data_row">
        @php
            $rand = rand();
        @endphp
        <tr>
            <td>
                <input required  type="date" name="detail_date[]" class="form-control" placeholder="" id="">
            </td>
            <td>
                <input required  type="text" name="detail_doc[]" class="form-control" placeholder="" id="">
            </td>
            <td>
                <input required  type="text" name="detail_marks_no[]" class="form-control" placeholder="" id="">
            </td>
            
            <td>
                <input required  type="text" name="detail_seal_no[]" class="form-control" placeholder="" id="">
            </td>
            <td>
                <input type="text" class="form-control " name="detail_packagesNos[]">
            </td>
            <td>
                <input required  type="text" name="detail_no_of_packages[]" class="form-control" placeholder="" id="">
            </td>
            <td>
                <a class="btn btn-danger deleteRow"> <i class="fa fa-trash"></i></a> 
            </td>
        </tr>
    </script>
@endsection

@section('customJS')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{asset('assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js')}}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{asset('assets/pages/scripts/components-select2.min.js')}}" type="text/javascript"></script>
    {{-- <script src="{{asset('assets/pages/scripts/ui-sweetalert.min.js')}}" type="text/javascript"></script> --}}
    <!-- END PAGE LEVEL SCRIPTS -->

    <script>
        $("#export_agent").keyup(function (){  

            let val = $(this).val();
            
            $.ajax({
               type:'POST',
               url:'{{ route("AjaxCallForExportCargoToGetAgent")}}',
               data:{ _token : '<?php echo csrf_token() ?>',
                     value : val,
                    },
               success:function(data) {
                $('#agentList').html("");
                $.each(data,function(key,val){
                    $('#agentList').append('<option>'+val.name+'</option>');
                })
               }
            });
        });

        $("#export_vessel").keyup(function (){  

            let val = $(this).val();

            $.ajax({
                type:'POST',
                url:'{{ route("AjaxCallForExportCargoToGetVessel")}}',
                data:{ _token : '<?php echo csrf_token() ?>',
                        value : val,
                        },
                success:function(data) {
                    $('#vesselList').html("");
                    $.each(data,function(key,val){
                        $('#vesselList').append('<option>'+val.name+'</option>');
                    })
                }
            });
        });

        $("#export_consignee").keyup(function (){  

            let val = $(this).val();

            $.ajax({
                type:'POST',
                url:'{{ route("AjaxCallForExportCargoToGetConsignee")}}',
                data:{ _token : '<?php echo csrf_token() ?>',
                        value : val,
                        },
                success:function(data) {
                    $('#consigneeList').html("");
                    $.each(data,function(key,val){
                        $('#consigneeList').append('<option>'+val.name+'</option>');
                    })
                }
            });
        });


        $("#export_container").keyup(function() {
            let val = $(this).val();
            $.ajax({
                type:'POST',
                url:'{{ route("AjaxCallForExportCargoToGetContainer")}}',
                data:{ _token : '<?php echo csrf_token() ?>',
                        value : val,
                        },
                success:function(data) {
                    $('#containerList').html("");
                    $.each(data,function(key,val){
                        $('#containerList').append('<option>'+val.name+'</option>');
                    })
                }
            });
        });

        // AjaxCallForExportCargoToGetContainer

        $("#add_detail").click(function(){  
            $('tbody').append($("#data_row").html());
        });

        
        $("table").on("click", ".deleteRow", function() {
            // $(this).closest("tr")
            $(this).closest("tr").remove();
        });

        

        var SweetAlert = function () {
            return {
                init: function () {
                    $(".mt-sweetalert").each(function () {
                        var t = $(this).data("title"),
                            a = $(this).data("message"),
                            s = $(this).data("type"),
                            e = $(this).data("allow-outside-click"),
                            n = $(this).data("show-confirm-button"),
                            c = $(this).data("show-cancel-button"),
                            o = $(this).data("close-on-confirm"),
                            i = $(this).data("close-on-cancel"),
                            l = $(this).data("confirm-button-text"),
                            u = $(this).data("cancel-button-text"),
                            h = $(this).data("popup-title-success"),
                            d = $(this).data("popup-message-success"),
                            r = $(this).data("popup-title-cancel"),
                            f = $(this).data("popup-message-cancel"),
                            p = $(this).data("confirm-button-class"),
                            m = $(this).data("cancel-button-class");
                        $(this).click(function () {
                            swal({
                                title: t,
                                text: a,
                                type: s,
                                allowOutsideClick: e,
                                showConfirmButton: n,
                                showCancelButton: c,
                                confirmButtonClass: p,
                                cancelButtonClass: m,
                                closeOnConfirm: o,
                                closeOnCancel: i,
                                confirmButtonText: l,
                                cancelButtonText: u
                            }, function (t) {
                                t ? myAlert() : swal(r, f, "error")
                            })
                        })
                    })
                }
            }
        }();

        $('#alert_msg').click(function (e){  
            e.preventDefault();
            SweetAlert.init();
        });

        function myAlert()
        {  
            $('#ExportForm').submit();
        }

        $('#export_container').blur(function(){
            console.log('bilal');
            textVal = $(this).val();
            reqUrl = "{{route('container-fetch')}}";
            $.getJSON(reqUrl+'/'+textVal,function(data){
                console.log('data',data);
                console.log('len',data.data.length);
                let len = data.data.length;
                if(len == 1)
                {
                    $('#size').val(data.data[0]['size']);
                    $('#size').attr("style", "pointer-events: none;");
                    $('#container_type').val(data.data[0]['itemtype_Id']);
                    $('#container_type').attr("style", "pointer-events: none;");
                }
                else{
                    // console.log('data is null');
                    $('#size').val('');
                    $('#size').attr("style", "pointer-events: auto;");
                    $('#container_type').val('');
                    $('#container_type').attr("style", "pointer-events: auto;");
                }
            });
        });
       
    </script>
    

@endsection