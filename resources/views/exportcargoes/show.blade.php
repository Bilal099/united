@extends('layouts.app')
@section('customCSS')
<link href="{{asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css">
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
@endsection
@section('content')
    @php
        $flag = 0;
        $flag2 = 0; // For remark button

        foreach ($export_details->exportcargosdetail as $item)
        {
            if($item->average != 0)
            {
                $flag = 1;
            }
        }
        foreach ($export_details->exportcargosdetail as $item)
        {
            if($item->average != 0)
            {
                $flag2 = 1;
            }
            elseif ($item->average == 0) 
            {
                $flag2 = 0;
                break;
            }
        }
    @endphp
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="icon-settings font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase">View Export Cargo</span>
                    </div>

                    @if ($flag == 0)
                        <a href="{{route('exportCargo.showCertificateOfMeasurement',$export_details->id)}}" class="btn btn-success pull-right">Add Certificate of measurement</a> 
                    @endif
                    <a href="{{route('export-cargo.index')}}" class="btn default pull-right" style="margin-right: 4px">Back</a>

                    {{-- <form action="{{route('exportCargo.showCertificateOfMeasurement')}}" method="post">
                        @csrf
                        <input type="hidden" name="id" value="{{$export_details->id}}">
                        <button type="submit" class="btn btn-success pull-right">Add Certificate of measurement</button>
                        <a href="{{route('export-cargo.index')}}" class="btn default pull-right">Cancel</a>

                    </form> --}}
                    {{-- <button class="btn btn-success pull-right" >Show Remarks</button> --}}
                </div>
                @if (Session::has('message'))
                    <div id="" class="custom-alerts alert alert-success fade in">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                        <i class="fa-lg fa fa-warning"></i>  
                        {{Session::get('message')}}
                    </div>
                @endif
                <div class="portlet-body">
                        <div class="form-body row">

                            <div class="col-sm-12">

                                <div class="col-xs-12">
                                    <div class="mt-element-ribbon bg-grey-steel">
                                        <div class="ribbon ribbon-round ribbon-color-danger uppercase">Export Cargo Certified By,</div>
                                        <p class="ribbon-content">Certified tha upon receipt of instruction from M/s. 
                                            <span class="caption-subject font-green bold uppercase">{{$export_details->exportcargosCertificate->agentName}}</span>
                                            as Local Agents for the charterers/owners M/s. 
                                            <span class="caption-subject font-green bold uppercase">{{$export_details->exportcargosCertificate->undersignedSurveyors}}</span>
                                            as undersigned surveyors, attended at 
                                            <span class="caption-subject font-green bold uppercase">{{$export_details->exportcargosCertificate->attendedAt}}</span>.
                                            <br>
                                            @if ($export_details->exportcargosCertificate->side = 'east')
                                                <span class="font-green uppercase bold"  >East</span>/<span class="uppercase bold">West</span>
                                            @else
                                                <span class="font-green uppercase bold"  >West</span>/<span class="uppercase bold">East</span>
                                            @endif
                                            Karachi on / from 
                                            <span class="caption-subject font-green bold uppercase">{{$export_details->exportcargosCertificate->date}}</span>
                                            for the purpose of carryinout cargo condition Survey at the time of Arrival that mentioned below:
                                        </p>
                                    </div>
                                </div>
                                
                                
                            </div>

                            <div class="col-sm-12">
                                <div class="portlet-title">
                                    <div class="caption font-red-sunglo">
                                        <i class="icon-settings font-red-sunglo"></i>
                                        <span class="caption-subject bold uppercase">Export Cargo</span>
                                        <a href="{{route('exportCargo.invoice',$export_details->id)}}" class="btn btn-success pull-right" target="_blank">PDF</a>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="col-sm-12">
                                
                                <table class="table table-striped table-bordered" style="margin-top:20px">
                                    <thead>
                                        <tr>
                                            <th>Added By</th>
                                            <th>Serial No.</th>
                                            <th>Vessels</th>
                                            <th>VOY</th>
                                            <th>OF</th>
                                            <th>Shipper/Clearing Agent</th>
                                            <th>Container No.</th>
                                            <th>Container Size</th>
                                            <th>Container Type</th>
                                            <th>Consignee</th>
                                            <th>Port/Destination</th>
                                            <th>Status</th>
                                            <th>Description Of Cargo</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{{$export_details->user->name}}</td>
                                            <td>{{$export_details->serialNo}}</td>
                                            <td>{{$export_details->vessel->name}}</td>
                                            <td>{{$export_details->voy}}</td>
                                            <td>{{date_format(date_create($export_details->departureDate),"j M, Y")}}</td>
                                            <td>{{$export_details->agent->name}}</td>
                                            <td>{{$export_details->container->name}}</td>
                                            <td>{{$export_details->container->size}}</td>
                                            <td>{{$export_details->container->ContainerType->name}}</td>
                                            <td>{{$export_details->frieghtforwarder->name}}</td>
                                            <td>{{$export_details->port->nameWoDiac}} / {{$export_details->port->country}}</td>
                                            <td>{{$export_details->status}}</td>
                                            <td>{{$export_details->description}}</td>
                                            
                                            
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            
                            
                                <div class="col-sm-12">
                                    <h4 class="caption-subject bold uppercase">Export Cargo Details
                                        @if ($flag2 == 1)
                                            <button class="btn btn-success pull-right" id="btn-toggle-remarks" data-value="hide">Show Edit Remarks</button>
                                        @endif
                                    </h4>
                                </div>
                            
                            
                            <div class="col-sm-12">
                                <div class="portlet-body">
                                <form action="{{route('exportCargo.remarks')}}" method="post" id="formRemarks">
                                    <input type="hidden" name="export_id" value="{{$export_details->id}}">
                                    @csrf
                                    <table class="table table-striped table-bordered table-hover" style="margin-top:20px" id="sample_table">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Date</th>
                                                <th>Description Of Cargo</th>
                                                <th>Marks & NO.</th>
                                                {{-- <th>Container NO.</th> --}}
                                                {{-- <th>Size</th> --}}
                                                <th>Seal NO.</th>
                                                <th>NO. Of Packages</th>
                                                <th>Remarks</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                $i = 1;
                                                
                                            @endphp
                                            @foreach ($export_details->exportcargosdetail as $item)
                                                <tr>
                                                    <td>{{$i}}</td>
                                                    <td>{{date_format(date_create($item->cargo_date),"j M, Y")}}</td>
                                                    <td>{{$item->description}}</td>
                                                    <td>{{$item->marksAndNo}}</td>
                                                    {{-- <td>{{$item->container->name}}</td> --}}
                                                    {{-- <td>{{$item->size}}</td> --}}
                                                    <td>{{$item->sealNo}}</td>
                                                    <td>{{$item->noOfPackages}}</td>
                                                    <td class="export_remarks">{{$item->remarks}}</td>
                                                    <td class="remarks-colum hidden">
                                                        <input type="hidden" name="exportdetailId[]" value="{{$item->id}}">
                                                        <input type="text" value="{{$item->remarks}}" name="remarks[]" class="form-control" placeholder="Enter Remarks" />
                                                    </td>
                                                </tr>
                                                @php
                                                    $i += 1;
                                                @endphp
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <button type="submit" form="formRemarks" class="btn blue hidden updateRemarksBtn">Update Remarks</button>

                                </form>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            {{-- <button type="submit" form="formRemarks" class="btn blue hidden updateRemarksBtn">Update Remarks</button> --}}
                            {{-- <a href="{{route('export-cargo.index')}}" class="btn default">Cancel</a> --}}
                        </div>
                </div>
            </div>
        </div>
    </div>
    {{-- @php
        $flag = 0;

        foreach ($export_details->exportcargosdetail as $item)
        {
            
            if($item->average != 0)
            {
                $flag = 1;
            }
        }
    @endphp --}}
    @if ($flag == 1)
        <div class="row"> 
            <div class="col-md-12">
                <!-- BEGIN SAMPLE FORM PORTLET-->
                <div class="portlet light ">
                    <div class="portlet-title">
                        <div class="caption font-red-sunglo">
                            <i class="icon-settings font-red-sunglo"></i>
                            <span class="caption-subject bold uppercase">View Export Cargo Measurement</span>

                        </div>

                        <div class="actions">
                            @if ($flag2 == 1)
                                <a class="btn btn-success " id="" href="{{route('exportcargoDetail.invoice',$export_details->id)}}" target="_blank" >Measurements PDF</a>
                            @endif

                            <button class="btn btn-success " id="btn-toggle-measurement" data-value="hide" >Show Edit Measurements</button>
                        </div>
                        

                    </div>
                    <div class="portlet-body">
                            <div class="form-body row">
                                <form action="{{route('exportCargo.AddCertificateOfMeasurement')}}" method="post" id="formMeasurements">
                                    @csrf
                                    <input type="hidden" name="export_id" value="{{$export_details->id}}">

                                    <table class="table table-striped table-bordered" style="margin-top:20px" id="importDetailTable">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Marks & NO.</th>
                                                <th>Packages Nos.</th>
                                                <th>NO. Of Packages</th>
                                                <th>Length C M S</th>
                                                <th>Breadth C M S</th>
                                                <th>Depth C M S</th>
                                                <th>Average M<sup>3</sup> </th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                $i = 1;
                                            @endphp
                                            @foreach ($export_details->exportcargosdetail as $item)

                                            {{-- @if ($item->average != 0) --}}
                                                
                                            
                                                <tr>
                                                    <td>{{$i}}</td>
                                                    <td>{{$item->marksAndNo}}</td>
                                                    <td>{{$item->packagesNos}}</td>
                                                    <td class="noOfPackages">{{$item->noOfPackages}}</td>
                                                    <td class="export_detail_editable_disable">{{$item->length}}</td>
                                                    <td class="export_detail_editable_disable">{{$item->breadth}}</td>
                                                    <td class="export_detail_editable_disable">{{$item->depth}}</td>

                                                    <td class="export_detail_editable_enable hidden ">
                                                        <input type="hidden" name="exportdetailId[]" value="{{$item->id}}">
                                                        <input min="0" step="0.01" type="number" value="{{(($item->length != null)? $item->length:0)}}" name="length[]" class="form-control" placeholder="Enter Length" />
                                                    </td>
                                                    <td class="export_detail_editable_enable hidden">
                                                        <input min="0" step="0.01" type="number" value="{{(($item->breadth != null)? $item->breadth:0)}}" name="breadth[]" class="form-control" placeholder="Enter Breadth" />
                                                    </td>
                                                    <td class="export_detail_editable_enable hidden">
                                                        <input min="0" step="0.01" type="number" value="{{(($item->depth != null)? $item->depth:0)}}" name="depth[]" class="form-control" placeholder="Enter Depth" />
                                                    </td>

                                                    <td class="export_detail_editable_disable avg_calculation_td">{{$item->average}}</td>
                                                    <td class="export_detail_editable_enable hidden">
                                                        <input min="0" step="0.01" type="number" value="{{(($item->average != null)? $item->average:0)}}" name="average[]" class="form-control avg_calculation_input" readonly />
                                                    </td>
                                                    {{-- <td class="export_detail_editable_disable avg_calculation_td">{{$item->average}}</td> --}}
                                                    
                                                </tr>
                                                @php
                                                    $i += 1;
                                                @endphp
                                            {{-- @endif --}}
                                                
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <div class="form-actions">
                                        <button type="submit"  class="btn blue  updateMeasurementsBtn hidden">Update Measurements</button>
                                        {{-- <a href="{{route('export-cargo.show',$export_id)}}" class="btn default">Cancel</a> --}}
                                    </div>
                                </form>
                                
                            </div>
                    </div>
                </div>
            </div>
        </div>

        
        
    @endif
    
@endsection
@section('customJS')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{asset('assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js')}}" type="text/javascript"></script>

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
{{-- <script src="{{asset('assets/pages/scripts/ui-sweetalert.min.js')}}" type="text/javascript"></script> --}}
<!-- END PAGE LEVEL SCRIPTS -->

    <script>
        $(document).on('click','#btn-toggle-remarks',function(){
            if($(this).attr('data-value') === 'hide')
            {
                $('.remarks-colum').removeClass('hidden');
                $(this).attr('data-value','show');
                $(this).text('Hide Edit Remarks');
                $('.updateRemarksBtn').removeClass('hidden');
                $('.export_remarks').addClass('hidden');
            }
            else
            {
                $('.remarks-colum').addClass('hidden');
                $(this).attr('data-value','hide');
                $(this).text('Show Edit Remarks');
                $('.updateRemarksBtn').addClass('hidden');
                $('.export_remarks').removeClass('hidden');
            }
        })

        $(document).on('click','#btn-toggle-measurement',function(){
            if($(this).attr('data-value') === 'hide')
            {
                $('.export_detail_editable_enable').removeClass('hidden');
                $(this).attr('data-value','show');
                $(this).text('Hide Edit Measurements');
                $('.updateMeasurementsBtn').removeClass('hidden');
                $('.export_detail_editable_disable').addClass('hidden');
            }
            else
            {
                $('.export_detail_editable_enable').addClass('hidden');
                $(this).attr('data-value','hide');
                $(this).text('Show Edit Measurements');
                $('.updateMeasurementsBtn').addClass('hidden');
                $('.export_detail_editable_disable').removeClass('hidden');
            }
        })

        

        $(document).on('keyup','.export_detail_editable_enable',function(){

            let ref = $(this).closest('tr');

            let lenght              = parseFloat(ref.find('input[name="length[]"]').val()) || 0;
            let breadth             = parseFloat(ref.find('input[name="breadth[]"]').val()) || 0;
            let depth               = parseFloat(ref.find('input[name="depth[]"]').val()) || 0;
            let noOfPackages        = parseFloat(ref.find('.noOfPackages').text()) || 0;

            if(lenght == 0 || lenght == null || lenght == '')
            {
                ref.find('input[name="length[]"]').val("0");
                ref.find('.avg_calculation_input').val("0");

            }
            if(breadth == 0 || breadth == null || breadth == '')
            {
                ref.find('input[name="breadth[]"]').val("0");
                ref.find('.avg_calculation_input').val("0");

            }
            if(depth == 0 || depth == null || depth == '')
            {
                ref.find('input[name="depth[]"]').val("0");
                ref.find('.avg_calculation_input').val("0");
            }

            let avg = (lenght * breadth * depth * noOfPackages) * 0.000001;

            ref.find('.avg_calculation_td').text(((avg != 0) ? avg:0));
            ref.find('.avg_calculation_input').val(((avg != 0) ? avg:0));
        })

        
    </script>
@endsection