@extends('layouts.app')

@section('customCSS')
    
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="icon-settings font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase">Add New Container</span>
                </div>
            </div>
            <div class="portlet-body form">
                <form role="form" method="POST" action="{{url('container')}}">
                    @csrf
                    <div class="form-body">
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" name="name" class="form-control" placeholder="Name">
                        </div>
                        <div class="form-group">
                            <label>Size</label>
                            <input type="text" name="size" class="form-control" placeholder="Size">
                        </div>
                        <div class="form-group">
                            <label>Container Type</label>
                            {{-- <input type="text" name="number" class="form-control" placeholder="Number"> --}}
                            <select name="ItemType" id="" class="form-control">
                                <option selected disabled>Select Option</option>
                                @foreach ($ItemType as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn blue">Save</button>
                        <a href="{{url('container')}}" class="btn default">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('customJS')
    
@endsection