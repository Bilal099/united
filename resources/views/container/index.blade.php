@extends('layouts.app')

@section('customCSS')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->

    <style>
        a.dt-button.buttons-print.btn.dark.btn-outline {
            display: none;
        }

        .btn.btn-outline.red {
            display: none;
        }

        .btn.btn-outline.green{
            display: none;

        }

        .btn.btn-outline.yellow{
            display: none;
            
        }

        .btn.btn-outline.purple{
            display: none;
            
        }

        .btn.btn-outline.dark{
            display: none;
            
        }
    </style>
@endsection

@section('content')
{{-- @dd($container) --}}
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase">Containers</span>
                </div>
                <div class="tools"> </div>
                <div>
                    <a href="{{url('container/create')}}" class="btn btn-danger" style="float:right">Add Container</a>
                </div>
            </div>

            @if (session('success'))
            <div id="prefix_1128615150407" class="custom-alerts alert alert-success fade in">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                <i class="fa-lg fa fa-warning"></i>  
                {{session('success')}}
            </div>
            @endif
            
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_1">
                    <thead>
                        <tr>
                            <th>S.No</th>
                            <th>Name</th>
                            <th>Size</th>
                            <th>Container Type</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    
                    <tbody>
                        @php
                            $i = 1;
                        @endphp
                        @foreach ($container_detail as $item)
                        <tr>
                            <td>{{$i}}</td>
                            <td>{{$item->name}}</td>
                            <td>{{$item->size}}</td>
                            <td>{{$item->ContainerType->name}}</td>
                            <td>
                                <form action="{{route('container.destroy',$item->id)}}" method="post">
                                    @method('DELETE')
                                    @csrf
                                    <a href="{{route('container.edit',$item->id)}}" class="btn btn-primary">Edit</a>
                                    @if (count($item->exportcargoes) == 0 && count($item->importcargoes) == 0)
                                        <button type="submit" class="btn btn-danger delete-btn">Delete</button>
                                    @endif
                                </form>
                            </td>
                        </tr>
                        @php
                            $i += 1;
                        @endphp
                        @endforeach
                        {{-- <tr>
                            <td>Tiger Nixon</td>
                            <td>System Architect</td>
                            <td>Edinburgh</td>
                            <td>61</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Garrett Winters</td>
                            <td>Accountant</td>
                            <td>Tokyo</td>
                            <td>63</td>
                            <td></td>
                        </tr> --}}
                        
                        
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
        
    </div>
</div>
@endsection

@section('customJS')
    <script src="{{asset('assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>

    <script>
        $('.delete-btn').click(function (e) { 
            e.preventDefault();
            
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this record!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                })
                .then((willDelete) => {
                if (willDelete) {
                    $(this).closest('form').submit();
                } else {
                    swal("Your record is safe!");
                }
            });
        });
    </script>
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <!-- END PAGE LEVEL SCRIPTS -->
@endsection