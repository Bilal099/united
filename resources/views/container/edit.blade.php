@extends('layouts.app')

@section('customCSS')
    
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light ">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="icon-settings font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase">Add New Container</span>
                </div>
            </div>
            <div class="portlet-body form">
                <form role="form" method="POST" action="{{route('container.update',$Container->id)}}">
                    @method('PUT')

                    @csrf
                    <div class="form-body">
                        <div class="form-group @error('name') has-error @enderror">
                            <label>Name</label>
                            <input type="text" name="name" class="form-control" placeholder="Name" value="{{old('name') != null ?old('name'):$Container->name}}">
                            @error('name')
                                <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                            @enderror
                        </div>
                        <div class="form-group @error('size') has-error @enderror">
                            <label>Size</label>
                            <input type="text" name="size" class="form-control" placeholder="Size" value="{{old('size') != null ?old('size'):$Container->size}}">
                            @error('size')
                                <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                            @enderror
                        </div>
                        <div class="form-group @error('ItemType') has-error @enderror">
                            {{-- <label>Number</label>
                            <input type="text" name="number" class="form-control" placeholder="Number" value="{{old('name') != null ?old('name'):$Container->number}}"> --}}

                            <label>Container Type</label>
                            <select name="ItemType" id="" class="form-control">
                                <option selected disabled>Select Option</option>
                                @foreach ($ItemType as $item)
                                    <option value="{{$item->id}}" {{((old('ItemType')!=null)? (old('ItemType')==$item->id? 'selected':''):($item->id==$Container->itemtype_Id? 'selected':''))}} >{{$item->name}}</option>
                                @endforeach
                            </select>
                            @error('ItemType')
                                <span class="text-sm text-danger" style="padding: 5px;font-size:12px">{{$message}}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn blue">Save</button>
                        <a href="{{route('container.index')}}" class="btn default">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('customJS')
    
@endsection