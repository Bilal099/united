@extends('layouts.app')
@section('customCSS')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->

    <style>
        a.dt-button.buttons-print.btn.dark.btn-outline {
            display: none;
        }

        .btn-outline {
            display: none;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light ">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">View All Item Types</span>
                    </div>
                    <div class="pull-right"><a href="{{route('itemtypes.create')}}" class="btn btn-danger">Add Item Type</a> </div>
                </div>
                @if (session('success'))
                <div id="prefix_1128615150407" class="custom-alerts alert alert-success fade in">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                    <i class="fa-lg fa fa-warning"></i>  
                    {{session('success')}}
                </div>
                @endif
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="sample_table">
                        <thead>
                            <tr>
                                <th>Sr.No</th>
                                <th>Name</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $a =0;
                            @endphp
                            @foreach ($itemtypes as $key => $item)
                                <tr>
                                    <td>{{++$a}}</td>
                                    <td>{{$item->name}}</td>
                                    <td>
                                        
                                        <form action="{{route('itemtypes.destroy',$item->id)}}" method="post">
                                            @method('DELETE')
                                            @csrf
                                            <a href="{{route('itemtypes.edit',$item->id)}}" class="btn btn-primary">Edit</a>
                                            @if (count($item->containers) == 0)
                                                <button type="submit" class="btn btn-danger delete-btn">Delete</button>                                                
                                            @endif
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
@endsection
@section('customJS')
    <script src="{{asset('assets/global/scripts/datatable.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <!-- END PAGE LEVEL SCRIPTS -->
    <script>
        $(document).ready(function(){
            $('#sample_table').dataTable();
        })

        $('.delete-btn').click(function (e) { 
            e.preventDefault();
            
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this record!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                })
                .then((willDelete) => {
                if (willDelete) {
                    $(this).closest('form').submit();
                } else {
                    swal("Your record is safe!");
                }
            });
        });
    </script>
@endsection