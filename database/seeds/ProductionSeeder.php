<?php

use Illuminate\Database\Seeder;
use App\Models\ItemType;

class ProductionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // User::create([
        //     'name' => 'admin',
        //     'email' => 'admin@united.com',
        //     'password' => Hash::make($data['12345678']),
        //     'userTypeId' => 1
        // ]);
        ItemType::create(['name'=>'DV-Dry Van']);
        ItemType::create(['name'=>'FR-Flatrck']);
        ItemType::create(['name'=>'HC-High Cube']);
        ItemType::create(['name'=>'OT-Open Top']);
        ItemType::create(['name'=>'PL-Platform']);

    }
}
