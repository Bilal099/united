<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    
    return redirect()->route('login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::middleware(['auth'])->group(function () {
    
    Route::resource('container', 'ContainerController');
    Route::resource('frieghtForWarder', 'FrieghtForWarderController');
    Route::resource('vessels', 'VesselController');
    Route::resource('agents', 'AgentController');
    Route::resource('import-cargo', 'ImportCargoController');
    Route::post('import-cargo/update-remarks','ImportCargoController@updateRemarks')->name('importCargo.remarks');
    Route::get('importcargo/invoice/{importId}/{status}','ImportCargoController@generateImportPdf')->name('importCargo.invoice');
    
    Route::resource('export-cargo', 'ExportCargoController');
    Route::post('export-cargo/update-remarks','ExportCargoController@updateRemarks')->name('exportCargo.remarks');

    // Route::post('export-cargo/show-certificate-of-measurement','ExportCargoController@showCertificateOfMeasurement')->name('exportCargo.showCertificateOfMeasurement');
    Route::get('export-cargo/show-certificate-of-measurement/{id}','ExportCargoController@showCertificateOfMeasurement')->name('exportCargo.showCertificateOfMeasurement');

    Route::post('export-cargo/add-certificate-of-measurement','ExportCargoController@AddCertificateOfMeasurement')->name('exportCargo.AddCertificateOfMeasurement');
    
    Route::post('AjaxCallForExportCargoToGetAgent','ExportCargoJsonController@AjaxCallForExportCargoToGetAgent')->name('AjaxCallForExportCargoToGetAgent'); // Add by Bilal 17Feb2021
    Route::post('AjaxCallForExportCargoToGetVessel','ExportCargoJsonController@AjaxCallForExportCargoToGetVessel')->name('AjaxCallForExportCargoToGetVessel'); // Add by Bilal 17Feb2021
    Route::post('AjaxCallForExportCargoToGetConsignee','ExportCargoJsonController@AjaxCallForExportCargoToGetConsignee')->name('AjaxCallForExportCargoToGetConsignee'); // Add by Bilal 17Feb2021
    Route::post('AjaxCallForExportCargoToGetContainer','ExportCargoJsonController@AjaxCallForExportCargoToGetContainer')->name('AjaxCallForExportCargoToGetContainer'); // Add by Bilal 19Feb2021
    
    Route::get('exportcargo/invoice/{id}','ExportCargoController@generateExportPdf')->name('exportCargo.invoice');
    Route::get('exportcargoDetail/invoice/{id}','ExportCargoController@generateExportDetailPdf')->name('exportcargoDetail.invoice');

    
    
    Route::post('exportdetial-delete','ExportCargoJsonController@AjaxCallForExportDetailSoftDelete')->name('exportdetial-delete');
    
    Route::get('container-exist/{containerName?}','ImportCargoJsonController@containerExist')->name('container-exist');
    Route::get('vessel-fetch/{search?}','ImportCargoJsonController@vesselFetch')->name('vessel-fetch');
    Route::get('container-fetch/{search?}','ImportCargoJsonController@containerFetch')->name('container-fetch');
    Route::post('importdetal-delete','ImportCargoJsonController@deleteImportCargoDetail')->name('importdetal-delete');
    Route::resource('itemtypes', 'ItemTypeController');
    Route::resource('users', 'UserController', [
        'only' => ['index', 'create', 'store','edit','update','destroy']
    ]);

    Route::post('changePassword', 'changePasswordController@ChangePassword')->name('changePassword');

    Route::get('report','HomeController@ViewReport')->name('report.view');
    Route::post('generateExportReportUsingVesselAndContainerAndVoyage','ExportCargoController@generateExportReportUsingVesselAndContainerAndVoyage')->name('report.export');

    Route::post('generateImportPackageReportUsingVesselAndContainerAndVoyage','ImportCargoController@generateImportPackageReportUsingVesselAndContainerAndVoyage')->name('report.import_package');

    Route::post('generateImportVehicleReportUsingVesselAndContainerAndVoyage','ImportCargoController@generateImportVehicleReportUsingVesselAndContainerAndVoyage')->name('report.import_vehicle');

    Route::post('report/ref','HomeController@storeReport')->name('report.ref');


    Route::get('ViewReport','HomeController@viewDetailsReport')->name('report.ViewReport');

    Route::post('ViewReport/delete','ReportController@ref_report_delete')->name('report.delete');


});

Route::get("sendEmail", "EmailController@sendEmailToUser");

