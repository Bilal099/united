<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => 'throttle:100,1'], function () {

    Route::middleware('auth:api')->group(function () {
        Route::post('logout',"API\AuthController@logout");
        //Auto Complete Vessels
        Route::get('fetch-vessels/{search?}','API\CommonController@getVessels');
        //Auto Complete Containers
        Route::get('fetch-containers/{search?}','API\CommonController@getContainer');
        //Fetch Item Type
        Route::get('fetch-containertypes','API\CommonController@getItemType');
        
        Route::apiResource('importcargos', 'API\ImportController');

        Route::apiResource('exportcargos', 'API\ExportController');

        // Route::get('fetch-CertificateOfMeasurement/{id}','API\ExportController@get_showCertificateOfMeasurement');
        Route::post('insert-CertificateOfMeasurement','API\ExportController@AddCertificateOfMeasurement');
        Route::post('insert-ExportRemarks','API\ExportController@updateRemarks');

        //Auto Complete Agent
        Route::get('fetch-agents/{search?}','API\CommonController@getAgents');

        //Auto Complete Frieght Forwarder
        Route::get('fetch-frieghtForwarder/{search?}','API\CommonController@getFrieghtForWarder');

        // Fetch Ports
        Route::get('fetch-ports/{search?}','API\CommonController@getPorts');

    });
    Route::post('login',"API\AuthController@login");
});