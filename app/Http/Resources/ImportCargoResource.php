<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ImportCargoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user' => $this->user->name,
            'serialNo' => $this->serialNo,
            'vessel' => $this->vessel->name,
            'voy' => $this->voy,
            'departureDate' =>  date("Y/m/d", strtotime($this->departureDate)),
            'dom_des' => date("Y/m/d", strtotime($this->dom_des)),
            'container' => $this->container,
            'igm_number' => $this->igm_number,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'details' => $this->importcargosdetail
        ];
    }
}
