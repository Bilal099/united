<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ExportCargoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id'                        => $this->id,
            'user'                      => $this->user->name,
            'serialNo'                  => $this->serialNo,
            'vessel'                    => $this->vessel->name,
            'voy'                       => $this->voy,
            'departureDate'             => date("Y-m-d", strtotime($this->departureDate)),
            'status'                    => $this->status,
            'created_at'                => $this->created_at,
            'updated_at'                => $this->updated_at,
            'description'               => $this->description,
            'port'                      => $this->port->name,
            'agent'                     => $this->agent->name, 
            'frieghtForwarder'          => $this->frieghtforwarder->name, 
            'container'                 => $this->container,
            'details'                   => $this->exportcargosdetail,
            'exportcargosCertificate'   => $this->exportcargosCertificate,
            'mean_check'                => $this->mean_check,
        ];
    }
}
