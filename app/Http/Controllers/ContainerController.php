<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Container;
use App\Models\ItemType;

class ContainerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd()
        $container_detail = Container::all();
        // dd($container_detail);
        return view('container.index',compact('container_detail'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ItemType = ItemType::all();
        return view('container.create',compact('ItemType'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);

        $request->validate([
            'name'      => 'required',
            'size'      => 'required',
            'ItemType'  => 'required',
        ],[
            'name.required'      => 'This field is required',
            'size.required'      => 'This field is required',
            'ItemType.required'  => 'This field is required',
        ]);

        $container          = new Container;
        $container->name    = strtolower($request->name);
        $container->size    = $request->size;
        $container->itemtype_Id    = $request->ItemType;
        // $container->number  = $request->number;
        $container->save();

        // return view('container.index')->with('success','Container added successfully!');
        return redirect()->route('container.index')->with('success','Container added successfully!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Container = Container::find($id);
        $ItemType = ItemType::all();
        return view('container.edit',compact('Container','ItemType'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'      => 'required',
            'size'      => 'required',
            'ItemType'  => 'required',
        ],[
            'name.required'      => 'This field is required',
            'size.required'      => 'This field is required',
            'ItemType.required'  => 'This field is required',
        ]);

        $container          = Container::find($id);
        $container->name    = strtolower($request->name);
        $container->size    = $request->size;
        $container->itemtype_Id    = $request->ItemType;
        // $container->number  = $request->number;
        $container->save();

        // return view('container.index')->with('success','Container added successfully!');
        return redirect()->route('container.index')->with('success','Container updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Container::findOrFail($id)->delete();
        return redirect()->route('container.index')->with('success','Container deleted successfully!');
    }
}
