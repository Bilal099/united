<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::join('usertype','usertype.id','=','users.usertypeId')
        ->select('users.id','users.name','users.email','usertype.name as usertype','usertype.id as usertypeId','users.created_at')
        ->where('userTypeId','<>','1')->get();
        // dd($users);
        return view('users.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $usertypes = DB::table('usertype')->where('id','<>',1)->pluck('name','id');
        return view('users.create',compact('usertypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email:rfc|unique:users,email',
            'password' =>'required|min:8|confirmed',
            'user_type' => 'required'
        ]);
        if($request->user_type == 1)
        {
            return abort(401);
        }
        $user = User::create([
            'userTypeId' => $request->user_type,
            'name' =>$request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);
        return redirect()->route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $usertypes = DB::table('usertype')->where('id','<>',1)->pluck('name','id');
        return view('users.edit',compact('user','usertypes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email:rfc|unique:users,email,'.$id,
            'user_type' => 'required'
        ]);
        if($request->user_type != 2 || $request->user_type != 3)
        {
            return abort(401);
        }
        $user = User::findOrFail($id)->update([
            'userTypeId' => $request->user_type,
            'name' =>$request->name,
            'email' => $request->email
        ]);
        return redirect()->route('users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::findOrFail($id)->delete();
        return redirect()->route('users.index');
    }
}
