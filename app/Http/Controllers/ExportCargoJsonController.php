<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Agent;
use App\Models\Vessel;
use App\Models\FrieghtForWarder;
use App\Models\ExportCargoDetail;
use App\Models\Container;
use Auth;


class ExportCargoJsonController extends Controller
{
    public function AjaxCallForExportCargoToGetAgent(Request $request)
    {
        $Agent = Agent::where('name', 'like', '%'.$request->value.'%')->get();

        return $Agent->all();
    }

    public function AjaxCallForExportCargoToGetVessel(Request $request)
    {
        $Vessel = Vessel::where('name', 'like', '%'.$request->value.'%')->get();

        return $Vessel->all();
    }

    public function AjaxCallForExportCargoToGetConsignee(Request $request) // Frieght Forwarder
    {
        $FrieghtForWarder = FrieghtForWarder::where('name', 'like', '%'.$request->value.'%')->get();

        return $FrieghtForWarder->all();
    }

    public function AjaxCallForExportCargoToGetContainer(Request $request) 
    {
        $Container = Container::where('name', 'like', '%'.$request->value.'%')->get();

        return $Container->all();
    }

    public function AjaxCallForExportDetailSoftDelete(Request $request)
    {
        $ExportCargoDetail = ExportCargoDetail::find($request->id);
        $ExportCargoDetail->deleted_by = Auth::id();
        $ExportCargoDetail->deleted_at = NOW();
        $ExportCargoDetail->save();
        return response()->json(["status"=>true,"message" => 'Export Detail Deleted']);
    }
}
