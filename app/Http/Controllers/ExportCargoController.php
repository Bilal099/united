<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Port;
use App\Models\Vessel;
use App\Models\Agent;
use App\Models\ExportCargo;
use App\Models\FrieghtForWarder;
use App\Models\ExportCargoDetail;
use App\Models\ExportCertificate;
use App\Models\Container;
use App\Models\ItemType;
use App\Models\Report;


use Auth;
use DB;
use Exception;
use Illuminate\Support\Facades\Auth as FacadesAuth;
use PDF;


class ExportCargoController extends Controller
{
    public function index()
    {
        $export_details = ExportCargo::all()->sortByDesc('id');
        return view('exportcargoes.index',compact('export_details'));
    }

   
    public function create()
    {
        $port = Port::all();
        $option = "";
        $itemtypes = ItemType::all();

        return view('exportcargoes.create',compact('port','itemtypes'));
    }

    public function store(Request $request)
    {
        // $json = json_encode($request->all());
        // dd($request->all());
        if($request != null)
        {
            $request->validate([
                'local_agent_ms'            => 'required',
                'undesigned_surveyors'      => 'required',
                'certificate_date'          => 'required|date',
                'attended_at'               => 'required',
                'karachi_side'              => 'required',

                'vessel_name'               => 'required',
                'voy'                       => 'required',
                'of'                        => 'required|date',
                'shipper_clearing_agent'    => 'required',
                'export_cargo_container'    => 'required',
                'consignee'                 => 'required',
                'description_of_cargo'      => 'required',
                'port'                      => 'required',
                'container_type'            => 'required',
                'size'                      => 'required',

                'detail_date.*'             => 'required|date',
                'detail_doc.*'              => 'required',
                'detail_marks_no.*'         => 'required',
                'detail_packagesNos.*'      => 'required',
                // 'detail_container_no.*'     => 'required',
                // 'detail_size.*'             => 'required',
                'detail_seal_no.*'          => 'required',
                'detail_no_of_packages.*'   => 'required',

                ],[

                'detail_date.*.required'            => 'The Date field is required',
                'detail_doc.*.required'             => 'The Description field is required',
                'detail_marks_no.*.required'        => 'The Marks field is required',
                'detail_packagesNos.*.required'     => 'The packages Nos field is required',
                // 'detail_container_no.*.required'    => 'The Container field is required',
                'detail_size.*.required'            => 'The Size field is required',
                'detail_seal_no.*.required'         => 'The Seal field is required',
                'detail_no_of_packages.*.required'  => 'The Package field is required',
            ]);
            
            $serialNo = time().'-'.mt_rand(10,100);
                
            DB::transaction(function() use ($request,$serialNo){

                // Vessel working
                $vessel = Vessel::firstOrCreate(['name'=>strtolower($request->vessel_name)]);
                $vessel_id       = $vessel->id;
                
                // Agent working
                $agent_phone = explode(' / ',$request->shipper_clearing_agent);
                if(isset($agent_phone[1]))
                {
                    $agent = Agent::where('phone',$agent_phone[1])->first();
                }
                else
                {
                    $agent = Agent::where('name',$agent_phone[0])->first();
                }

                if($agent != null)
                {
                    $agent_id = $agent['id'];
                }
                else{
                    $agent              = new Agent;
                    $agent->name        = strtolower($agent_phone[0]);
                    $agent->save();
                    $agent_id           = $agent->id;
                }

                // consignee working
                $FrieghtForWarder = FrieghtForWarder::firstOrCreate(['name'=>strtolower($request->consignee)]);
                $FrieghtForWarder_id       = $FrieghtForWarder->id;
                
                $date                                       = date('d-m-Y');
                $user_id                                    = Auth::user()->id; 
                
                
                // $container_data = Container::firstOrCreate(['name' => strtolower($request->export_cargo_container),'itemtype_Id' => 1,'size'=> '5']);
                $container_data = Container::firstOrCreate(['name' => strtolower($request->export_cargo_container),'itemtype_Id' => $request->container_type,'size'=> $request->size]);

                
                $container_id = $container_data->id; 
                
                $ExportCargo                                = new ExportCargo;
                $ExportCargo->userId                        = $user_id; 
                $ExportCargo->serialNo                      = $serialNo;
                $ExportCargo->vesselId                      = $vessel_id; 
                $ExportCargo->agentId                       = $agent_id; 
                $ExportCargo->frieghtForwarderId            = $FrieghtForWarder_id; 
                $ExportCargo->containerNo                   = $container_id;

                $ExportCargo->portId                        = $request->port; 
                $ExportCargo->voy                           = $request->voy;
                $ExportCargo->departureDate                 = $request->of;
                $ExportCargo->description                   = $request->description_of_cargo;
                $ExportCargo->status                        = 'Arrival';
                if(isset($request->mean_check))
                {
                    $ExportCargo->mean_check                = 'yes';
                }
                else{
                    $ExportCargo->mean_check                = 'no';
                }
                $ExportCargo->save();

                $ExportCargo_id                             = $ExportCargo->id;

                foreach($request->detail_marks_no as $key => $index)
                {
                    

                    $exportCargoDetail                      = new ExportCargoDetail;
                    $exportCargoDetail->userId              = $user_id;
                    $exportCargoDetail->exportCargoId       = $ExportCargo_id; 
                    $exportCargoDetail->description         = $request->detail_doc[$key];
                    $exportCargoDetail->marksAndNo          = $request->detail_marks_no[$key];
                    // $exportCargoDetail->size                = $request->detail_size[$key];
                    $exportCargoDetail->sealNo              = $request->detail_seal_no[$key];
                    $exportCargoDetail->packagesNos        = $request->detail_packagesNos[$key];
                    $exportCargoDetail->noOfPackages        = $request->detail_no_of_packages[$key];
                    $exportCargoDetail->cargo_date          = $request->detail_date[$key];
                    $exportCargoDetail->save();
                    
                }
                
                $ExportCertificate                          = new ExportCertificate;
                $ExportCertificate->exportCargoId           = $ExportCargo_id; 
                $ExportCertificate->agentName               = $request->local_agent_ms;
                $ExportCertificate->undersignedSurveyors    = $request->undesigned_surveyors;
                $ExportCertificate->attendedAt              = $request->attended_at;
                $ExportCertificate->side                    = $request->karachi_side;
                $ExportCertificate->date                    = $request->certificate_date;
                $ExportCertificate->save();
            },3);
                
            return redirect()->route('export-cargo.index')->with('success','Export Added successfully!');
        
        }
    }

    public function show($id)
    {
        $export_details = ExportCargo::findOrFail($id);
        return view('exportcargoes.show',compact('export_details'));
    }

    
    public function edit($id)
    {
        
        $export_details = ExportCargo::findOrFail($id);
        // dd($export_details->containerNo);
        $port = Port::all();
        $itemtypes = ItemType::all();

        // $container_data = Container::find($export_details->containerNo);
        // dd($container_data);
        return view('exportcargoes.edit',compact('export_details','port','itemtypes'));
    }

    public function update(Request $request, $id)
    {
        // dd($request);
        $request->validate([
            'local_agent_ms'            => 'required',
            'undesigned_surveyors'      => 'required',
            'certificate_date'          => 'required|date',
            'attended_at'               => 'required',
            'karachi_side'              => 'required',

            'vessel_name'               => 'required',
            'voy'                       => 'required',
            'of'                        => 'required|date',
            'shipper_clearing_agent'    => 'required',
            'export_cargo_container'    => 'required',
            'consignee'                 => 'required',
            'description_of_cargo'      => 'required',
            'port'                      => 'required',
            'container_type'            => 'required',
            'size'                      => 'required',

            'detail_date.*'             => 'required|date',
            'detail_doc.*'              => 'required',
            'detail_marks_no.*'         => 'required',
            // 'detail_container_no.*'     => 'required',
            'detail_packagesNos.*'      => 'required',
            // 'detail_size.*'             => 'required',
            'detail_seal_no.*'          => 'required',
            'detail_no_of_packages.*'   => 'required',

            ],[

            'detail_date.*.required'            => 'The Date field is required',
            'detail_doc.*.required'             => 'The Description field is required',
            'detail_marks_no.*.required'        => 'The Marks field is required',
            // 'detail_container_no.*.required'    => 'The Container field is required',
            'detail_packagesNos.*.required'     => 'The packages Nos field is required',
            'detail_size.*.required'            => 'The Size field is required',
            'detail_seal_no.*.required'         => 'The Seal field is required',
            'detail_no_of_packages.*.required'  => 'The Package field is required',
        ]);

        // Vessel working
        $vessel = Vessel::firstOrCreate(['name'=>strtolower($request->vessel_name)]);
        $vessel_id       = $vessel->id;

        // Agent working
        $agent_phone = explode(' / ',$request->shipper_clearing_agent);
        if(isset($agent_phone[1]))
        {
            $agent = Agent::where('phone',$agent_phone[1])->first();
        }
        else
        {
            $agent = Agent::where('name',$agent_phone[0])->first();
        }

        if($agent != null)
        {
            $agent_id = $agent['id'];
        }
        else{
            $agent              = new Agent;
            $agent->name        = strtolower($agent_phone[0]);
            $agent->save();
            $agent_id           = $agent->id;
        }

        // consignee working
        $FrieghtForWarder = FrieghtForWarder::firstOrCreate(['name'=>strtolower($request->consignee)]);
        $FrieghtForWarder_id       = $FrieghtForWarder->id;

        
        DB::transaction(function() use ($request,$vessel_id,$agent_id,$FrieghtForWarder_id,$id){
                
            $user_id                                    = Auth::user()->id; 
           
            $container_data = Container::firstOrCreate(['name' => strtolower($request->export_cargo_container),'itemtype_Id' => $request->container_type,'size'=> $request->size]);
            $container_id = $container_data->id; 
            


            $ExportCargo                                = ExportCargo::find($id);
            $ExportCargo->userId                        = $user_id; 
            $ExportCargo->vesselId                      = $vessel_id; 
            $ExportCargo->agentId                       = $agent_id; 
            $ExportCargo->frieghtForwarderId            = $FrieghtForWarder_id; 
            $ExportCargo->containerNo                   = $container_id;
            $ExportCargo->portId                        = $request->port; 
            $ExportCargo->voy                           = $request->voy;
            $ExportCargo->departureDate                 = $request->of;
            $ExportCargo->description                   = $request->description_of_cargo;
            if(isset($request->mean_check))
            {
                $ExportCargo->mean_check                = 'yes';
            }
            else{
                $ExportCargo->mean_check                = 'no';
            }
            $ExportCargo->save();

            foreach($request->detail_marks_no as $key => $index)
            {

                $exportCargoDetail                      = isset($request->export_detail_id[$key])? ExportCargoDetail::find($request->export_detail_id[$key]):new ExportCargoDetail;

                if(isset($request->export_detail_id[$key]))
                {
                    if($exportCargoDetail->noOfPackages != $request->detail_no_of_packages[$key])
                    {
                        if($exportCargoDetail->average != 0 && $exportCargoDetail->length != 0 && $exportCargoDetail->breadth != 0 && $exportCargoDetail->depth != 0 )
                        {
                            $avg                            = ($exportCargoDetail->length * $exportCargoDetail->breadth * $exportCargoDetail->depth * $request->detail_no_of_packages[$key]) * 0.000001;
                            $exportCargoDetail->average     = $avg;
                        } 
                    }
                }
                $exportCargoDetail->userId              = $user_id;
                $exportCargoDetail->exportCargoId       = $id; 
                $exportCargoDetail->description         = $request->detail_doc[$key];
                $exportCargoDetail->marksAndNo          = $request->detail_marks_no[$key];
                // $exportCargoDetail->size                = $request->detail_size[$key];
                $exportCargoDetail->sealNo              = $request->detail_seal_no[$key];
                $exportCargoDetail->packagesNos         = $request->detail_packagesNos[$key];
                $exportCargoDetail->noOfPackages        = $request->detail_no_of_packages[$key];
                $exportCargoDetail->cargo_date          = $request->detail_date[$key];
                $exportCargoDetail->save();
            }

            $ExportCertificate                          = ExportCertificate::find($request->exportCertificate_id);
            $ExportCertificate->exportCargoId           = $id; 
            $ExportCertificate->agentName               = $request->local_agent_ms;
            $ExportCertificate->undersignedSurveyors    = $request->undesigned_surveyors;
            $ExportCertificate->attendedAt              = $request->attended_at;
            $ExportCertificate->side                    = $request->karachi_side;
            $ExportCertificate->date                    = $request->certificate_date;
            $ExportCertificate->save();
        },3);

        return redirect()->route('export-cargo.index')->with('success','Export Added successfully!');
        

    }

    public function destroy($id)
    {
        // dd('Delete Pending');
        ExportCargo::findOrFail($id)->delete();
        return redirect()->route('export-cargo.index')->with('success','Export Cargo Details deleted successfully!');
    }

    public function updateRemarks(Request $request)
    {
        DB::transaction(function() use ($request){

            foreach($request->exportdetailId as $key => $item)
            {
                ExportCargoDetail::where('id', $item)->update(['remarks' => $request->remarks[$key],'remarkUserId'=> Auth::id()]);
            }
            ExportCargo::where('id',$request->export_id)->update(['status'=> 'Stuffing', 'stuffing_date' =>date(now())]);

        },3);

        // foreach($request->exportdetailId as $key => $item)
        // {
        //     ExportCargoDetail::where('id', $item)->update(['remarks' => $request->remarks[$key],'remarkUserId'=> Auth::id()]);
        // }
        return redirect()->back()->withMessage('Remarks Updated Successfully');
    }

    // public function showCertificateOfMeasurement(Request $request)
    public function showCertificateOfMeasurement($id)
    {
        // $export_id = $request->id;
        $export_id = $id;
        $export_details = ExportCargo::findOrFail($export_id);
        return view('exportcargoes.showCertificateMeasurement',compact('export_details','export_id'));
    }

    public function AddCertificateOfMeasurement(Request $request)
    {
        // dd($request);
        foreach($request->exportdetailId as $key => $item)
        {
            ExportCargoDetail::where('id', $item)->update([
                'length' => $request->length[$key],
                'breadth' => $request->breadth[$key],
                'depth' => $request->depth[$key],
                'average' => $request->average[$key],
            ]);

        }
        // return $this->get_showCertificateOfMeasurement($request->export_id);
        // return redirect()->route('exportCargo.showCertificateOfMeasurement',$request->export_id)->withMessage('Measurements Updated Successfully');
        // return view('exportcargoes.showCertificateMeasurement',compact('export_details'));
        return redirect()->route('export-cargo.show',$request->export_id)->withMessage('Measurement Updated Successfully');

        // return redirect()->back()->withMessage('Measurement Updated Successfully');

    }

    public function get_showCertificateOfMeasurement($id)
    {
        $export_id = $id;
        $export_details = ExportCargo::findOrFail($export_id);
        return view('exportcargoes.showCertificateMeasurement',compact('export_details','export_id'))->with('message','Measurements Updated Successfully');
    }

    // exportCargoPDF

    public function generateExportPdf($id)
    {
        $ExportCargo = ExportCargo::find($id);
        // dd($ExportCargo);

        $data = [
            'ExportCargo'    => $ExportCargo,
       ];
        $pdf = PDF::loadView('exportcargoes.exportCargoPDF',$data);
        return $pdf->setPaper('a4', 'LANDSCAPE')->stream('Export-Cargo.pdf');
    }

    public function generateExportDetailPdf($id)
    {
        $ExportCargo = ExportCargo::find($id);
        // dd($ExportCargo);

        $data = [
            'ExportCargo'    => $ExportCargo,
        ];
        $pdf = PDF::loadView('exportcargoes.certificateMeasurementPDF',$data);
        return $pdf->setPaper('a4', 'PORTAIRT')->stream('Certificate-of-measurement.pdf');
    }

    public function generateExportReportUsingVesselAndContainerAndVoyage(Request $request)
    {
        $vessel_id;
        $container_id;
        $voy;

        // try {

            if($request != null)
            {
                if(isset($request->view_detail_report))
                {
                    $vessel_id      = $request->vessel_id;
                    $container_id   = $request->container_id;
                    $voy            = $request->voy;
                }
                else{
                    $vessel_id      = $request->export_vessel;
                    $container_id   = $request->export_container;
                    $voy            = $request->export_voy;
                }

                $save_report = Report::where([
                    ['vessel_id',"=",$vessel_id],
                    ['container_id',"=",$container_id],
                    ['voy',"=",$voy],
                    ['report_type',"=","export_form"]
                ])->first();

                if($save_report != null)
                {
                    $ref_number = $save_report->ref_number;
                    $report_created_date = $save_report->created_at;
                }


                $report = ExportCargo::where([
                    ['vesselId','=',$vessel_id],
                    ['containerNo','=',$container_id],
                    ['voy','=',$voy],
                    ['status','=','Stuffing']
                ])->get();

                $query = 'SELECT agents.name AS agent,
                            frieghtforwarders.name AS frieghtforwarder,
                            ec.mean_check,
                            ecd1.* 
                            FROM exportcargoes ec 
                            JOIN 
                                (SELECT count(ecd.marksAndNo) AS num_of_rows,
                                SUM(ecd.average) AS avg_sum, 
                                SUM(ecd.noOfPackages) AS noOfPackages_sum ,
                                ecd.* 
                                FROM exportcargodetails ecd 
                                GROUP BY 
                                ecd.userId, 
                                ecd.marksAndNo, 
                                ecd.exportCargoId 
                                ORDER BY ecd.id ) ecd1 
                            ON ec.id = ecd1.exportCargoId
                            JOIN agents ON ec.agentId = agents.id
                            JOIN frieghtforwarders ON ec.frieghtForwarderId = frieghtforwarders.id
                            WHERE ec.vesselId ='.$vessel_id.' AND ec.containerNo = '.$container_id.' AND ec.voy = "'.$voy.'" AND status = "Stuffing"
                            ORDER BY ecd1.id';

                $report_details = DB::select($query);

                if(count($report) != 0)
                {
                    $container          = $report[0]->container->name;
                    $container_size     = $report[0]->container->size;

                    $container_type     = $report[0]->container->ContainerType->name;
                    $port               = $report[0]->port->name;
                    $vessel_name        = $report[0]->vessel->name;
                    $voy                = $report[0]->voy;
                    $date_of_insp       = $report[0]->stuffing_date;
                    $data = [
                        'report'                => $report,
                        'container'             => $container,
                        'container_type'        => $container_type,
                        'container_size'        => $container_size,
                        'port'                  => $port,
                        'vessel_name'           => $vessel_name,
                        'voy'                   => $voy,
                        'date_of_insp'          => $date_of_insp,
                        'ref_number'            => isset($ref_number)? $ref_number:"",
                        'report_details'        => $report_details,
                        'report_created_date'   => isset($report_created_date)? $report_created_date:"",
                    ];
                    
                    // return view('reports.exportReport',$data);
                    $pdf = PDF::loadView('reports.exportReport',$data);
                    return $pdf->setPaper('a4', 'PORTAIRT')->stream('ExportReport-'.now().'.pdf');
                }
                else{
                    return redirect()->route('report.view')->with('error','The selected data record is not available!');
                }
            }
            else
            {
                return redirect()->route('report.view')->with('error','Something fields are empty!');
            }
        // } 
        // catch (\Throwable $th) {
        //     return redirect()->route('report.view')->with('error','There is some issue!');
        // }
    }
}
