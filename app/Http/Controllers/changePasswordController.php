<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Validator;
use Hash;
use App\User;

class changePasswordController extends Controller
{
    public function ChangePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'current_password' => 'required',
            'new_password' => 'required|same:confirm_new_password|min:8',
        ]);
        if ($validator->passes()) {
            if(Hash::check($request->current_password, Auth::user()->password))
            {
                Auth::user()->update([
                    'password' => Hash::make($request->new_password)
                ]);
                return response()->json(['status'=>true]);
            }
            else
            {
                $validator->getMessageBag()->add('password', 'Old Password doesn\'t matched');
                return response()->json(['status'=>false,'error'=>$validator->errors()->all()]);
            }
        }
        else
        {
            return response()->json(['status'=>false,'error'=>$validator->errors()->all()]);
        }
    }
}
