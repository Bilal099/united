<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Vessel;
use App\Models\Report;
use App\Models\Container;
use App\Models\ImportCargo;
use App\Models\ExportCargo;

// use App\Http

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function ViewReport()
    {
        $Vessel = Vessel::all();
        $Container = Container::all();
        // dd($Container);
        return view('reports.report',compact('Vessel','Container'));

    }

    public function storeReport(Request $request)
    {
        try 
        {
            $detail = array();
            if($request->all() != null)
            {   
                
                if($request->form_type_modal == "export_form")
                {
                    $report = ExportCargo::where([
                        ['vesselId','=',$request->vessel_modal],
                        ['containerNo','=',$request->container_modal],
                        ['voy','=',$request->voy_modal],
                        ['status','=','Stuffing']
                    ])->get();
                }
                elseif ($request->form_type_modal == "import_package_form") 
                {
                    $report = ImportCargo::where([
                        ['vesselId','=',$request->vessel_modal],
                        ['containerId','=',$request->container_modal],
                        ['voy','=',$request->voy_modal]
                    ])->get();
                }
                elseif ($request->form_type_modal == "import_vehicle_form") 
                {
                    $report = ImportCargo::where([
                        ['vesselId','=',$request->vessel_modal],
                        ['containerId','=',$request->container_modal],
                        ['voy','=',$request->voy_modal]
                    ])->first();
                }

                // dd($report);

                if($report != null)
                {
                    $status = Report::where([
                        ['vessel_id',"=",$request->vessel_modal],
                        ['container_id',"=",$request->container_modal],
                        ['voy',"=",$request->voy_modal],
                        ['report_type',"=",$request->form_type_modal]
                    ])->first();
    
                    if($status == null)
                    {
                        $report = new Report;
                        $report->vessel_id          = $request->vessel_modal;
                        $report->container_id       = $request->container_modal;
                        $report->voy                = $request->voy_modal;
                        $report->ref_number         = $request->ref_number;
                        $report->report_type        = $request->form_type_modal;
                        $report->save();
                    }
                    else{
                        $detail[0] = true;
                        $detail[1] = "ref_exist";
                        $detail[2] = $status->ref_number;
    
                        return $detail;
                    }
                }
                
            }
        } 
        catch (\Throwable $th) {
            $detail[0] = false;
            return $detail;
        }
        $detail[0] = true;
        return $detail;
    }

    public function viewDetailsReport()
    {
        $report = Report::all()->sortByDesc('id');
        // dd($report);
        return view('reports.ViewReportDetails',compact('report'));
    }
}
