<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Vessel;
use App\Models\Container;
use App\Models\ImportCargoDetail;
use Auth;
class ImportCargoJsonController extends Controller
{
    public function vesselFetch($search = null)
    {
        if($search == null)
            $vessel = Vessel::select('id','name')->get();
        else
            $vessel = Vessel::whereLike("name",$search)->select('id','name')->get();
        // dd($search);
        return response()->json(['status' => true, 'data' => $vessel]); 
    }

    public function containerFetch($search = null)
    {
        if($search == null)
            $container = Container::join('itemtypes','containers.itemtype_Id','=','itemtypes.id')->select('containers.id','containers.name','containers.size','containers.itemtype_Id','itemtypes.name as type')->get();
        else
            $container = Container::join('itemtypes','containers.itemtype_Id','=','itemtypes.id')->whereLike("containers.name",$search)->select('containers.id','containers.name','containers.size','containers.itemtype_Id','itemtypes.name as type')->get();
        return response()->json(['status' => true, 'data' => $container]); 
    }

    public function deleteImportCargoDetail(Request $request)
    {
        $importDetail = ImportCargoDetail::find($request->id);
        $importDetail->deleted_by = Auth::id();
        $importDetail->deleted_at = NOW();
        $importDetail->save();
        return response()->json(["status"=>true,"message" => 'Import Detail Deleted']);
    }

    public function containerExist($containerName)
    {
        $container  = Container::where('name',$containerName)->first();
        if($container != null)
        {
            return response()->json(['status' => true, 'data' => $container]); 
        }
        return response()->json(['status' => false]); 
    }
}
