<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\FrieghtForWarder;

class FrieghtForWarderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $FrieghtForWarder = FrieghtForWarder::all();
        return view('frieghtForWarder.index',compact('FrieghtForWarder'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('frieghtForWarder.create');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required',
        ]);

        $FrieghtForWarder = new FrieghtForWarder;
        $FrieghtForWarder->name = strtolower($request->name);
        $FrieghtForWarder->save();

        return redirect()->route('frieghtForWarder.index')->with('success','Frieght For Warder added successfully!');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $FrieghtForWarder = FrieghtForWarder::find($id);
        return view('frieghtForWarder.edit',compact('FrieghtForWarder'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
        ]);
        
        $FrieghtForWarder = FrieghtForWarder::find($id);
        $FrieghtForWarder->name = strtolower($request->name);
        $FrieghtForWarder->save();

        return redirect()->route('frieghtForWarder.index')->with('success','Frieght For Warder updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        FrieghtForWarder::findOrFail($id)->delete();
        return redirect()->route('frieghtForWarder.index')->with('success','Frieght For Warder deleted successfully!');
    }
}
