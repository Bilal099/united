<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Vessel;
use App\Models\Container;
use App\Models\ItemType;
use App\Models\Agent;
use App\Models\FrieghtForWarder;
use App\Models\Port;
class CommonController extends Controller
{
    public function getVessels($search = null)
    {
        $vessels = Vessel::selectRaw("vessels.id,UPPER(vessels.name) as name")->where('name','LIKE','%'.$search.'%')->get();
        return response()->json(['status' => true, 'data' => $vessels]);
    }

    public function getContainer($search = null)
    {
        $containers = Container::join('itemtypes','containers.itemtype_Id','=','itemtypes.id')->selectRaw("containers.id,UPPER(containers.name) as name,containers.size,itemtypes.name as type")->where('containers.name','LIKE','%'.$search.'%')->get();
        return response()->json(['status' => true, 'data' => $containers]);
    }

    public function getItemType($search = null)
    {
        $itemTypes = ItemType::selectRaw('id,UPPER(name)')->where('name','LIKE','%'.$search.'%')->get();
        return response()->json(['status' => true, 'data' => $itemTypes]);
    }

    public function getAgents($search = null)
    {
        $agent = Agent::selectRaw('id,UPPER(name)')->where('name','LIKE','%'.$search.'%')->get();
        return response()->json(['status' => true, 'data' => $agent]);
    }

    public function getFrieghtForWarder($search = null)
    {
        $FrieghtForWarder = FrieghtForWarder::selectRaw('id,UPPER(name)')->where('name','LIKE','%'.$search.'%')->get();
        return response()->json(['status' => true, 'data' => $FrieghtForWarder]);
    }

    public function getPorts()
    {
        $ports = Port::all();
        return response()->json(['status' => true, 'data' => $ports]);
    }

}
