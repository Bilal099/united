<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\ImportCargoResource;
use App\Models\ImportCargo;
use App\Models\ImportCargoDetail;
use DB;
use App\Models\Container;
use App\Models\Vessel;

class ImportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('status') && $request->status == 23)
        {
            // $imports = ImportCargo::with(['importcargosdetail','user'])->where('status',23)->orderBy('id', 'DESC')->get();
            $imports = ImportCargo::where('status',23)->orderBy('id', 'DESC')->get();

        }
        else
        {
            // $imports = ImportCargo::with(['importcargosdetail','user'])->where('status',45)->orderBy('id', 'DESC')->get();
            $imports = ImportCargo::where('status',45)->orderBy('id', 'DESC')->get();

        }
        return ImportCargoResource::collection($imports);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $mvId = 0;
            $containerId = 0;
            if ($request->mv != null) {
                $mvId = $this->containerOrVesselGetOrCreate($request->mv,'vessel');
            }
            if ($request->container_name != null) {
                $containerId = $this->containerOrVesselGetOrCreate($request->only(['container_name','size','container_type_id']),'container');
            }
            DB::transaction(function() use ($request,$mvId,$containerId){
                $importcargo = new ImportCargo;
                $importcargo->userId = $request->user()->id;
                $importcargo->serialNo = time().'-'.mt_rand(10,100);
                $importcargo->vesselId = $mvId;
                $importcargo->voy = $request->voy;
                $importcargo->containerId = $containerId;
                $importcargo->igm_number = $request->igm_number;
                $importcargo->departureDate = $request->of_date;
                $importcargo->dom_des = $request->date_of_measurement;
                $importcargo->status = 45;
                $importcargo->save();
                foreach($request->details as $key => $item)
                {
                    $importcargoDetail = new ImportCargoDetail;
                    $importcargoDetail->userId = $request->user()->id;
                    $importcargoDetail->importCargoId = $importcargo->id;
                    $importcargoDetail->cargo_index = $item["cargo_index"];
                    $importcargoDetail->stowageOnBoard = isset($item["stowage_on_board"])?$item["stowage_on_board"]:null;
                    $importcargoDetail->description = $item["description"];
                    $importcargoDetail->sealNo = isset($item["seal_no"])?$item["seal_no"]:null;
                    $importcargoDetail->mark_no = $item["mark_no"];
                    $importcargoDetail->measurment_m3 =$item["measurement_m3"];
                    $importcargoDetail->save();
                }
            },3);

            return response()->json(["status"=>true,"message"=>"Import Added Successfully"]);
        } catch (\Throwable $th) {
            return response()->json(["status"=>false,"message"=>$th->getMessage()]);
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $imports = ImportCargo::with(['importcargosdetail','user'])->find($id);
        if($imports == null)
        {
            return response()->json(["status" => false, "message"=>"No result Found"],404);
        }
        return new ImportCargoResource($imports);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $mvId = 0;
            $containerId = 0;
            if ($request->mv != null) {
                $mvId = $this->containerOrVesselGetOrCreate($request->mv,'vessel');
            }
            if ($request->container_name != null) {
                $containerId = $this->containerOrVesselGetOrCreate($request->only(['container_name','size','container_type_id']),'container');
            }
            DB::transaction(function() use ($request,$id,$mvId,$containerId){
                $importcargo = ImportCargo::findOrFail($id);
                $importcargo->userId = $request->user()->id;
                $importcargo->vesselId = $mvId;
                $importcargo->voy = $request->voy;
                $importcargo->containerId = $containerId;
                $importcargo->igm_number = $request->igm_number;
                $importcargo->departureDate = $request->of_date;
                $importcargo->dom_des = $request->date_of_measurement;
                foreach($request->details as $key => $item)
                {
                    if(isset($item['remarks']))
                    {
                        $importcargo->status = 23;
                    }
                }
                $importcargo->save();
                foreach($request->details as $key => $item)
                {
                    $importcargoDetail = ImportCargoDetail::where('id', @$item["id"])->firstOr(function () {
                        return new ImportCargoDetail;
                    });
                    $importcargoDetail->userId = $request->user()->id;
                    $importcargoDetail->importCargoId = $importcargo->id;
                    $importcargoDetail->cargo_index = $item['cargo_index'];
                    $importcargoDetail->stowageOnBoard = $item['stowage_on_board'];
                    $importcargoDetail->description = $item['description'];
                    $importcargoDetail->sealNo = $item['seal_no'];
                    $importcargoDetail->mark_no = $item['mark_no'];
                    if(isset($item['remarks']))
                    {
                        $importcargoDetail->remarks = $item['remarks'];
                        $importcargoDetail->accessories = isset($item['accessories'])? $item['accessories']:null ;
                    }
                    $importcargoDetail->remarkUserId = $request->user()->id;
                    $importcargoDetail->measurment_m3 =$item['measurement_m3'];
                    $importcargoDetail->save();
                }
            },3);
            return response()->json(["status"=>true,"message"=>"Import Updated Successfully"]);

        } catch (\Throwable $th) {
            return response()->json(["status"=>false,"message"=>$th->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    private function  containerOrVesselGetOrCreate($value,$type)
    {
        switch ($type) {
            case 'container':
                $cont = Container::where('name',$value['container_name'])->first();
                if($cont == null)
                {
                    $cont = Container::create(['name'=>strtolower($value['container_name']),'size'=>$value['size'],'itemtype_Id'=>$value['container_type_id']]);
                }
                return $cont->id;
            break;
            case 'vessel':
                $vessel = Vessel::firstOrCreate(['name'=>strtolower($value)]);
                return $vessel->id;
            break;
        }
    }
}
