<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Port;
use App\Models\Vessel;
use App\Models\Agent;
use App\Models\ExportCargo;
use App\Models\FrieghtForWarder;
use App\Models\ExportCargoDetail;
use App\Models\ExportCertificate;
use App\Models\Container;
use App\Models\ItemType;
use App\Http\Resources\ExportCargoResource;

use Auth;
use DB;
use Exception;
use PDF;

class ExportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $exports = ExportCargo::with(['exportcargosdetail','exportcargosCertificate','user','port'])->get();
        $exports = ExportCargo::orderByDesc('id')->get();
        return ExportCargoResource::collection($exports);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $serialNo = time().'-'.mt_rand(10,100);

            // Vessel working
            $vessel = Vessel::firstOrCreate(['name'=>strtolower($request->vessel_name)]);
            $vessel_id       = $vessel->id;
            
            // Agent working
            $agent_phone = explode(' / ',$request->shipper_clearing_agent);
            if(isset($agent_phone[1]))
            {
                $agent = Agent::where('phone',$agent_phone[1])->first();
            }
            else
            {
                $agent = Agent::where('name',$agent_phone[0])->first();
            }

            if($agent != null)
            {
                $agent_id = $agent['id'];
            }
            else{
                $agent              = new Agent;
                $agent->name        = strtolower($agent_phone[0]);
                $agent->save();
                $agent_id           = $agent->id;
            }

            // consignee working
            $FrieghtForWarder = FrieghtForWarder::firstOrCreate(['name'=>strtolower($request->consignee)]);
            $FrieghtForWarder_id       = $FrieghtForWarder->id;
            
            DB::transaction(function() use ($request,$vessel_id,$serialNo,$agent_id,$FrieghtForWarder_id){
                
                $date                                       = date('d-m-Y');
                $user_id                                    = Auth::user()->id; 
                // dd($request->container_type);
                $container_data = Container::firstOrCreate(['name' => strtolower($request->export_cargo_container),'itemtype_Id' => $request->container_type,'size'=> $request->size]);
                $container_id = $container_data->id; 
                
                $ExportCargo                                = new ExportCargo;
                $ExportCargo->userId                        = $user_id; 
                $ExportCargo->serialNo                      = $serialNo;
                $ExportCargo->vesselId                      = $vessel_id; 
                $ExportCargo->agentId                       = $agent_id; 
                $ExportCargo->frieghtForwarderId            = $FrieghtForWarder_id; 
                $ExportCargo->containerNo                   = $container_id;

                $ExportCargo->portId                        = $request->port; 
                $ExportCargo->voy                           = $request->voy;
                $ExportCargo->departureDate                 = $request->of;
                $ExportCargo->description                   = $request->description_of_cargo;
                $ExportCargo->status                        = 'Arrival';
                if($request->mean_check != "" && $request->mean_check != "no")
                {
                    $ExportCargo->mean_check                = 'yes';
                }
                else{
                    $ExportCargo->mean_check                = 'no';
                }
                $ExportCargo->save();

                $ExportCargo_id                             = $ExportCargo->id;

                foreach($request->details as $key => $index)
                {
                    $exportCargoDetail                      = new ExportCargoDetail;
                    $exportCargoDetail->userId              = $user_id;
                    $exportCargoDetail->exportCargoId       = $ExportCargo_id; 
                    $exportCargoDetail->description         = $index["detail_doc"];
                    $exportCargoDetail->marksAndNo          = $index["detail_marks_no"];
                    $exportCargoDetail->sealNo              = $index["detail_seal_no"];
                    $exportCargoDetail->packagesNos         = $index["detail_packagesNos"];
                    $exportCargoDetail->noOfPackages        = $index["detail_no_of_packages"];
                    $exportCargoDetail->cargo_date          = $index["detail_date"];
                    $exportCargoDetail->save();
                    
                }
                $ExportCertificate                          = new ExportCertificate;
                $ExportCertificate->exportCargoId           = $ExportCargo_id; 
                $ExportCertificate->agentName               = $request->local_agent_ms;
                $ExportCertificate->undersignedSurveyors    = $request->undesigned_surveyors;
                $ExportCertificate->attendedAt              = $request->attended_at;
                $ExportCertificate->side                    = $request->karachi_side;
                $ExportCertificate->date                    = $request->certificate_date;
                $ExportCertificate->save();
            },3);
            return response()->json(["status"=>true,"message"=>"Export Added Successfully"]);
        } catch (\Throwable $th) {
            return response()->json(["status"=>false,"message"=>$th->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $exports = ExportCargo::with(['exportcargosdetail','exportcargosCertificate','user','port'])->find($id);
        if($exports == null)
        {
            return response()->json(["status" => false, "message"=>"No result Found"],404);
        }
        return new ExportCargoResource($exports);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        try{
            $serialNo = time().'-'.mt_rand(10,100);

            // Vessel working
            $vessel = Vessel::firstOrCreate(['name'=>strtolower($request->vessel_name)]);
            $vessel_id       = $vessel->id;
            
            // Agent working
            $agent_phone = explode(' / ',$request->shipper_clearing_agent);
            if(isset($agent_phone[1]))
            {
                $agent = Agent::where('phone',$agent_phone[1])->first();
            }
            else
            {
                $agent = Agent::where('name',$agent_phone[0])->first();
            }

            if($agent != null)
            {
                $agent_id = $agent['id'];
            }
            else{
                $agent              = new Agent;
                $agent->name        = strtolower($agent_phone[0]);
                $agent->save();
                $agent_id           = $agent->id;
            }

            // consignee working
            $FrieghtForWarder = FrieghtForWarder::firstOrCreate(['name'=>strtolower($request->consignee)]);
            $FrieghtForWarder_id       = $FrieghtForWarder->id;
            DB::transaction(function() use ($request,$vessel_id,$serialNo,$agent_id,$FrieghtForWarder_id,$id){
                
                $user_id                                    = Auth::user()->id; 
            
                $container_data = Container::firstOrCreate(['name' => strtolower($request->export_cargo_container),'itemtype_Id' => $request->container_type,'size'=> $request->size]);
                $container_id   = $container_data->id; 
                
                $ExportCargo                                = ExportCargo::find($id);
                $ExportCargo->userId                        = $user_id; 
                $ExportCargo->serialNo                      = $serialNo;
                $ExportCargo->vesselId                      = $vessel_id; 
                $ExportCargo->agentId                       = $agent_id; 
                $ExportCargo->frieghtForwarderId            = $FrieghtForWarder_id; 
                $ExportCargo->containerNo                   = $container_id;

                $ExportCargo->portId                        = $request->port; 
                $ExportCargo->voy                           = $request->voy;
                $ExportCargo->departureDate                 = $request->of;
                $ExportCargo->description                   = $request->description_of_cargo;
                // $ExportCargo->status                        = 'Arrival';
                if($request->mean_check != "" && $request->mean_check != "no")
                {
                    $ExportCargo->mean_check                = 'yes';
                }
                else{
                    $ExportCargo->mean_check                = 'no';
                }
                $ExportCargo->save();

                $ExportCargo_id                             = $ExportCargo->id;

                foreach($request->details as $key => $index)
                {
                    $exportCargoDetail = isset($index["export_detail_id"])? ExportCargoDetail::find($index["export_detail_id"]):new ExportCargoDetail;

                    if(isset($index["export_detail_id"]))
                    {
                        if($exportCargoDetail->noOfPackages != $index["detail_no_of_packages"])
                        {
                            if($exportCargoDetail->average != 0 && $exportCargoDetail->length != 0 && $exportCargoDetail->breadth != 0 && $exportCargoDetail->depth != 0 )
                            {
                                $avg                            = ($exportCargoDetail->length * $exportCargoDetail->breadth * $exportCargoDetail->depth * $index["detail_no_of_packages"]) * 0.000001;
                                $exportCargoDetail->average     = $avg;
                            } 
                        }
                    }
                    $exportCargoDetail->userId              = $user_id;
                    $exportCargoDetail->exportCargoId       = $ExportCargo_id; 
                    $exportCargoDetail->description         = $index["detail_doc"];
                    $exportCargoDetail->marksAndNo          = $index["detail_marks_no"];
                    $exportCargoDetail->sealNo              = $index["detail_seal_no"];
                    $exportCargoDetail->packagesNos         = $index["detail_packagesNos"];
                    $exportCargoDetail->noOfPackages        = $index["detail_no_of_packages"];
                    $exportCargoDetail->cargo_date          = $index["detail_date"];
                    $exportCargoDetail->save();
                    
                }

                $ExportCertificate                          = ExportCertificate::find($request->exportCertificate_id);
                $ExportCertificate->exportCargoId           = $ExportCargo_id; 
                $ExportCertificate->agentName               = $request->local_agent_ms;
                $ExportCertificate->undersignedSurveyors    = $request->undesigned_surveyors;
                $ExportCertificate->attendedAt              = $request->attended_at;
                $ExportCertificate->side                    = $request->karachi_side;
                $ExportCertificate->date                    = $request->certificate_date;
                $ExportCertificate->save();
                // dd("bilal");

            },3);
            return response()->json(["status"=>true,"message"=>"Export Update Successfully"]);
        } catch (\Throwable $th) {
            return response()->json(["status"=>false,"message"=>$th->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function AddCertificateOfMeasurement(Request $request)
    {
        // dd($request);
        try {
            DB::transaction(function() use ($request){

                foreach($request->details as $key => $item)
                {
                    ExportCargoDetail::where('id', $item['id'])->update([
                        'length'    => $item["length"],
                        'breadth'   => $item["breadth"],
                        'depth'     => $item["depth"],
                        'average'   => $item["average"],
                    ]);

                }
            },3);
            return response()->json(["status"=>true,"message"=>"Export Measurement Update Successfully"]);
        } catch (\Throwable $th) {
            return response()->json(["status"=>false,"message"=>$th]);
        }
    }

    public function updateRemarks(Request $request)
    {
        try {
            DB::transaction(function() use ($request){

                foreach($request->details as $key => $item)
                {
                    ExportCargoDetail::where('id', $item['id'])->update(['remarks' => $item['remarks'],'remarkUserId'=> Auth::user()->id]);

                }
                // dd("bilal");

                // ExportCargo::where('id',$request->export_id)->update(['status'=> 'Stuffing']);
                ExportCargo::where('id',$request->export_id)->update(['status'=> 'Stuffing', 'stuffing_date' =>date(now())]);
                // dd("bilal");
    
            },3);
            return response()->json(["status"=>true,"message"=>"Export Remarks Update Successfully"]);

        } catch (\Throwable $th) {
            return response()->json(["status"=>false,"message"=>$th]);
        }
    }

    
}
