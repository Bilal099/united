<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Validator;
use Auth;
use DB;
use Artisan;
class AuthController extends Controller
{
    public function login(Request $request)
    {
        $validator = Validator::make($request->only(['email','password']), [
            'email' => 'required|email:rfc',
            'password' => 'required|min:8',
        ]);
        if ($validator->passes()) {
            
            if (Auth::attempt(['email' => $request->email, 'password' => $request->password,'userTypeId' =>3])) {
                
                $token =  Auth::user()->createToken('Api Token')->accessToken;
                $user = array("id"=>$request->user()->id,"name"=>$request->user()->name,"email"=>$request->user()->email,"user_type"=>$request->user()->userTypeId);
                return response()->json(['status'=>true,'access_token'=>$token,'user' => $user]);
            }
            else
            {
                return response()->json(['status'=>false,'error'=>['Invalid username or password.']]);
            }
        }
        else
        {
            return response()->json(['status'=> false,'error'=>$validator->errors()->all()]);
        }
    }
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([ 'status'=> true, 'message' => 'Successfully Logout!']);
    }
}
