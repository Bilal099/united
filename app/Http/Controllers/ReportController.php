<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Report;

class ReportController extends Controller
{
    public function ref_report_delete(Request $request)
    {
        $detail = array();
        try {
            $report = Report::find($request->id);
            $report->forceDelete();
        } 
        catch (\Throwable $th) {
            $detail[0] = false;
            $detail[1] = "The record is not Delete!";
            return $detail;
        }
        $detail[0] = true;
        $detail[1] = "The record is Deleted!";
        return $detail;
    }
}
