<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ItemType;
use App\Models\ImportCargo;
use App\Models\Container;
use App\Models\Vessel;
use App\Models\ImportCargoDetail;
use App\Models\Report;

use DB;
use Auth;
use PDF;
class ImportCargoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $importcargos = ImportCargo::all()->sortByDesc('id');
        return view('importcargoes.index',compact('importcargos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $itemtypes = ItemType::all();
        return view('importcargoes.create',compact('itemtypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'mv' => 'required',
            'container' => 'required',
            'voy' => 'required',
            'of' => 'required|date',
            'date_of_measurement' => 'required|date',
            'index.*' => 'required',
            'stowage.*' => '',
            'marks.*' => 'required',
            'description.*' => '',
            'measurment.*' => 'required',
            'size' => 'required|numeric',
            'container_type' => 'required',
            'sealno.*' => ''
            ],[
            'index.*.required' => 'The index field is required',
            'stowage.*.required' => 'The Stowage field is required',
            'marks.*.required' => 'The Marks field is required',
            'description.*.required' => 'The Description field is required',
            'sealno.*.required' => 'The Seal No field is required',
            'size.*.numeric' => 'The Size must be a number',
            'measurment.*.required' => 'The Measurment field is required',
        ]);
        // dd($request->only(['container','size','container_type']));
        $mvId = 0;
        $containerId = 0;
        if ($request->mv != null) {
            $mvId = $this->containerOrVesselGetOrCreate($request->mv,'vessel');
        }
        if ($request->container != null) {
            $containerId = $this->containerOrVesselGetOrCreate($request->only(['container','size','container_type']),'container');
        }
        DB::transaction(function() use ($request,$mvId,$containerId){
            $importcargo = new ImportCargo;
            $importcargo->userId = Auth::id();
            $importcargo->serialNo = time().'-'.mt_rand(10,100);
            $importcargo->vesselId = $mvId;
            $importcargo->voy = $request->voy;
            $importcargo->igm_number = $request->igm_number;
            $importcargo->containerId = $containerId;
            $importcargo->departureDate = $request->of;
            $importcargo->dom_des = $request->date_of_measurement;
            $importcargo->status = 45;
            $importcargo->save();
            foreach($request->index as $key => $index)
            {
                $importcargoDetail = new ImportCargoDetail;
                $importcargoDetail->userId = Auth::id();
                $importcargoDetail->importCargoId = $importcargo->id;
                $importcargoDetail->cargo_index = $index;
                $importcargoDetail->stowageOnBoard = $request->stowage[$key];
                $importcargoDetail->description = $request->description[$key];
                $importcargoDetail->sealNo = $request->sealno[$key];
                $importcargoDetail->mark_no = $request->marks[$key];
                $importcargoDetail->measurment_m3 = $request->measurment[$key];
                $importcargoDetail->save();
            }
        },3);
        return redirect()->route('import-cargo.index')->withMessage("Import Cargo Added successfully");
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $importcargo = ImportCargo::findOrFail($id);
        return view('importcargoes.show',compact('importcargo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $importcargo = ImportCargo::findOrFail($id);
        $itemtypes = ItemType::all();
        $vessel = Vessel::find($importcargo->vesselId);
        $mvId = $vessel->name;
        $container = Container::find($importcargo->containerId);
        $containerId =  $container->name;
        return view('importcargoes.edit',compact('importcargo','itemtypes','mvId','containerId'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $request->validate([
            'mv' => 'required',
            'container' => 'required',
            'voy' => 'required',
            'of' => 'required|date',
            'date_of_measurement' => 'required|date',
            'index.*' => 'required',
            'stowage.*' => '',
            'marks.*' => 'required',
            'description.*' => '',
            'measurment.*' => 'required',
            'size' => 'required|numeric',
            'container_type' => 'required',
            'sealno.*' => ''
            ],[
            'index.*.required' => 'The index field is required',
            'stowage.*.required' => 'The Stowage field is required',
            'marks.*.required' => 'The Marks field is required',
            'description.*.required' => 'The Description field is required',
            'measurment.*.required' => 'The Measurment field is required',
            'size.*.required' => 'The Size field is required',
            'type.*.required' => 'The Item Type field is required',
            'sealno.*.required' => 'The Seal No field is required',
            'size.*.numeric' => 'The Size must be a number',
            
        ]);
        $mvId = 0;
        $containerId = 0;
        if ($request->mv != null) {
            $mvId = $this->containerOrVesselGetOrCreate($request->mv,'vessel');
        }
        if ($request->container != null) {
            // $containerId = $this->containerOrVesselGetOrCreate($request->container,'container');
            $containerId = $this->containerOrVesselGetOrCreate($request->only(['container','size','container_type']),'container');
        }
        DB::transaction(function() use ($request,$mvId,$containerId,$id){
            $importcargo = ImportCargo::findOrFail($id);
            $importcargo->userId = Auth::id();
            // $importcargo->serialNo = time().'-'.mt_rand(10,100);
            $importcargo->vesselId = $mvId;
            $importcargo->voy = $request->voy;
            $importcargo->containerId = $containerId;
            $importcargo->igm_number = $request->igm_number;
            $importcargo->departureDate = $request->of;
            $importcargo->dom_des = $request->date_of_measurement;
            $importcargo->save();
            foreach($request->importdetailId as $key => $index)
            {
                $importcargoDetail = ImportCargoDetail::where('id', $index)->firstOr(function () {
                    return new ImportCargoDetail;
                });
                $importcargoDetail->userId = Auth::id();
                $importcargoDetail->importCargoId = $importcargo->id;
                $importcargoDetail->cargo_index = $request->index[$key];
                $importcargoDetail->stowageOnBoard = $request->stowage[$key];
                $importcargoDetail->description = $request->description[$key];
                // $importcargoDetail->containerId =$containerId;
                // $importcargoDetail->size = $request->size[$key];
                // $importcargoDetail->itemTypeId = $request->type[$key];
                $importcargoDetail->sealNo = $request->sealno[$key];
                $importcargoDetail->mark_no = $request->marks[$key];
                $importcargoDetail->measurment_m3 = $request->measurment[$key];
                $importcargoDetail->save();
            }
        },3);
        return redirect()->route('import-cargo.index')->withMessage("Import Cargo Updated successfully");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function updateRemarks(Request $request)
    {
        foreach($request->importdetailId as $key => $item)
        {
            ImportCargoDetail::where('id', $item)->update([ 
                'remarks' => $request->remarks[$key],
                'accessories'=> ($request->accessories[$key] != null || $request->accessories[$key] != "" )? $request->accessories[$key]:null ,
                'remarkUserId'=> Auth::id()
            ]);
        }
        ImportCargo::where('id',$request->importcargoId)->update(['status'=>23]);
        return redirect()->back()->withMessage('Remarks Updated Successfully');
    }
    

    private function  containerOrVesselGetOrCreate($value,$type)
    {
        switch ($type) {
            case 'container':
                $cont = Container::where('name',$value['container'])->first();
                if($cont == null)
                {
                    $cont = Container::create(['name'=>strtolower($value['container']),'size'=>$value['size'],'itemtype_Id'=>$value['container_type']]);
                }
                return $cont->id;
            break;
            case 'vessel':
                $vessel = Vessel::firstOrCreate(['name'=>strtolower($value)]);
                return $vessel->id;
            break;
        }
    }

    public function generateImportPdf($importId,$status)
    {
        $imports = ImportCargo::findOrFail($importId);
        if($status == 45)
        {
            $pdf = PDF::loadView('importcargoes.invoice-form45',['imports'=>$imports]);
            $filename = time().'-'.mt_rand(10,100).'.pdf';
            return $pdf->setPaper('a4', 'portrait')->stream($filename);
        }
        else if($status == 23)
        {
            $pdf = PDF::loadView('importcargoes.invoice-form23',['imports'=>$imports]);
            $filename = time().'-'.mt_rand(10,100).'.pdf';
            return $pdf->setPaper('a4', 'landscape')->stream($filename);
        }
    }

    // import package Report
    public function generateImportPackageReportUsingVesselAndContainerAndVoyage(Request $request)
    {
        $vessel_id;
        $container_id;
        $voy;

        try {
            if($request != null)
            {
                if(isset($request->view_detail_report))
                {
                    $vessel_id      = $request->vessel_id;
                    $container_id   = $request->container_id;
                    $voy            = $request->voy;
                }
                else{
                    $vessel_id      = $request->import_package_vessel;
                    $container_id   = $request->import_package_container;
                    $voy            = $request->import_package_voy;
                }

                $save_report = Report::where([
                    ['vessel_id',"=",$vessel_id],
                    ['container_id',"=",$container_id],
                    ['voy',"=",$voy],
                    ['report_type',"=","import_package_form"]
                ])->first();

                if($save_report != null)
                {
                    $ref_number = $save_report->ref_number;
                    $report_created_date = $save_report->created_at;
                }

                $report = ImportCargo::where([
                    ['vesselId','=',$vessel_id],
                    ['containerId','=',$container_id],
                    ['voy','=',$voy]
                ])->get();
                if(count($report) != 0)
                {
                    $container          = $report[0]->container->name;
                    $container_size     = $report[0]->container->size;
                    $container_type     = $report[0]->container->ContainerType->name;
                    $vessel_name        = $report[0]->vessel->name;
                    $voy                = $report[0]->voy;
                    $date_of_insp       = $report[0]->departureDate;
                    $data = [
                        'report'                => $report,
                        'container'             => $container,
                        'container_type'        => $container_type,
                        'container_size'        => $container_size,
                        'vessel_name'           => $vessel_name,
                        'voy'                   => $voy,
                        'date_of_insp'          => $date_of_insp,
                        'ref_number'            => isset($ref_number)? $ref_number:"",
                        'report_created_date'   => isset($report_created_date)? $report_created_date:"",

                    ];
            
                    $pdf = PDF::loadView('reports.importPackageReport',$data);
                    return $pdf->setPaper('a4', 'PORTAIRT')->stream('ImportPackageReport-'.now().'.pdf');
                }
                else{
                    return redirect()->route('report.view')->with('error','The selected data record is not available!');
                }
            }
            else
            {
                return redirect()->route('report.view')->with('error','Something fields are empty!');
            }
        } 
        catch (\Throwable $th) {
            return redirect()->route('report.view')->with('error','There is some issue!');
        }
    }

    // import Vehicle Report
    public function generateImportVehicleReportUsingVesselAndContainerAndVoyage(Request $request)
    {
        $vessel_id;
        $container_id;
        $voy;

        try {
            if($request != null)
            {
                if(isset($request->view_detail_report))
                {
                    $vessel_id      = $request->vessel_id;
                    $container_id   = $request->container_id;
                    $voy            = $request->voy;
                }
                else{
                    $vessel_id      = $request->import_vehicle_vessel;
                    $container_id   = $request->import_vehicle_container;
                    $voy            = $request->import_vehicle_voy;
                }

                $save_report = Report::where([
                    ['vessel_id',"=",$vessel_id],
                    ['container_id',"=",$container_id],
                    ['voy',"=",$voy],
                    ['report_type',"=","import_vehicle_form"]
                ])->first();

                if($save_report != null)
                {
                    $ref_number = $save_report->ref_number;
                    $report_created_date = $save_report->created_at;
                }

                $report = ImportCargo::where([
                    ['vesselId','=',$vessel_id],
                    ['containerId','=',$container_id],
                    ['voy','=',$voy]
                ])->first();
                if($report != null)
                {
                    $container          = $report->container->name;
                    $container_type     = $report->container->ContainerType->name;
                    $vessel_name        = $report->vessel->name;
                    $voy                = $report->voy;
                    $date_of_insp       = $report->departureDate;
                    $data = [
                        'report'                => $report,
                        'container'             => $container,
                        'container_type'        => $container_type,
                        'vessel_name'           => $vessel_name,
                        'voy'                   => $voy,
                        'date_of_insp'          => $date_of_insp,
                        'ref_number'            => isset($ref_number)? $ref_number:"",
                        'report_created_date'   => isset($report_created_date)? $report_created_date:"",

                    ];
                    // dd($report);
                    $pdf = PDF::loadView('reports.importVehicleReport',$data);
                    return $pdf->setPaper('a4', 'PORTAIRT')->stream('ImportVehicleReport-'.now().'.pdf');
                }
                else{
                    return redirect()->route('report.view')->with('error','The selected data record is not available!');
                }
            }
            else
            {
                return redirect()->route('report.view')->with('error','Something fields are empty!');
            }
        } 
        catch (\Throwable $th) {
            return redirect()->route('report.view')->with('error','There is some issue!');
        }
    }

}
