<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class ImportCargoDetail extends Model
{
    use SoftDeletes;
    protected $table = "importcargodetails";
    public function importcargo()
    {
        return $this->belongsTo('App\Models\ImportCargoDetail','importCargoId', 'id');
    }
}
