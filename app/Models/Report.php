<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $table = "reports";

    public $fillable = [ 
        'vessel_modal',
        'container_modal',
        'voy_modal',
        'form_type_modal',
    ];

    public function container()
    {
        return $this->belongsTo('App\Models\Container', 'container_id', 'id');
    }
    public function vessel()
    {
        return $this->belongsTo('App\Models\Vessel', 'vessel_id', 'id');
    }
}
