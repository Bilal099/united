<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Container extends Model
{
    use SoftDeletes;
    public $fillable = ['name','size','itemtype_Id'];
    public $timestamps = false;

    public function importcargoes()
    {
        return $this->hasMany('App\Models\ImportCargo','containerId', 'id');
    }

    public function exportcargoes()
    {
        return $this->hasMany('App\Models\ExportCargo','containerNo', 'id');
    }

    public function ContainerType()
    {
        return $this->belongsTo('App\Models\ItemType','itemtype_Id', 'id');
    }
}
