<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class ExportCargoDetail extends Model
{
    use SoftDeletes;
    protected $table = "exportcargodetails";

    public function container()
    {
        return $this->belongsTo('App\Models\Container','containerNo', 'id');
    }

    // public function exportcargos()
    // {
    //     return $this->belongsTo('App\Models\ExportCargo','exportCargoId', 'id');
    // }
}
