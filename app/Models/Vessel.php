<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vessel extends Model
{
    public $fillable = ['name'];
    public $timestamps = false;

    public function importcargoes()
    {
        return $this->hasMany('App\Models\ImportCargo','vesselId', 'id');
    }

    public function exportcargoes()
    {
        return $this->hasMany('App\Models\ExportCargo','vesselId', 'id');
    }
}
