<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExportCargo extends Model
{
    use SoftDeletes;
    protected $table = 'exportcargoes';

    protected $fillable = [
        'userId',                         
        'serialNo',                      
        'vesselId',                       
        'agentId',                        
        'frieghtForwarderId',             
        'portId',                         
        'voy',                           
        'departureDate',                 
        'description',                   
        'status',     
        'mean_check',                  
        'created_at', 
    ];
    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo('App\User', 'userId', 'id');
    }
    public function agent()
    {
        return $this->belongsTo('App\Models\Agent', 'agentId', 'id');
    }
    public function vessel()
    {
        return $this->belongsTo('App\Models\Vessel', 'vesselId', 'id');
    }
    public function frieghtforwarder()
    {
        return $this->belongsTo('App\Models\FrieghtForWarder', 'frieghtForwarderId', 'id');
    }
    public function port()
    {
        return $this->belongsTo('App\Models\Port', 'portId', 'id');
    }

    public function container()
    {
        return $this->belongsTo('App\Models\Container','containerNo', 'id');
    }

    public function exportcargosdetail()
    {
        return $this->hasMany('App\Models\ExportCargoDetail','exportCargoId', 'id');
    }
    public function exportcargosCertificate()
    {
        // return $this->hasMany('App\Models\ExportCertificate','exportCargoId', 'id');
        return $this->hasOne('App\Models\ExportCertificate','exportCargoId', 'id');
    }
}
