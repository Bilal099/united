<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class FrieghtForWarder extends Model
{
    use SoftDeletes;
    protected $table = "frieghtforwarders";
    protected $fillable = [
        'name'
    ];
    public $timestamps = false;

    public function exportcargoes()
    {
        return $this->hasMany('App\Models\ExportCargo','frieghtForwarderId', 'id');
    }
}
