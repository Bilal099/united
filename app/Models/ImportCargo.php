<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class ImportCargo extends Model
{
    use SoftDeletes;
    protected $table = "importcargoes";

    public function user()
    {
        return $this->belongsTo('App\User', 'userId', 'id');
    }
    public function container()
    {
        return $this->belongsTo('App\Models\Container', 'containerId', 'id');
    }
    public function vessel()
    {
        return $this->belongsTo('App\Models\Vessel', 'vesselId', 'id');
    }
    public function importcargosdetail()
    {
        return $this->hasMany('App\Models\ImportCargoDetail','importCargoId', 'id');
    }
}
