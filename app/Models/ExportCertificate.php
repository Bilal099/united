<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExportCertificate extends Model
{
    use SoftDeletes;
    protected $table = "exportcertificate";
    public $timestamps = false;

    public function exportCargo()
    {
        return $this->belongsTo('App\Models\ExportCargo','exportCargoId' ,'id');
    }


    //exportcertificate
}
