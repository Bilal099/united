<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class ItemType extends Model
{
    use SoftDeletes;
    protected $table = "itemtypes";
    public $fillable = ['name'];

    public function containers()
    {
        return $this->hasMany('App\Models\Container','itemtype_Id', 'id');
    }
}
